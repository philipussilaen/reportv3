function formatDate(inputDate) {
    var d = inputDate.split(" ");
    var date = new Date(d[0]);
    var t = d[1].split(":");

    const monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    const monthShort = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

    if (!isNaN(date.getTime())) {
        var day = ('0' + date.getDate()).slice(-2);
        var month = monthShort[date.getMonth()]; // ('0' + (date.getMonth() + 1)).slice(-2)
        var year = date.getFullYear();
        // var hours = date.getHours();
        // var minutes = date.getMinutes();
        var hours = t[0];
        var minutes = t[1];
        var seconds = t[2];
        var ampm = hours >= 12 ? 'PM' : 'AM';
        // hours = hours % 12;
        // hours = hours ? hours : 12; // the hour '0' should be '12'
        // hours = hours < 10 ? '0' + hours : hours;
        // minutes = minutes < 10 ? '0' + minutes : minutes;

        return day + ' ' + month + ' ' + year + ' ' + hours + ':' + minutes + ':' + seconds;
        // return date.getDate();
    }
}

function formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}

function formatEpochDate(inputDate) {
    const monthShort = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    var date = new Date(inputDate);

    var year = date.getFullYear();
    var month = monthShort[date.getMonth()]; // date.getMonth() + 1;
    var day = ('0' + date.getDate()).slice(-2);
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var seconds = date.getSeconds();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    // hours = hours % 12;
    // hours = hours ? hours : 12;
    hours = hours < 10 ? '0' + hours : hours;
    minutes = minutes < 10 ? '0' + minutes : minutes;
    seconds = seconds < 10 ? '0' + seconds : seconds;

    return (day + " " + month + " " + year + " " + hours + ":" + minutes + ":" + seconds);
}

// collapse filter
$('#linkFilter').click(function () {
    if ($('#collapseFilter').hasClass('collapse show')) {
        $('#btnCollapse').html('SHOW FILTER <i class="fas fa-chevron-right"></i>');
    } else {
        $('#btnCollapse').html('HIDE FILTER <i class="fas fa-chevron-down"></i>');
    }
});

// daterange picker
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth() + 1; // January is 0

var yyyy = today.getFullYear();
if (dd < 10) {
    dd = '0' + dd;
}
if (mm < 10) {
    mm = '0' + mm;
}
var today = yyyy + '-' + mm + '-' + dd;
var startDate = yyyy + '-' + mm + '-' + '01';
// var today = mm + '/' + dd + '/' + yyyy;
// var startDate = mm + '/01/' + yyyy;

$('.daterange').daterangepicker({
    autoApply: true, // Hide the apply and cancel buttons, and automatically apply a new date range as soon as two dates are clicked.
    // dateLimit: { days: 0 }, // Limit date range count
    startDate: startDate,
    maxDate: today,
    ranges: {
        'Today': [moment(), moment()],
        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    },
    alwaysShowCalendars: true,
    showDropdowns: true,
    locale: {
        format: 'YYYY-MM-DD'
    }
}, function (start, end) {
    // console.log("Callback has been called!");
    $('.daterange span').html(start.format('YYYY-MM-DD') + ' / ' + end.format('YYYY-MM-DD'));
    // $('.daterange span').html(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
    startDate = start;
    endDate = end;
});

$('.daterange_blank').daterangepicker({
    autoApply: true,
    autoUpdateInput: false,
    maxDate: today,
    ranges: {
        'Today': [moment(), moment()],
        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month': [moment().startOf('month'), moment().endOf('month')],
        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    },
    alwaysShowCalendars: true,
    showDropdowns: true,
    locale: {
        format: 'YYYY-MM-DD'
    }
}, function (start, end) {
    $('.daterange_blank').val(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
    startDate = start;
    endDate = end;
});