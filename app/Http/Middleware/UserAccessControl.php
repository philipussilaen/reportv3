<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
// use App\Http\Model\Roles;

class UserAccessControl
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $url = $request->getPathInfo();
        $user_id = Auth::user()->id;
        $route = \DB::table('user_route')
        ->where('u_id', $user_id)
        ->get()->toArray();
        
        foreach ($route as $key => $value) {
            if(preg_match("/\{(.*)\}/", $value->route, $matches)) {
                $check = str_replace($matches, '', $value->route);
                $routes[] = str_replace('/', '', $check);
            } else {
                $routes[] = str_replace('/', '', $value->route);
            }
        }

        foreach ($request->route()->parameters() as $row) {
            $url = str_replace($row, '', $url);
        }

        $url = str_replace('/', '', $url);
        if (!empty($user_id) && !in_array($url, $routes)) {
            return new Response(view('auth.404'));
        }
        return $next($request);
    }

    public function strposa($haystack, $needles=array(), $offset=0) {
        $chr = array();
        foreach($needles as $needle) {
            $res = strpos($haystack, $needle, $offset);
            if ($res !== false) $chr[$needle] = $res;
        }
        if(empty($chr)) return false;
        
        return min($chr);
    }
}
