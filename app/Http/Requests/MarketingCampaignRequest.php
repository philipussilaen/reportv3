<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MarketingCampaignRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'description' => 'nullable',
            'date'=>'required',
            'status' => 'required',
            'priority' => 'required',
            'promoType' => 'required',
            'amount_type' => 'required',
            'amount' => 'required|numeric',
            'category' => 'required_if:promoType,transaction|in:fixed,cutback',
            'voucher_required' => 'required',
            'voucher_type' => 'required|in:all,member,voucher',
            'limit_usage' => 'nullable|numeric',
            'minimal_amount' => 'nullable|numeric',
            'maximum_amount' => 'nullable|numeric'
        ];
    }
}
