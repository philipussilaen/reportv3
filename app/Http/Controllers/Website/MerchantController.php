<?php

namespace App\Http\Controllers\Website;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Popbox\WebsiteCompany;
use App\Models\Popbox\WebsiteMerchant;

class MerchantController extends Controller
{
    public function view(Request $request)
    {
        $data["list"] = WebsiteMerchant::all();
        return view('website.merchant.list', $data);
    }

    public function create()
    {
        $company = WebsiteCompany::all();
        $category = [
            'adv' => 'Brand',
            'market' => 'E-Commerce',
            'demand' => 'On Demand',
        ];
        $data = [
            'company' => $company,
            'category' => $category
        ];
        return view('website.merchant.create', $data);
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'image' => 'required|image|mimes:jpg,jpeg,png|max:1024',
            'region' => 'required',
            'category' => 'required',
            'status' => 'required',
            'company' => 'required'
        ]);

        if( $request->hasFile('image')){ 
            $image = $request->file('image'); 
            $fileName = $image->getClientOriginalName();
            $fileExtension = $image->getClientOriginalExtension();
            $newImage = $request->category."_".time().'.'.$fileExtension;
            $image->move(public_path('uploads/merchant'), $newImage);
        } else {
            $newImage = NULL;
        }

        if($validatedData) {
            $data = new WebsiteMerchant();
            
            $data->name = $request->name;
            $data->description = $request->description;
            $data->image = $newImage;
            $data->url = $request->url;
            $data->region = $request->region;
            $data->category = $request->category;
            $data->company = $request->company;
            $data->status = $request->status;
            $data->save();
        }
		return redirect('website/merchant')->with(['success'=> 'New merchant has been added']);
    }

    public function edit(Request $request)
    {
        $id = $request->id;
        $merchant = WebsiteMerchant::where('id_merchant', $id)->first();
        if($merchant == NULL) {
            return redirect('website/merchant');
        } else {
            $company = WebsiteCompany::all();
            $category = [
                'adv' => 'Brand',
                'market' => 'E-Commerce',
                'demand' => 'On Demand',
            ];
            $data = [
                'company' => $company,
                'category' => $category,
                'data' => $merchant
            ];
            return view('website.merchant.edit', $data);
        }
    }

    public function update(Request $request)
    {
        $id = $request->id;
        $validatedData = $request->validate([
            'name' => 'required',
            'image' => 'image|mimes:jpg,jpeg,png|max:1024',
            'region' => 'required',
            'category' => 'required',
            'status' => 'required',
            'company' => 'required'
        ]);

        if( $request->hasFile('image')){ 
            $image = $request->file('image'); 
            $fileName = $image->getClientOriginalName();
            $fileExtension = $image->getClientOriginalExtension();
            $newImage = $request->category."_".time().'.'.$fileExtension;
            $image->move(public_path('uploads/merchant'), $newImage);
        } else {
            $newImage = $request->old_image;
        }

        $update = [
            'image' => $newImage,
            'name' => $request->name,
            'description' => $request->description,
            'url' => $request->url,
            'region' => $request->region,
            'category' => $request->category,
            'status' => $request->status,
            'company' => $request->company
        ];
        
        $query = WebsiteMerchant::where('id_merchant', $id)->update($update);
        if($query) {
            return redirect('website/merchant/edit/'.$id)->with(['success'=> 'Merchant has been updated']);
        } else {
            return redirect('website/merchant/edit/'.$id)->with(['error'=> 'Updating merchant failed']);
        }
    }

    public function destroy(Request $request)
    {
        $id = $request->id;
        WebsiteMerchant::where('id_merchant', $id)->delete();
        return redirect('website/merchant')->with(['success'=> 'Merchant has been deleted']);
    }
}
