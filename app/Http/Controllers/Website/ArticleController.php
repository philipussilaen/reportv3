<?php

namespace App\Http\Controllers\Website;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Popbox\WebsiteArticle;
use App\Models\Popbox\WebsiteCompany;

class ArticleController extends Controller
{
    public function view(Request $request)
    {
        $data["list"] = WebsiteArticle::all();
        return view('website.article.list', $data);
    }

    public function create()
    {
        $company = WebsiteCompany::all();
        $status = ['Draft','Non','Publish']; // draft, non, publish
        $type = ['Blog','News','Promo','Promo-Mobile','Promo-Topup','Random','Testi']; // blog, news, promo, promo-mobile, promo-topup, random, testi
        $data = [
            'company' => $company,
            'status' => $status,
            'type' => $type
        ];
        return view('website.article.create', $data);
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'title' => 'required|max:100',
            'subtitle' => 'required|max:100',
            'short_title' => 'required|max:50',
            'content' => 'required',
            'type' => 'required',
            'status' => 'required',
            'company' => 'required',
            'region' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'priority' => 'required',
            'image' => 'image|mimes:jpg,jpeg,png|max:1024'
        ]);

        if( $request->hasFile('image')){ 
            $image = $request->file('image'); 
            $fileName = $image->getClientOriginalName();
            $fileExtension = $image->getClientOriginalExtension();
            $string = str_replace(' ', '-', strtolower($request->short_title));
            $string = preg_replace('/[^\p{L}\p{N}\s]/u', '', $string);
            $newImage = time()."_".$string.'.'.$fileExtension;
            $image->move(public_path('uploads/article'), $newImage);
        } else {
            $newImage = NULL;
        }
        $time = date('H:i:s');
        $start = $request->start_date.' '.$time;
        $end = $request->end_date.' '.$time;

        if($validatedData) {
            $data = new WebsiteArticle();
            
            $data->title = $request->title;
            $data->subtitle = $request->subtitle;
            $data->short_title = $request->short_title;
            $data->content = $request->content;
            $data->type = $request->type;
            $data->status = $request->status;
            $data->company = $request->company;
            $data->region = $request->region;
            $data->start_date = $start;
            $data->end_date = $end;
            $data->created_date = date('Y-m-d');;
            $data->priority = $request->priority;
            $data->image = $newImage;
            $data->save();
        }
		return redirect('website/article')->with(['success'=> 'New article has been added']);
    }

    public function edit(Request $request)
    {
        $id = $request->id;
        $article = WebsiteArticle::where('id_article', $id)->first();
        if($article == NULL) {
            return redirect('website/article');
        } else {
            $company = WebsiteCompany::all();
            $status = ['Draft','Non','Publish']; // draft, non, publish
            $type = ['Blog','News','Promo','Promo-Mobile','Promo-Topup','Random','Testi']; // blog, news, promo, promo-mobile, promo-topup, random, testi
            $data = [
                'data' => $article,
                'company' => $company,
                'status' => $status,
                'type' => $type
            ];
            return view('website.article.edit', $data);
        }
    }

    public function update(Request $request)
    {
        $id = $request->id;
        $validatedData = $request->validate([
            'title' => 'required|max:100',
            'subtitle' => 'required|max:100',
            'short_title' => 'required|max:50',
            'content' => 'required',
            'type' => 'required',
            'status' => 'required',
            'company' => 'required',
            'region' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'priority' => 'required',
            'image' => 'image|mimes:jpg,jpeg,png|max:1024'
        ]);
        
        if( $request->hasFile('image')){ 
            $image = $request->file('image'); 
            $fileName = $image->getClientOriginalName();
            $fileExtension = $image->getClientOriginalExtension();
            $string = str_replace(' ', '-', strtolower($request->short_title));
            $string = preg_replace('/[^\p{L}\p{N}\s]/u', '', $string);
            $newImage = time()."_".$string.'.'.$fileExtension;
            $image->move(public_path('uploads/article'), $newImage);
        } else {
            $newImage = $request->old_image;
        }
        $time = date('H:i:s');
        $start = $request->start_date.' '.$time;
        $end = $request->end_date.' '.$time;
        
        $update = [
            'title' => $request->title,
            'subtitle' => $request->subtitle,
            'short_title' => $request->short_title,
            'content' => $request->content,
            'type' => strtolower($request->type),
            'status' => strtolower($request->status),
            'company' => $request->company,
            'region' => $request->region,
            'start_date' => $start,
            'end_date' => $end,
            'priority' => $request->priority,
            'image' => $newImage
        ];
        
        $query = WebsiteArticle::where('id_article', $id)->update($update);
        if($query) {
            return redirect('website/article/edit/'.$id)->with(['success'=> 'Article has been updated']);
        } else {
            return redirect('website/article/edit/'.$id)->with(['error'=> 'Updating article failed']);
        }
    }

    public function destroy(Request $request)
    {
        $id = $request->id;
        WebsiteArticle::where('id_article', $id)->delete();
        return redirect('website/article')->with(['success'=> 'Article has been deleted']);
    }
}
