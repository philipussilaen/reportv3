<?php

namespace App\Http\Controllers\Website;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Popbox\WebsiteCareer;
use App\Models\Popbox\WebsiteCategory;
use App\Models\Popbox\WebsiteCompany;

class CareerController extends Controller
{
    public function view(Request $request)
    {
        $data["list"] = WebsiteCareer::all();
        return view('website.career.list', $data);
    }

    public function create()
    {
        $company = WebsiteCompany::all();
        $category = WebsiteCategory::all();
        return view('website.career.create', compact('company','category'));
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'job_title' => 'required|max:255',
            'description' => 'required',
            'requirement' => 'required',
            'job_desc' => 'required',
            'category' => 'required',
            'company' => 'required',
            'region' => 'required',
            'status' => 'required'
        ]);

        if($validatedData) {
            $data = new WebsiteCareer();
            
            $data->job_title = $request->job_title;
            $data->description = $request->description;
            $data->requirement = $request->requirement;
            $data->job_desc = $request->job_desc;
            $data->benefit = $request->benefit;
            $data->status = $request->status;
            $data->company = $request->company;
            $data->category = $request->category;
            $data->location = $request->region;
            $data->created_date = date("Y-m-d H:i:s");
            $data->save();
        }
		return redirect('website/career')->with(['success'=> 'New career info has been added']);
    }

    public function edit(Request $request)
    {
        $id = $request->id;
        $career = WebsiteCareer::where('id_career', $id)->first();
        if($career == NULL) {
            return redirect('website/career');
        } else {
            $company = WebsiteCompany::all();
            $category = WebsiteCategory::all();
            $data = [
                'company' => $company,
                'category' => $category,
                'data' => $career
            ];
            return view('website.career.edit', $data);
        }
    }

    public function update(Request $request)
    {
        $id = $request->id;
        $validatedData = $request->validate([
            'job_title' => 'required|max:255',
            'description' => 'required',
            'requirement' => 'required',
            'job_desc' => 'required',
            'category' => 'required',
            'company' => 'required',
            'region' => 'required',
            'status' => 'required'
        ]);
        
        $update = [
            'job_title' => $request->job_title,
            'description' => $request->description,
            'requirement' => $request->requirement,
            'job_desc' => $request->job_desc,
            'benefit' => $request->benefit,
            'category' => $request->category,
            'company' => $request->company,
            'location' => $request->region,
            'status' => strtolower($request->status)
        ];
        
        $query = WebsiteCareer::where('id_career', $id)->update($update);
        if($query) {
            return redirect('website/career/edit/'.$id)->with(['success'=> 'Career info has been updated']);
        } else {
            return redirect('website/career/edit/'.$id)->with(['error'=> 'Updating career info failed']);
        }
    }

    public function destroy(Request $request)
    {
        $id = $request->id;
        WebsiteCareer::where('id_career', $id)->delete();
        return redirect('website/career')->with(['success'=> 'Career info has been deleted']);
    }
}
