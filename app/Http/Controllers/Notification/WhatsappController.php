<?php

namespace App\Http\Controllers\Notification;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Lib\Whatsapp;
use App\Http\Helpers\ProxApi;
use App\Models\NewLocker\SmsHistory;
use App\Models\NewLocker\Express;
use App\Models\NewLocker\Box;
use DB;
use Illuminate\Support\Facades\Log;
use App\Lib\Audit;
class WhatsappController extends Controller
{
    protected $whatsapp;

    public function __construct() {
        $token = env('WABLAS_TOKEN');
        $this->whatsapp = new Whatsapp($token);
    }
    public function resend(Request $request) {
        $phone = $request->phone;
        $locker =  $request->locker;
        $code =  $request->code;
        $express = Express::where('id',$code)->first();
        $realNumber = $this->buildPhone($express->operator_id, $phone);
        $content = $this->buildMessage($express);
        $payload = [
            "phone"=>$realNumber,
            "message"=>$content,
        ];
        Audit::create($request->code,'Delivery Activity Detail',json_encode($payload),'Resend via Whatsapp');
        return $this->send($payload);
        
    }

    private function buildMessage($express) {
        $boxId = $express->box_id;
        $box = Box::where('id',$boxId)->first();
        $boxName = $box->name;
        $validateCode = $express->validateCode;
        $expressNumber = $express->expressNumber;
        $overdue = date('d/m/y H:i', $express->overdueTime/1000);
        $operator = $express->operator_id;
        $groupName = $express->groupName;
        // message
        $content = "Kode Buka : ".$validateCode." Order No: ".$expressNumber." sudah tiba di PopBox@".$boxName.". Valid s/d: ".$overdue.". CS: 02122538719";
        if($operator === '145b2728140f11e5bdbd0242ac110001') {
            if($groupName == 'COD') {
                $content = "Order Anda " . $expressNumber . " sudah sampai di Popbox@" . $boxName . ", bayar sebelum " . $overdue . " di loker/kasir untuk dapat PIN ambil barang - www.popbox.asia";
            }else{
                $templateGroup  = DB::connection('newlocker_db')->table('tb_newlocker_grouptemplate')->where('name',$groupName)->first();
                if(!is_null($templateGroup)) {
                    $template = DB::connection('newlocker_db')
                            ->table('tb_newlocker_smstemplate')
                            ->where('smsType','!=','REJECT_EXPRESS_STORE_NOTIFY_CUSTOMER')
                            ->where('templateGroup_id',$templateGroup->id)
                            ->first();
                    $contentDb = str_replace('{overdueDayOfMouth}/{overdueMouth}/17','{overdue}',$template->content);
                    $pattern = array('/{validateCode}/','/{expressNumber}/','/{boxName}/','/{overdue}/');
                    $replace = array($validateCode,$expressNumber,$boxName,$overdue);
                    $content = preg_replace($pattern,$replace, $contentDb);
                } 
            }
        }else{
            $content = "Pin Code: ".$validateCode."\nCollect your order ".$expressNumber." at PopBox Locker@".$boxName." before ".$overdue.". www.popbox.asia";
        }
        return $content;
    }
    private function buildPhone($code,$phone) {
        $area = [
            '145b2728140f11e5bdbd0242ac110001'=>'id',
            '4028808357ad74f50157bc5681567b9e'=>'my'
        ];
        $ptn = "/^0/";
        if($area[$code] === 'id') {
            $codeArea = "62";
        }else{
            $codeArea = "60";
        }
        $newPhone = preg_replace($ptn, $codeArea, $phone);
        return $newPhone;
    }
    private function send($payload) {
        $code = '200';
        $message = '';
        try {
            $response =$this->whatsapp->send($payload)->response();
            $message = $response;
            $this->logWhatsap($payload['phone'], json_encode($message));
        } catch (\Throwable $th) {
            $message = $th->getMessage();
            $code = ($th->getCode() === 0 ) ? 500 : $th->getCode();
        }
        return response()->json($message, $code);
    }

    private function logWhatsap($phone, $message) {
        Log::info('phone: '.$phone.' Message: '.$message);
    }
}
