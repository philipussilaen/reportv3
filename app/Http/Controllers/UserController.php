<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Helpers\WebCurl;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\MessageBag;
use Hash;
use App\Models\User;
use App\Models\Group;
use App\Models\UserGroup;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = User::all();
        
        return view('user.user', compact('list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['groups'] = Group::all();
        $header[] = 'POPBOX-HEADER-AUTH:'.env('POPBOX_HEADER_AUTH');
        $header[] = 'SERVICE-KEY:'.env('SERVICE_KEY');
        $curl = new WebCurl($header);
        $url1 = env('PROX_URL').'company/list';
        $list = json_decode($curl->get($url1, $header));
        $data['companies'] = $list->data;
        return view('user.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'parent_id_company' => 'required',
            'password' => 'required|string|min:6',
            'c_password' => 'required|same:password'
        ]);

        if($validatedData) {
        // $data = array(
        //     'name' => $request->name,
        //     'email' => $request->email,
        //     'password' => Hash::make($request->password)
        // );
        
        // $id = DB::table('users')->insertGetId($data);
        // foreach ($request->group_id as $key => $row)
        // {
        //     $groupRole []  = [ 
        //         'user_id' => $id,
        //         'group_id' => $row
        //     ];
        // }
        // DB::table('user_groups')->insert($groupRole);

        // Laravel attach : many-to-many
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->id_company = $request->parent_id_company;
        $user->save();

        $group = Group::find($request->group_id);
        $user->groups()->attach($group);
        }
		return redirect('user')->with(['success'=> 'New user has been added']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $id = $request->input('id',null);
        $data = User::where('id', $id)->first();
        $groups = Group::all();

        $header[] = 'POPBOX-HEADER-AUTH:'.env('POPBOX_HEADER_AUTH');
        $header[] = 'SERVICE-KEY:'.env('SERVICE_KEY');
        $curl = new WebCurl($header);
        $url1 = env('PROX_URL').'company/list';
        $list = json_decode($curl->get($url1, $header));
        $companies = $list->data;

        $selectedGroup = UserGroup::where('user_id', $id)->get();
        $selected = [];
        foreach ($selectedGroup as $key => $value) {
            $selected[] = $value->group_id;
        }
        
        return view('user.edit', compact('companies', 'data', 'groups', 'selectedGroup', 'selected'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = $request->input('id',null);
        $data = User::where('id', $id)->first();
        $data->name = $request->name;
        $data->id_company = $request->parent_id_company;
        $data->save();

        $data->groups()->sync($request->group_id);
        // Insert, update, or delete role_route table
        // foreach ($request->group_id as $key => $row)
        // {
        //     $userGroup []  = [ 
        //         'user_id' => $data->id,
        //         'group_id' => $row
        //     ];
        // }
        // DB::table('user_groups')->insert($userGroup);

        // $user->groups()->wherePivot('group_id', '!=', $request->group_id)->detach();
        // dd($request->group_id);
        
        return redirect('user')->with(['success'=> 'User detail has been changed']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $id = $request->input('id',null);
        User::where('id', $id)->delete();
        return redirect('user')->with(['success'=> 'User has been deleted']);
    }

    /**
     * Reset password.
     *
     * @return \Illuminate\Http\Response
     */
    public function reset(Request $request)
    {
        $id = $request->input('id',null);
        $data = User::where('id', $id)->first();
        $data->password = Hash::make('popdash123');
        $data->save();

        return redirect('user')->with(['success'=> "User's password has been reset"]);
    }
}
