<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Role;
use App\Models\Route;
use App\Models\Role_route;
use App\Models\UserRoute;
use App\Menu;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $list= Role::all();
        
        return view('role.role', compact('list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $menu = Route::All();
        return view('role.create')
        ->with('menu', $menu);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:255',
            'description' => 'required'
        ]);

        if($validatedData) {
            $role = new Role;
            $role->name = $request->name;
            $role->description = $request->description;
            $role->save();
            
            $route = Route::find($request->route_id);
            $role->routes()->attach($route);
        }

        // $data = new Role();
        //var_dump($request);
		// $data->name = $request->name;
		// $data->description = $request->description;
        // $data->save();
        
        /*// get last role id

        // Insert, update, or delete role_route table
        foreach ($request->route_id as $key => $row)
        {
            $roleRoute []  = [ 
                'role_id' => $lastRoleId,
                'route_id' => $row->id,
            ];
        }
        DB::table('role_routes')->insert($roleRoute);*/
		return redirect('role')->with(['success'=> 'New role has been added']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $id = $request->input('id',null);
        $list = Role::where('id', $id)->first();
        $menu = Route::All();
        
        $selectedMenu = Role_route::where('role_id', $id)->get();
        $selectedRoute = [];
        foreach ($selectedMenu as $key => $value) {
            $selectedRoute[] = $value->route_id;
        }

        return view('role.edit', compact('list', 'menu', 'selectedMenu', 'selectedRoute'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = $request->input('id',null);
        $data = Role::where('id', $id)->first();
        $data->name = $request->name;
        $data->description = $request->description;
        $data->save();

        $data->routes()->sync($request->route_id);

        // Insert, update, or delete role_route table
        // $roleroute = Role_route::get_by_role_id($data->id);
        // foreach($roleroute as $row) {
        //     $routes[] = $row->route_id;
        // }
        
        // foreach ($request->route_id as $key => $row)
        // {
        //     $roleRoute []  = [ 
        //         'role_id' => $data->id,
        //         'route_id' => $row,
        //     ];
        // }
        // DB::table('role_routes')->insert($roleRoute);
      
        return redirect('role')->with(['success'=> 'Role detail has been changed']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $id = $request->input('id',null);
        Role::where('id', $id)->delete();
        return redirect('role')->with(['success'=> 'Role has been deleted']);
    }
}
