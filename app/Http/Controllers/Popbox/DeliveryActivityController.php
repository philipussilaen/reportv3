<?php

namespace App\Http\Controllers\Popbox;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Helpers\ProxApi;
use App\Http\Helpers\WebCurl;
use App\Models\NewLocker\Box;
use App\Models\NewLocker\Blocked;
use App\Models\NewLocker\Company;
use App\Models\NewLocker\Express;
use App\Models\NewLocker\MouthType;
use App\Models\NewLocker\SmsHistory;
use App\Lib\Audit;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class DeliveryActivityController extends Controller
{
    public function viewDeliveryActivity(Request $request)
    {
        $id_company = Auth::user()->id_company;
        if($id_company == '161e5ed1140f11e5bdbd0242ac110001' || $id_company == NULL) {
            $countries = [
                '145b2728140f11e5bdbd0242ac110001' => 'Indonesia',
                '4028808357ad74f50157bc5681567b9e' => 'Malaysia'
            ];
            $companies = Company::distinct()
            ->where('company_name','NOT LIKE',"GRAB%")
            ->get(['company_name'])
            ->pluck('company_name');
        } else {
            $countries = [];
            $companies = $this->getCompanies($id_company);
        }
        $types = [
            'COURIER_STORE' => 'LAST MILE',
            // 'POPSAFE' => 'POPSAFE',
            'CUSTOMER_STORE' => 'POPSEND',
            'CUSTOMER_REJECT' => 'RETURN'
        ];

        $status = [
            'CUSTOMER_TAKEN' => 'CUSTOMER TAKEN',
            'COURIER_TAKEN' => 'COURIER TAKEN',
            'IN_STORE' => 'IN STORE',
            'IMPORTED' => 'IMPORTED',
            'OPERATOR_TAKEN' => 'OPERATOR TAKEN',
            'OVERDUE' => 'OVERDUE'
        ];
        
        $location = Box::orderBy('name','asc')->get()->pluck('id','name');
        
        $data = [
            'id_company'=> $id_company,
            'companies' => $companies,
            'countries' => $countries,
            'location'  => $location,
            'status'    => $status,
            'types'     => $types
        ];

        return view('popbox.locker.delivery.list', $data);
    }

    private function getCompanies($id_company)
    {
        $header[] = 'POPBOX-HEADER-AUTH:'.env('POPBOX_HEADER_AUTH');
        $header[] = 'SERVICE-KEY:'.env('SERVICE_KEY');
        $curl = new WebCurl($header);
        $url = env('PROX_URL').'company/parent/child/'.$id_company;
        try {
            $response = json_decode($curl->get($url, $header));
            $data['companies'] = $response->data;
        }  catch (\Exception $e) {
            $data['companies'] = NULL;
        }
        $companies = []; $child = [];
        if($data['companies'] != NULL) {
            $companies[] = $data['companies'][0]->company_name;
            // $companies[0]['id_company'] = $data['companies'][0]->id_company;
            // $companies[0]['company_name'] = $data['companies'][0]->company_name;
            $child = $data['companies'][0]->child;
        }
        if($child) {
            foreach ($child as $key => $row) {
                $companies[] = $row->company_name;
                // $companies[$key+1]['id_company'] = $row->id_company;
                // $companies[$key+1]['company_name'] = $row->company_name;
            }
        }
        return $companies;
    }

    private function getCompaniesId($id_company)
    {
        $header[] = 'POPBOX-HEADER-AUTH:'.env('POPBOX_HEADER_AUTH');
        $header[] = 'SERVICE-KEY:'.env('SERVICE_KEY');
        $curl = new WebCurl($header);
        $url = env('PROX_URL').'company/parent/child/'.$id_company;
        try {
            $response = json_decode($curl->get($url, $header));
            $data['companies'] = $response->data;
        }  catch (\Exception $e) {
            $data['companies'] = NULL;
        }
        $companies_id = []; $child = [];
        if($data['companies'] != NULL) {
            $companies_id[] = $data['companies'][0]->id_company;
            $child = $data['companies'][0]->child;
        }
        if($child) {
            foreach ($child as $key => $row) {
                $companies_id[] = $row->id_company;
            }
        }
        return $companies_id;
    }

    private function convertEpoch($epoch) {
        $date = explode(" - ", $epoch);
        $startDate = $date[0].' 00:00:00';
        $endDate = $date[1].' 23:59:59';
        
        $start = strtotime($startDate) * 1000;
        $end = strtotime($endDate) * 1000;

        return array('start' => $start, 'end' => $end);
    }

    public function getAjaxDeliveryActivity(Request $request) {
        $columns = array(
            0 => 'expressNumber',
            1 => 'expressType',
            2 => 'locker_name',
            3 => 'locker_size',
            4 => 'phone',
            5 => 'company_name',
            6 => 'status',
            7 => 'importTime',
            8 => 'storeTime',
            9 => 'takeTime',
            10=> 'overdueTime',
            11=> 'pin',
            12=> 'id',
            13=> 'operator_id',
            14=> 'courier'
        );
        
        $start = $request->start;
        $order = $columns[8];//$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $limit = isset($request->length) ? $request->length : 10;
        $currentPage = isset($start) ? ($start/$limit)+1 : 1;
        
        $id_company = Auth::user()->id_company;
        $companies_id = $this->getCompaniesId($id_company);

        $rangeDate = $request->date;
        $expressNumber = $request->expressNumber;
        $expressType = $request->expressType;
        $location = $request->location;
        $phone = $request->phone;
        $status = $request->status;
        $date = $request->storeTime;
        $pin = $request->pin;
        $parcelId = $request->parcelId;
        $country = $request->country;
        $courier = $request->courier;
        $company = $request->company;
        $importTime = $request->importTime;
        $takeTime = $request->takeTime;
        $overdueTime = $request->overdueTime;
        $dateBefore = $request->dateBefore;
        $dateAfter = $request->dateAfter;
        $startDate = null;
        $endDate = null;
        if (!empty($rangeDate)) {
            $date = explode(" - ", $rangeDate);
            $startDate = $date[0].' 00:00:00';
            $endDate = $date[1].' 23:59:59';
        }
        $unixStartDate = strtotime($startDate) * 1000;
        $unixEndDate = strtotime($endDate) * 1000;
        $unixNowDateTime = time()*1000;

        $startImportUnix = null; $endImportUnix = null;
        $startTakeUnix = null; $endTakeUnix = null;
        $startOverdueUnix = null; $endOverdueUnix = null;
        if (!empty($importTime)) { $import = $this->convertEpoch($importTime); $startImportUnix = $import['start']; $endImportUnix = $import['end']; }
        if (!empty($takeTime)) { $take = $this->convertEpoch($takeTime); $startTakeUnix = $take['start']; $endTakeUnix = $take['end']; }
        if (!empty($overdueTime)) { $overdue = $this->convertEpoch($overdueTime); $startOverdueUnix = $overdue['start']; $endOverdueUnix = $overdue['end']; }

        $totalData = Express::with(['company','box','mouth','user'])->count();
        $allData = Express::with(['company','box','mouth','user'])
        ->when($rangeDate, function ($query) use ($unixStartDate,$unixEndDate){
            return $query->whereBetween('tb_newlocker_express.storetime',[$unixStartDate,$unixEndDate]);
        })
        ->when($id_company != '161e5ed1140f11e5bdbd0242ac110001' && $companies_id, function ($query) use ($companies_id){
            return $query->whereIn('tb_newlocker_express.logisticsCompany_id',$companies_id);
        })
        ->when($parcelId, function ($query) use ($parcelId){
            return $query->where('tb_newlocker_express.id','LIKE',"%$parcelId%");
        })
        ->when($expressNumber, function ($query) use ($expressNumber){
            // return $query->where('tb_newlocker_express.expressNumber','LIKE',"%$expressNumber%")
            //     ->orWhere('tb_newlocker_express.customerStoreNumber','LIKE',"%$expressNumber%");
            return $query->whereRaw('(tb_newlocker_express.expressNumber LIKE "%'.$expressNumber.'%" or tb_newlocker_express.customerStoreNumber LIKE "%'.$expressNumber.'%")');
        })
        ->when($expressType, function ($query) use ($expressType){
            // if ($expressType == 'POPSAFE') {
            //     return $query->where('tb_newlocker_express.expressType','=',"CUSTOMER_STORE")
            //         ->where('tb_newlocker_express.groupName','=',"POPDEPOSIT");
            // } else {
                return $query->where('tb_newlocker_express.expressType','=',"$expressType");
            // }
        })
        ->when($status, function ($query) use ($status,$unixNowDateTime){
            if ($status == 'OVERDUE') {
                return $query->where('tb_newlocker_express.status','IN_STORE')
                    ->where('tb_newlocker_express.overdueTime','<',$unixNowDateTime);
            } else if ($status == 'IN_STORE') {
                return $query->where('tb_newlocker_express.status','IN_STORE')
                    ->where('tb_newlocker_express.overdueTime','>=', $unixNowDateTime);
            } else {
                return $query->where('tb_newlocker_express.status',"$status");
            }
        })
        ->when($location, function ($query) use ($location){
            return $query->where('tb_newlocker_express.box_id','=',"$location");
        })
        ->when($phone, function ($query) use ($phone){
            return $query->where('tb_newlocker_express.takeUserPhoneNumber','LIKE',"%$phone%");
        })
        ->when($pin, function ($query) use ($pin){
            return $query->where('tb_newlocker_express.validateCode','LIKE', "%$pin%");
        })
        ->when($country, function ($query) use ($country){
            return $query->where('tb_newlocker_express.operator_id',$country);
        })
        ->when($company, function ($query) use ($company){
            return $query->whereHas('company', function ($q) use($company) {
                $q->where('tb_newlocker_company.company_name','LIKE',"%$company%");
            });
        })
        ->when($courier, function ($query) use ($courier){
            return $query->whereHas('user', function ($q) use($courier) {
                $q->where('tb_newlocker_user.username','LIKE',"%$courier%");
            });
        })
        ->when($importTime, function ($query) use ($startImportUnix, $endImportUnix){
            return $query->whereBetween('tb_newlocker_express.importTime',[$startImportUnix, $endImportUnix]);
        })
        ->when($takeTime, function ($query) use ($startTakeUnix, $endTakeUnix){
            return $query->whereBetween('tb_newlocker_express.takeTime',[$startTakeUnix, $endTakeUnix]);
        })
        ->when($overdueTime, function ($query) use ($startOverdueUnix, $endOverdueUnix){
            return $query->whereBetween('tb_newlocker_express.overdueTime',[$startOverdueUnix, $endOverdueUnix]);
        })
        ->when($dateBefore, function ($query) use ($dateBefore, $dateAfter){
            return $query->whereNotNull('tb_newlocker_express.storeTime')
            ->whereNotBetween('tb_newlocker_express.storeTime',[$dateBefore, $dateAfter]);
        })
        ->orderBy($order, $dir)
        ->paginate($limit,['*'],'draw',$currentPage);
        
        $data = [];
            foreach($allData->items() as $post) {
                $nestedData['parcelId'] = $post->id;
                $nestedData['expressNumber'] = $post->expressNumber;
                $nestedData['expressType'] = $post->expressType;
                $nestedData['customerStoreNumber'] = $post->customerStoreNumber;
                $nestedData['phone'] = $post->takeUserPhoneNumber;
                $nestedData['storePhone'] = $post->storeUserPhoneNumber;
                $nestedData['status'] = $post->status;
                $nestedData['importTime'] = $post->importTime;
                $nestedData['storeTime'] = $post->storeTime;
                $nestedData['takeTime'] = $post->takeTime;
                $nestedData['overdueTime'] = $post->overdueTime;
                $nestedData['validateCode'] = $post->validateCode;
                $nestedData['country'] = $post->operator_id;
                $nestedData['groupName'] = $post->groupName;

                if($post->company != null) {
                    $nestedData['company_name'] = $post->company->company_name;
                } else {
                    $nestedData['company_name'] = '';
                }

                if($post->box != null) {
                    $nestedData['locker_name'] = $post->box->name;
                } else {
                    $nestedData['locker_name'] = '';
                }

                if($post->mouth != null) {
                    $nestedData['locker_number'] = $post->mouth->number;
                    $mouthtype = MouthType::where('id_mouthtype', $post->mouth->mouthType_id)->first();
                    $nestedData['locker_size'] = $mouthtype->name;
                } else {
                    $nestedData['locker_number'] = '';
                    $nestedData['locker_size'] = '';
                }
                
                if($post->user != null) {
                    $nestedData['courier'] = $post->user->username;
                } else {
                    $nestedData['courier'] = '';
                }

                $nestedData['nowDateTime'] = time() *1000;

                $data[] = $nestedData;
            }

        $totalFiltered = $allData->total();

        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );
        return($json_data);
    }

    public function postAjaxSummaryDeliveryActivityList(Request $request)
    {
        $dateRange = $request->input('dateRange', null);
        $lockerId = $request->input('locker', null);
        $expressType = $request->input('expressType', null);
        $parcelNumber = $request->input('parcelNumber', null);
        $pin = $request->input('pin', null);
        $phone = $request->input('phone', null);
        $status = $request->input('status', null);

        $startDate = date('Y-m-1');
        $endDate = date('Y-m-d');
        if (!empty($dateRange)) {
            $tmpDate = explode('-', $dateRange);
            $startDate = date('Y-m-d', strtotime($tmpDate[0]));
            $endDate = date('Y-m-d', strtotime($tmpDate[1]));
        }
        $startDate .= " 00:00:00";
        $endDate .= " 23:59:59";

        $unixStartDate = strtotime($startDate) * 1000;
        $unixEndDate = strtotime($endDate) * 1000;

        $unixNowDateTime = time()*1000;

        // count OVERDUE parcel
        $countOverdue = Express::whereBetween('tb_newlocker_express.storetime',[$unixStartDate,$unixEndDate])
            ->where('tb_newlocker_express.status','IN_STORE')
            ->where('tb_newlocker_express.overdueTime','<',$unixNowDateTime)
            // ->when($companyId,function($query) use($companyId){
            //     return $query->where('tb_newlocker_express.logisticsCompany_id',$companyId);
            // })
            ->count();

        // count IN_STORE
        $countInStore = Express::whereBetween('tb_newlocker_express.storetime',[$unixStartDate,$unixEndDate])
            ->where('tb_newlocker_express.status','IN_STORE')
            // ->when($companyId,function($query) use($companyId){
            //     return $query->where('tb_newlocker_express.logisticsCompany_id',$companyId);
            // })
            ->count();

        // count CUSTOMER_TAKEN
        $countCustomerTaken = Express::whereBetween('tb_newlocker_express.storetime',[$unixStartDate,$unixEndDate])
            ->where('tb_newlocker_express.status','CUSTOMER_TAKEN')
            // ->when($companyId,function($query) use($companyId){
            //     return $query->where('tb_newlocker_express.logisticsCompany_id',$companyId);
            // })
            ->count();
        dd($startDate, $endDate, $countOverdue, $countInStore, $countCustomerTaken);
    }

    public function getDetail(Request $request)
    {
        $parameter = $request->route()->parameters();
        $parcelId = $parameter['parcel_id'];
        $express = Express::with(['detail'])
        ->leftJoin('tb_newlocker_company as tnc','tnc.id_company','=','tb_newlocker_express.logisticsCompany_id')
        ->leftJoin('tb_newlocker_box as tnb','tnb.id','=','tb_newlocker_express.box_id')
        ->leftJoin('tb_newlocker_mouth as tnm','tnm.id_mouth','=','tb_newlocker_express.mouth_id')
        ->leftjoin('tb_newlocker_mouthtype as tnmt','tnmt.id_mouthtype','=','tnm.mouthType_id')
        ->where('tb_newlocker_express.id',$parcelId)
        ->select('tb_newlocker_express.id','tb_newlocker_express.expressType','tb_newlocker_express.expressNumber','tb_newlocker_express.customerStoreNumber','tb_newlocker_express.takeUserPhoneNumber','tb_newlocker_express.status','tb_newlocker_express.groupName','tb_newlocker_express.overdueTime','tb_newlocker_express.storeTime','tb_newlocker_express.takeTime','tb_newlocker_express.storeUserPhoneNumber','tb_newlocker_express.validateCode', 'tb_newlocker_express.storeUser_id', 'tb_newlocker_express.takeUser_id','tb_newlocker_express.operator_id','tnc.company_name as companyName','tnc.company_type as companyType','tnb.name as lockerName','tnm.number AS lockerNumber','tnmt.name AS lockerSize')
        ->first();
        $sms['data'] = SmsHistory::where('express_id', $express->id)->get();
        $sms['sms_count'] = SmsHistory::countSMS($express->id);
        $data = [
            'express' => $express,
            'sms' => $sms
        ];
        
        return view('popbox.locker.delivery.detail', $data);
    }

    public function getDetailPin(Request $request)
    {
        $express_id = $request->input('parcel_id');
        $pin = $request->input('pin');
        $message = 'Here is the code to open the locker: '.$pin;

        $audit = json_encode([
            "express_id"=>$express_id,
            "message"=>$message,
        ]);
        
        Audit::create($express_id,'Delivery Activity Detail',$audit,'Show PIN');

        return json_encode('show');
    }

    public function downloadExcel(Request $request)
    {
        $expressNumber = $request->input('expressNumber', null);
        $expressType = $request->input('expressType', null);
        $location = $request->input('location', null);
        $phone = $request->input('phone', null);
        $status = $request->input('status', null);
        $pin = $request->input('pin', null);
        $parcelId = $request->input('parcelId', null);
        $country = $request->input('country', null);
        $company = $request->input('company', null);
        $courier = $request->input('courier', null);
        $importTime = $request->input('importTime', null);
        $takeTime = $request->input('takeTime', null);
        $overdueTime = $request->input('overdueTime', null);
        $dateBefore = $request->input('dateBefore', null);
        $dateAfter = $request->input('dateAfter', null);
        $isAnomaly = $request->input('isAnomaly', null);

        if($isAnomaly == '1') {
            $storeTime = null;
        } else {
            $storeTime = $request->input('storeTime', null);
        }
        $id_company = Auth::user()->id_company;
        $companies_id = $this->getCompaniesId($id_company);
        $startDate = null; $endDate = null;
        $start = null; $end = null;
        $unixStartDate = null; $unixEndDate = null;
        if (!empty($storeTime)) {
            $date = explode(" - ", $storeTime);
            $start = $date[0];
            $end = $date[1];
            $startDate = $start.' 00:00:00';
            $endDate = $end.' 23:59:59';
            $unixStartDate = strtotime($startDate) * 1000;
            $unixEndDate = strtotime($endDate) * 1000;
        }
        $unixNowDateTime = time()*1000;

        $startImportUnix = null; $endImportUnix = null;
        $startTakeUnix = null; $endTakeUnix = null;
        $startOverdueUnix = null; $endOverdueUnix = null;
        if (!empty($importTime)) { $import = $this->convertEpoch($importTime); $startImportUnix = $import['start']; $endImportUnix = $import['end']; }
        if (!empty($takeTime)) { $take = $this->convertEpoch($takeTime); $startTakeUnix = $take['start']; $endTakeUnix = $take['end']; }
        if (!empty($overdueTime)) { $overdue = $this->convertEpoch($overdueTime); $startOverdueUnix = $overdue['start']; $endOverdueUnix = $overdue['end']; }
        
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        
        // style
        $bold = [
            'font' => [
                'bold' => true,
                'size' => 20
            ]
        ];

        // title
        $sheet->mergeCells('A1:E1');
        $sheet->setCellValue('A1', 'Locker Delivery Activity List');
        $sheet->getStyle('A1')->applyFromArray($bold);
        $sheet->mergeCells('A2:E2');
        if (!empty($storeTime)) {
        $sheet->setCellValue('A2', $start.' - '.$end);
        }

        // header
        $count = 4;
        $sheet->setCellValue('A'.$count, 'No');
        $sheet->setCellValue('B'.$count, 'Parcel Number');
        $sheet->setCellValue('C'.$count, 'Type');
        $sheet->setCellValue('D'.$count, 'Location');
        $sheet->setCellValue('E'.$count, 'Locker Number');
        $sheet->setCellValue('F'.$count, 'Phone');
        $sheet->setCellValue('G'.$count, 'Courier Company');
        $sheet->setCellValue('H'.$count, 'Status');
        $sheet->setCellValue('I'.$count, 'Import Time');
        $sheet->setCellValue('J'.$count, 'Store Time');
        $sheet->setCellValue('K'.$count, 'Take Time');
        $sheet->setCellValue('L'.$count, 'Overdue Time');
        $sheet->setCellValue('M'.$count, 'Name');
        $sheet->setCellValue('N'.$count, 'IC / Passport Number');

        /*$data = Express::select('tb_newlocker_express.*','tnb.name as locker_name','tnc.company_name','tnm.numberinCabinet as locker_number','tnmt.name as locker_size','tnu.username')
        ->leftJoin('tb_newlocker_company as tnc','tnc.id_company','=','tb_newlocker_express.logisticsCompany_id')
        ->leftJoin('tb_newlocker_box as tnb','tnb.id','=','tb_newlocker_express.box_id')
        ->leftJoin('tb_newlocker_mouth as tnm','tnm.id_mouth','=','tb_newlocker_express.mouth_id')
        ->leftjoin('tb_newlocker_mouthtype as tnmt','tnmt.id_mouthtype','=','tnm.mouthType_id')
        ->leftJoin('tb_newlocker_user as tnu','tnu.id_user','=','tb_newlocker_express.storeUser_id')
        ->whereBetween('tb_newlocker_express.storetime',[$unixStartDate,$unixEndDate])
        ->when($companies_id, function ($query) use ($companies_id){
            return $query->whereIn('tb_newlocker_express.logisticCompany_id',$companies_id);
        })
        ->when($parcelId, function ($query) use ($parcelId){
            return $query->where('tb_newlocker_express.id','LIKE',"%$parcelId%");
        })
        ->when($expressNumber, function ($query) use ($expressNumber){
            return $query->where('tb_newlocker_express.expressNumber','LIKE',"%$expressNumber%")
                ->orWhere('tb_newlocker_express.stomerStoreNumber','LIKE',"%$expressNumber%");
        })
        ->when($expressType, function ($query) use ($expressType){
            // if ($expressType == 'POPSAFE') {
            //     return $query->where('tb_newlocker_express.groupName','=',"POPDEPOSIT");
            // } else {
                return $query->where('tb_newlocker_express.expressType','=',"$expressType");
            // }
        })
        ->when($status, function ($query) use ($status,$unixNowDateTime){
            if ($status == 'OVERDUE') {
                return $query->where('tb_newlocker_express.status','IN_STORE')
                    ->where('tb_newlocker_express.overdueTime','<',$unixNowDateTime);
            } else if ($status == 'IN_STORE') {
                return $query->where('tb_newlocker_express.status','IN_STORE')
                    ->where('tb_newlocker_express.overdueTime','=', null);
            } else {
                return $query->where('tb_newlocker_express.status',"$status");
            }
        })
        ->when($box_id, function ($query) use ($box_id){
            return $query->where('tb_newlocker_express.box_id','=',"$box_id");
        })
        ->when($phone, function ($query) use ($phone){
            return $query->where('tb_newlocker_express.takeUserPhoneNumber','LIKE',"%$phone%");
        })
        ->when($pin, function ($query) use ($pin){
            return $query->where('validateCode',$pin);
        })
        ->when($operator_id, function ($query) use ($operator_id){
            return $query->where('tb_newlocker_express.operator_id',$operator_id);
        })
        ->when($company, function ($query) use ($company){
            return $query->where('tnc.company_name','LIKE',"%$company%");
        })
        ->when($courier, function ($query) use ($courier){
            return $query->where('tnu.username','LIKE',"%$courier%");
        })
        ->when($importTime, function ($query) use ($startImportUnix, $endImportUnix){
            return $query->whereBetween('tb_newlocker_express.importTime',[$startImportUnix, $endImportUnix]);
        })
        ->when($takeTime, function ($query) use ($startTakeUnix, $endTakeUnix){
            return $query->whereBetween('tb_newlocker_express.takeTime',[$startTakeUnix, $endTakeUnix]);
        })
        ->when($overdueTime, function ($query) use ($startOverdueUnix, $endOverdueUnix){
            return $query->whereBetween('tb_newlocker_express.overdueTime',[$startOverdueUnix, $endOverdueUnix]);
        })
        // ->when($search, function ($query) use ($search){
        //     return $query->where('tb_newlocker_express.expressNumber', 'LIKE', '%'.$search.'%')
        //     ->orWhere('tnb.name', 'LIKE', '%'.$search.'%')
        //     ->orWhere('tnc.company_name', 'LIKE', '%'.$search.'%')
        //     ->orWhere('tb_newlocker_express.takeUserPhoneNumber', 'LIKE', '%'.$search.'%');
        // })
        ->orderBy('tb_newlocker_express.storeTime', 'desc')
        ->get();
        */

        $data = Express::with(['box','company','detail','mouth','user'])
        ->when($storeTime, function ($query) use ($unixStartDate,$unixEndDate){
            return $query->whereBetween('tb_newlocker_express.storetime',[$unixStartDate,$unixEndDate]);
        })
        ->when($id_company != '161e5ed1140f11e5bdbd0242ac110001' && $companies_id, function ($query) use ($companies_id){
            return $query->whereIn('tb_newlocker_express.logisticsCompany_id',$companies_id);
        })
        ->when($parcelId, function ($query) use ($parcelId){
            return $query->where('tb_newlocker_express.id','LIKE',"%$parcelId%");
        })
        ->when($expressNumber, function ($query) use ($expressNumber){
            // return $query->where('tb_newlocker_express.expressNumber','LIKE',"%$expressNumber%")
            //     ->orWhere('tb_newlocker_express.customerStoreNumber','LIKE',"%$expressNumber%");
            return $query->whereRaw('(tb_newlocker_express.expressNumber LIKE "%'.$expressNumber.'%" or tb_newlocker_express.customerStoreNumber LIKE "%'.$expressNumber.'%")');
        })
        ->when($expressType, function ($query) use ($expressType){
            // if ($expressType == 'POPSAFE') {
            //     return $query->where('tb_newlocker_express.expressType','=',"CUSTOMER_STORE")
            //         ->where('tb_newlocker_express.groupName','=',"POPDEPOSIT");
            // } else {
                return $query->where('tb_newlocker_express.expressType','=',"$expressType");
            // }
        })
        ->when($status, function ($query) use ($status,$unixNowDateTime){
            if ($status == 'OVERDUE') {
                return $query->where('tb_newlocker_express.status','IN_STORE')
                    ->where('tb_newlocker_express.overdueTime','<',$unixNowDateTime);
            } else if ($status == 'IN_STORE') {
                return $query->where('tb_newlocker_express.status','IN_STORE')
                    ->where('tb_newlocker_express.overdueTime','>=', $unixNowDateTime);
            } else {
                return $query->where('tb_newlocker_express.status',"$status");
            }
        })
        ->when($location, function ($query) use ($location){
            return $query->where('tb_newlocker_express.box_id','=',"$location");
        })
        ->when($phone, function ($query) use ($phone){
            return $query->where('tb_newlocker_express.takeUserPhoneNumber','LIKE',"%$phone%");
        })
        ->when($pin, function ($query) use ($pin){
            return $query->where('tb_newlocker_express.validateCode','LIKE', "%$pin%");
        })
        ->when($country, function ($query) use ($country){
            return $query->where('tb_newlocker_express.operator_id',$country);
        })
        ->when($company, function ($query) use ($company){
            return $query->whereHas('company', function ($q) use($company) {
                $q->where('tb_newlocker_company.company_name','LIKE',"%$company%");
            });
        })
        ->when($courier, function ($query) use ($courier){
            return $query->whereHas('user', function ($q) use($courier) {
                $q->where('tb_newlocker_user.username','LIKE',"%$courier%");
            });
        })
        ->when($importTime, function ($query) use ($startImportUnix, $endImportUnix){
            return $query->whereBetween('tb_newlocker_express.importTime',[$startImportUnix, $endImportUnix]);
        })
        ->when($takeTime, function ($query) use ($startTakeUnix, $endTakeUnix){
            return $query->whereBetween('tb_newlocker_express.takeTime',[$startTakeUnix, $endTakeUnix]);
        })
        ->when($overdueTime, function ($query) use ($startOverdueUnix, $endOverdueUnix){
            return $query->whereBetween('tb_newlocker_express.overdueTime',[$startOverdueUnix, $endOverdueUnix]);
        })
        ->when($isAnomaly == '1' && $dateBefore, function ($query) use ($dateBefore, $dateAfter){
            return $query->whereNotNull('tb_newlocker_express.storeTime')
            ->whereNotBetween('tb_newlocker_express.storeTime',[$dateBefore, $dateAfter]);
        })
        ->orderBy('tb_newlocker_express.storeTime', 'desc')
        ->get();
        
        if($data->isEmpty()) {
            // data kosong
            $count = $count+1;
            $sheet->mergeCells('A'.$count.':J'.$count);
            $sheet->setCellValue('A'.$count, 'No records found');
        } else { 
            // data ready
            $no = 1;
            foreach($data as $key => $row) {
                $count = $count+1;
                // expressNumber
                if($row['expressType'] == 'COURIER_STORE') {
                    $expressNumber = $row['expressNumber'];
                } else {
                    $expressNumber = $row['customerStoreNumber'];
                }

                // type
                // if(($row['expressType'] == 'CUSTOMER_STORE' || $row['expressType'] == 'COURIER_STORE') && $row['groupName'] == 'POPDEPOSIT') {
                //     $type = 'POPSAFE';
                // } else 
                if($row['expressType'] == 'COURIER_STORE') {
                    $type = 'LAST MILE';
                } else if($row['expressType'] == 'CUSTOMER_REJECT') {
                    $type = 'RETURN';
                } else if($row['expressType'] == 'CUSTOMER_STORE') {
                    $type = 'POPSEND';
                }

                // phone
                if($row['takeUserPhoneNumber'] != null) {
                    $phone = $row['takeUserPhoneNumber'];
                } else {
                    $phone = $row['storeUserPhoneNumber'];
                }

                // locker
                if($row->box != NULL) {
                    $locker_name = $row->box->name;
                } else {
                    $locker_name = '-';
                }
                if($row->mouth != NULL) {
                    $mouthtype = MouthType::where('id_mouthtype', $row->mouth->mouthType_id)->first();
                    if($mouthtype->name == 'MINI') {
                        $locker_size = 'XS';
                    } else {
                        $locker_size = $mouthtype->name;
                    }
                    $locker = $locker_size.'/'.$row->mouth->number;
                } else {
                    $locker = '';
                }
                if($row->box != NULL) {
                    $locker_name = $row->box->name;
                } else {
                    $locker_name = '';
                }
                if($row->company != NULL) {
                    $company_name = $row->company->company_name;
                } else {
                    $company_name = '';
                }

                // status
                if($row['status'] == 'IN_STORE' && $unixNowDateTime > $row['overdueTime'] && $row['expressType'] == 'COURIER_STORE') {
                    $status = 'OVERDUE';
                } else {
                    $status = $row['status'];
                }

                // name & ic number
                if($row->operator_id == '4028808357ad74f50157bc5681567b9e') {
                    if($row->detail != NULL) {
                        if($row->detail->receive_name != NULL) {
                            $name = $row->detail->receive_name;
                        } else {
                            $name = '';
                        }
                        $no_identification = "'".$row->detail->no_identification;
                    } else {
                        $name = '';
                        $no_identification = '';
                    }
                } else {
                    $name = '';
                    $no_identification = '';
                }
                
                // datetime
                $dateFormat = "Y-m-d H:i:s";
                $importTime = ''; $storeTime = ''; $takeTime = ''; $overdueTime = '';
                if(!empty($row['importTime'])) {
                    $importTime = date($dateFormat, substr($row['importTime'], 0, 10));
                }
                if(!empty($row['storeTime'])) {
                    $storeTime = date($dateFormat, substr($row['storeTime'], 0, 10));
                }
                if(!empty($row['takeTime'])) {
                    $takeTime = date($dateFormat, substr($row['takeTime'], 0, 10));
                }
                if(!empty($row['overdueTime'])) {
                    $overdueTime = date($dateFormat, substr($row['overdueTime'], 0, 10));
                }
                $sheet->setCellValue('A'.$count, $no++);
                $sheet->setCellValue('B'.$count, $expressNumber);
                $sheet->setCellValue('C'.$count, $type);
                $sheet->setCellValue('D'.$count, $locker_name);
                $sheet->setCellValue('E'.$count, $locker);
                $sheet->setCellValue('F'.$count, $phone);
                $sheet->setCellValue('G'.$count, $company_name);
                $sheet->setCellValue('H'.$count, $status);
                $sheet->setCellValue('I'.$count, $importTime);
                $sheet->setCellValue('J'.$count, $storeTime);
                $sheet->setCellValue('K'.$count, $takeTime);
                $sheet->setCellValue('L'.$count, $overdueTime);
                $sheet->setCellValue('M'.$count, $name);
                $sheet->setCellValue('N'.$count, $no_identification);
            }
        }

        $dateNow = date("ymd");
        $filename = $dateNow.' Delivery Activities.xlsx';
        
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xlsx");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'.$filename.'"');
        $writer->save("php://output");
        die();
    }

    public function blockingParcel(Request $request)
    {
        $express_id = $request->input('parcel_id');
        $validate_code = $request->input('pin');
        $is_blocking = ($request->input('blocking') == 'true') ? true : false;
        
        if($validate_code == "BLOCKED") {
            $new_code = Blocked::where('parcel_id', $express_id)->first()->old_validate_code;
        } else {
            $new_code = $validate_code;
        }

        ($is_blocking == true) ? $note = "Blocking Parcel" : $note = "Unblocking Parcel";

        $proxApiClass = new ProxApi;
        $proxApi = $proxApiClass->blockingParcel($express_id, $new_code, $is_blocking);

        $audit = [
            "before"=>json_encode([
                "express_id"=>$express_id,
                "validate_code"=>$validate_code,
                "is_block"=>$is_blocking,
            ]),
            "after"=>json_encode([
                "express_id"=>$express_id,
                "validate_code"=>$new_code,
                "is_block"=>!$is_blocking
            ])
        ];

        Audit::update($express_id,'Delivery Activity Detail',$audit,$note);
        return json_encode($proxApi);
    }

    public function changePhoneNumber(Request $request)
    {
        $parameter = $request->route()->parameters();
        if ($parameter) {
            $proxApi = new ProxApi;
            $res = $proxApi->changePhoneNumber($parameter['express_id'], $request->input('new-phone'));
            
            if (property_exists((object)$res, 'statusCode')) {
                return back()->with('failed','Failed Update Phone');
            } else {
                $request->session()->flash('success','Success Update Phone');
                return back()->with('success','Success Update Phone');
            }
        }
    }

    public function resendSms(Request $request)
    {
        $parameter = $request->route()->parameters();
        $proxApi = new ProxApi;
        $res = $proxApi->reSendSMSbyParcelId($parameter['express_id']);
            
        $express = Express::where('id',$parameter['express_id'])->first();
        $realNumber = $this->buildPhone($express->operator_id, $express->takeUserPhoneNumber);
        $content = $this->buildMessage($express);
        $audit = json_encode([
            "phone"=>$realNumber,
            "message"=>$content,
        ]);
        
        if (property_exists((object)$res, 'statusCode')) {
            return back()->with('failed','Failed Send SMS caused by '. $res->errorMessage);
        } else {
            Audit::create($parameter['express_id'],'Delivery Activity Detail',$audit,'Resend SMS');
            return back()->with('success','Success Send SMS');
        }
    }

    private function buildMessage($express) {
        $boxId = $express->box_id;
        $box = Box::where('id',$boxId)->first();
        $boxName = $box->name;
        $validateCode = $express->validateCode;
        $expressNumber = $express->expressNumber;
        $overdue = date('d/m/y H:i', $express->overdueTime/1000);
        $operator = $express->operator_id;
        $groupName = $express->groupName;
        // message
        $content = "Kode Buka : ".$validateCode." Order No: ".$expressNumber." sudah tiba di PopBox@".$boxName.". Valid s/d: ".$overdue.". CS: 02122538719";
        if($operator === '145b2728140f11e5bdbd0242ac110001') {
            if($groupName == 'COD') {
                $content = "Order Anda " . $expressNumber . " sudah sampai di Popbox@" . $boxName . ", bayar sebelum " . $overdue . " di loker/kasir untuk dapat PIN ambil barang - www.popbox.asia";
            }else{
                $templateGroup  = DB::connection('newlocker_db')->table('tb_newlocker_grouptemplate')->where('name',$groupName)->first();
                if(!is_null($templateGroup)) {
                    $template = DB::connection('newlocker_db')
                            ->table('tb_newlocker_smstemplate')
                            ->where('smsType','!=','REJECT_EXPRESS_STORE_NOTIFY_CUSTOMER')
                            ->where('templateGroup_id',$templateGroup->id)
                            ->first();
                    $contentDb = str_replace('{overdueDayOfMouth}/{overdueMouth}/17','{overdue}',$template->content);
                    $pattern = array('/{validateCode}/','/{expressNumber}/','/{boxName}/','/{overdue}/');
                    $replace = array($validateCode,$expressNumber,$boxName,$overdue);
                    $content = preg_replace($pattern,$replace, $contentDb);
                } 
            }
        }else{
            $content = "Pin Code: ".$validateCode."\nCollect your order ".$expressNumber." at PopBox Locker@".$boxName." before ".$overdue.". www.popbox.asia";
        }
        return $content;
    }

    private function buildPhone($code,$phone) {
        $area = [
            '145b2728140f11e5bdbd0242ac110001'=>'id',
            '4028808357ad74f50157bc5681567b9e'=>'my'
        ];
        $ptn = "/^0/";
        if($area[$code] === 'id') {
            $codeArea = "62";
        }else{
            $codeArea = "60";
        }
        $newPhone = preg_replace($ptn, $codeArea, $phone);
        return $newPhone;
    }
}
