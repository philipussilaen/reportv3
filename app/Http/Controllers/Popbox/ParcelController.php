<?php

namespace App\Http\Controllers\Popbox;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\NewLocker\Express;
use App\Models\Popbox\BuildingType;
use App\Models\Popbox\LockerLocation;

class ParcelController extends Controller
{
    public function getAnalytic(Request $request)
    {
        $lockerName = LockerLocation::orderBy('name','asc')->get()->pluck('name','locker_id')->toArray();
        $buildingType = BuildingType::orderBy('building_type','asc')->get()->pluck('building_type','id_building')->toArray();
        $countries = ['Indonesia', 'Malaysia'];
        $year = range(date("Y"), 2015);
        
        $data = [
            'lockerNames'   => $lockerName,
            'buildingTypes' => $buildingType,
            'countries'     => $countries,
            'year'          => $year
        ];
        return view('popbox.analytics', $data);
    }

    public function getAjaxAnalytic(Request $request)
    {
        $country = $request->country;
        $locker_id = $request->locker_id;
        if($request->year) {
            $year = $request->year;
        } else {
            $year = date("Y");
        }
        $date = $request->date;
        if($date != null) {
            $date = explode(" - ", $date);
            $startDate = date($date[0].' 00:00:00');
            $endDate = date($date[1].' 23:59:59');
        } else {
            $startDate = date($year.'-01-01 00:00:00');
            $endDate = date($year.'-12-31 23:59:59');
        }

        $count = Express::select(DB::raw('FROM_UNIXTIME(storeTime/1000, "%M") as bulan, count(tb_newlocker_express.id) as count_id, count(distinct(takeUserPhoneNumber)) as count_take_user, count(distinct(storeUserPhoneNumber)) as count_store_user'), DB::raw('FROM_UNIXTIME(storeTime/1000, "%Y") as year'))
        ->leftJoin('tb_newlocker_box as tnb','tb_newlocker_express.box_id','=','tnb.id')
        ->when($year, function($query) use($startDate, $endDate){
            return $query->whereBetween(DB::raw('FROM_UNIXTIME(storeTime/1000)'), [$startDate, $endDate]);
        })
        ->when($country, function($query) use($country){
            if($country == 'Indonesia') {
                return $query->where('tnb.operator_id', '145b2728140f11e5bdbd0242ac110001');
            } else {
                return $query->where('tnb.operator_id', '4028808357ad74f50157bc5681567b9e');
            }
        })
        ->when($locker_id, function($query) use($locker_id){
            return $query->where('tb_newlocker_express.box_id', $locker_id);
        })
        ->groupBy(DB::raw('FROM_UNIXTIME(storeTime/1000, "%m")'), DB::raw('FROM_UNIXTIME(storeTime/1000, "%Y")'))
        ->orderBy(DB::raw('FROM_UNIXTIME(storeTime/1000, "%Y")'),"ASC")
        ->orderBy(DB::raw('FROM_UNIXTIME(storeTime/1000, "%m")'),"ASC")
        ->get();

        $cat = []; $catData = []; $percent = []; $growth = []; 
        $sumUser = 0;
        foreach($count as $key => $c) {
            $cat[] = $c['bulan'].' '.$c['year'];
            $catData[] = $c['count_id'];
            $takeUser[] = $c['count_take_user'];
            $storeUser[] = $c['count_store_user'];
            $sumUser = $sumUser + $c['count_take_user'] + $c['count_store_user'];

            if($key == 0) {
                $allUsageData[] = [
                    'y' => $c['count_id'],
                    'growth' => 0
                ];
                $percent[] = [
                    'y'     => 0,
                    'growth'=> 0
                ];
                $growth[] = 0;
            } else {
                $growth[] = round(($c['count_id']- $catData[$key-1])/$catData[$key-1]*100, 2);
                $percentage = round($percent[$key-1]['y'] + $growth[$key], 2);
                $allUsageData[] = [
                    'y' => $c['count_id'],
                    'growth' => $growth[$key]
                ];
                $percent[] = [
                    'y' => $percentage,
                    'growth' => $growth[$key]
                ];
            }
        }

        $countWeekly = Express::select(DB::raw('WEEK(FROM_UNIXTIME(storeTime/1000, "%Y-%m-%d")) as bulan, count(tb_newlocker_express.id) as count_id'))
        ->leftJoin('tb_newlocker_box as tnb','tb_newlocker_express.box_id','=','tnb.id')
        ->when($year, function($query) use($startDate, $endDate){
            return $query->whereBetween(DB::raw('FROM_UNIXTIME(storeTime/1000)'), [$startDate, $endDate]);
        })
        ->when($country, function($query) use($country){
            if($country == 'Indonesia') {
                return $query->where('tnb.operator_id', '145b2728140f11e5bdbd0242ac110001');
            } else {
                return $query->where('tnb.operator_id', '4028808357ad74f50157bc5681567b9e');
            }
        })
        ->when($locker_id, function($query) use($locker_id){
            return $query->where('tb_newlocker_express.box_id', $locker_id);
        })
        ->groupBy(DB::raw('WEEK(FROM_UNIXTIME(storeTime/1000, "%Y-%m-%d"))'))
        ->get();

        $catWeekly = []; $catDataWeekly = []; $percentWeekly = []; $growthWeekly = [];
        foreach($countWeekly as $key => $c) {
            $catWeekly[] = $c['bulan'];
            $dataWeekly[] = $c['count_id'];

            if($key == 0) {
                $catDataWeekly[] = [
                    'y' => $c['count_id'],
                    'growth' => 0
                ];
                $percentWeekly[] = [
                    'y'     => 0,
                    'growth'=> 0
                ];
                $growthWeekly[] = 0;
            } else {
                $growthWeekly[] = round(($c['count_id']- $dataWeekly[$key-1])/$dataWeekly[$key-1]*100, 2);
                $percentage = round($percentWeekly[$key-1]['y'] + $growthWeekly[$key], 2);
                $catDataWeekly[] = [
                    'y' => $c['count_id'],
                    'growth' => $growthWeekly[$key]
                ];
                $percentWeekly[] = [
                    'y' => $percentage,
                    'growth' => $growthWeekly[$key]
                ];
            }
        }

        $countDaily = Express::select(DB::raw('FROM_UNIXTIME(storeTime/1000, "%Y-%m-%d") as day, count(tb_newlocker_express.id) as count_id'))
        ->leftJoin('tb_newlocker_box as tnb','tb_newlocker_express.box_id','=','tnb.id')
        ->when($year, function($query) use($startDate, $endDate){
            return $query->whereBetween(DB::raw('FROM_UNIXTIME(storeTime/1000)'), [$startDate, $endDate]);
        })
        ->when($country, function($query) use($country){
            if($country == 'Indonesia') {
                return $query->where('tnb.operator_id', '145b2728140f11e5bdbd0242ac110001');
            } else {
                return $query->where('tnb.operator_id', '4028808357ad74f50157bc5681567b9e');
            }
        })
        ->when($locker_id, function($query) use($locker_id){
            return $query->where('tb_newlocker_express.box_id', $locker_id);
        })
        ->groupBy(DB::raw('FROM_UNIXTIME(storeTime/1000, "%Y-%m-%d")'))
        ->get();

        $catDaily = []; $catDataDaily = []; $percentDaily = []; $growthDaily = [];
        foreach($countDaily as $key => $c) {
            $catDaily[] = $c['day'];
            $dataDaily[] = $c['count_id'];

            if($key == 0) {
                $catDataDaily[] = [
                    'y' => $c['count_id'],
                    'growth' => 0
                ];
                $percentDaily[] = [
                    'y'     => 0,
                    'growth'=> 0
                ];
                $growthDaily[] = 0;
            } else {
                $growthDaily[] = round(($c['count_id']- $dataDaily[$key-1])/$dataDaily[$key-1]*100, 2);
                $percentage = round($percentDaily[$key-1]['y'] + $growthDaily[$key], 2);
                $catDataDaily[] = [
                    'y' => $c['count_id'],
                    'growth' => $growthDaily[$key]
                ];
                $percentDaily[] = [
                    'y' => $percentage,
                    'growth' => $growthDaily[$key]
                ];
            }
        }
        
        $userData = array_merge(array_intersect($takeUser,$storeUser), array_diff($takeUser,$storeUser));
        foreach($userData as $key => $row) {
            if($key == 0) {
                $userPercent[] = 0;
            } else {
                $count = round(($row - $userData[$key-1])/$userData[$key-1]*100, 2);
                $userPercent[] = round($userPercent[$key-1] + $count, 2);
            }
        }

        $dataTopLocation = Express::select('tnb.name', DB::raw('count(tb_newlocker_express.box_id) as jumlah'))
        ->leftJoin('tb_newlocker_box as tnb','tb_newlocker_express.box_id','=','tnb.id')
        ->when($year, function($query) use($startDate, $endDate){
            return $query->whereBetween(DB::raw('FROM_UNIXTIME(storeTime/1000)'), [$startDate, $endDate]);
        })
        ->when($country,function($query) use($country){
            if($country == 'Indonesia') {
                return $query->where('tnb.operator_id', '145b2728140f11e5bdbd0242ac110001');
            } else {
                return $query->where('tnb.operator_id', '4028808357ad74f50157bc5681567b9e');
            }
        })
        ->groupBy('tb_newlocker_express.box_id')
        ->orderBy(DB::raw('count(tb_newlocker_express.box_id)'), 'desc')
        ->limit(10)
        ->get();

        $topLocation = []; 
        foreach($dataTopLocation as $key => $row) {
            $topLocation[$key] = array(
                'label' => $row->name,
                'value'=> $row->jumlah
            );

            $lockerName[] = $row->name;
            $highestParcelByLocation[] = $row->jumlah;
        }

        $dataBottomLocation = Express::select('tnb.name', DB::raw('count(tb_newlocker_express.box_id) as jumlah'))
        ->leftJoin('tb_newlocker_box as tnb','tb_newlocker_express.box_id','=','tnb.id')
        ->when($year, function($query) use($startDate, $endDate){
            return $query->whereBetween(DB::raw('FROM_UNIXTIME(storeTime/1000)'), [$startDate, $endDate]);
        })
        ->when($country,function($query) use($country){
            if($country == 'Indonesia') {
                return $query->where('tnb.operator_id', '145b2728140f11e5bdbd0242ac110001');
            } else {
                return $query->where('tnb.operator_id', '4028808357ad74f50157bc5681567b9e');
            }
        })
        ->groupBy('tb_newlocker_express.box_id')
        ->orderBy(DB::raw('count(tb_newlocker_express.box_id)'), 'asc')
        ->limit(10)
        ->get();

        $bottomLocation = []; 
        foreach($dataBottomLocation as $key => $row) {
            $bottomLockerName[] = $row->name;
            $lowestParcelByLocation[] = $row->jumlah;
        }
        
        /* $express = Express::select('tnb.id','tnb.name',DB::raw('COUNT(tb_newlocker_express.box_id) as jumlah'))
        ->join('tb_newlocker_box as tnb','tb_newlocker_express.box_id','=','tnb.id')
        ->when($year, function($query) use($startDate, $endDate){
            return $query->whereBetween(DB::raw('FROM_UNIXTIME(storeTime/1000)'), [$startDate, $endDate]);
        })
        ->when($country,function($query) use($country){
            if($country == 'Indonesia') {
                return $query->where('tnb.operator_id', '145b2728140f11e5bdbd0242ac110001');
            } else {
                return $query->where('tnb.operator_id', '4028808357ad74f50157bc5681567b9e');
            }
        })
        ->groupBy('tb_newlocker_express.box_id')
        ->orderBy(DB::raw('COUNT(tb_newlocker_express.box_id)'), 'desc')
        ->get()->toArray();
        
        foreach($express as $key => $row) {
            $express[$key]['type'] = LockerLocation::getBuildingType($row['id']);
        } */

        // dd(array_diff($express[]['type']));
        // $buildingType = BuildingType::all();
        // dd($allUsageData);
        $dataGraph = [
            'topLocation' => $lockerName,
            'parcelByLocation' => $highestParcelByLocation,
            'bottomLocation' => $bottomLockerName,
            'lowestParcelByLocation' => $lowestParcelByLocation,
            'category' => $cat,
            'categoryData' => $catData,
            'categoryPercentage' => $percent,
            'categoryGrowth' => $growth,
            'allUsageData' => $allUsageData,

            'categoryWeekly' => $catWeekly,
            'categoryDataWeekly' => $catDataWeekly,
            'categoryPercentageWeekly' => $percentWeekly,
            'categoryGrowthWeekly' => $growthWeekly,

            'categoryDaily' => $catDaily,
            'categoryDataDaily' => $catDataDaily,
            'categoryPercentageDaily' => $percentDaily,
            'categoryGrowthDaily' => $growthDaily,

            'userData' => $userData,
            'userPercent' => $userPercent
        ];

        echo json_encode($dataGraph);
    }
}
