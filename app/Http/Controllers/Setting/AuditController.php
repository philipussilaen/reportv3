<?php

namespace App\Http\Controllers\Setting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Audit;
use App\User;
use App\Http\Resources\AuditResource;
use DB;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Carbon\Carbon;

class AuditController extends Controller
{
    public function index() {
        $type = [
            'create',
            'read',
            'update',
            'delete'
        ];
        return view('setting.audit',compact('type'));
    }

    public function getUser(Request $request) {
        if ($request->has('search')) {
                $search = $request->input('search');
                $users = User::where('name', 'like', '%' . $search . '%')->get();
        }else{
            $users = User::all();
        }
        $list [] = [
            "id"=>'',
            "text"=>'All'
        ];
        foreach($users as $user) {
            $list[$user->id] = [
                "id"=>$user->id,
                "text"=>$user->name
            ];
        }
        return $list;
    }

    public function detail($id) {
        $model = Audit::find($id); 
        return new AuditResource($model);
    }

    public function source(Request $request) {
        $limit = isset($request->length) ? $request->length : 10; 
        $page =  ( $request->start / $limit ) + 1;
        $rangeDate = $request->date;
        $startDate = null;
        $endDate = null;
        if (!empty($rangeDate)) {
            $date = explode(" - ", $rangeDate);
            $startDate = $date[0].' 00:00:00';
            $endDate = $date[1].' 23:59:59';
        }
        $filter = [
            "key"=>$request->key,
            "module"=>$request->module,
            "type"=>$request->type,
            "user_id"=>$request->user_id,
        ];
        $filter = array_filter($filter);
        $total = Audit::all()->count();
        $db = Audit::where($filter)->orderBy('created_at','desc');
            if(!empty($rangeDate)) {
                $db->whereBetween('created_at',[$startDate, $endDate]);
            };
        $result = $db->paginate($limit,['*'],'draw',$page);
        
        $data = AuditResource::collection($result);
        $collection = [
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => $total,  
            "recordsFiltered" => $result->total(), 
            "data"            => $data   
        ];
        return response()->json($collection, 200);
    }

    public function excel(Request $request) {
        $rangeDate = $request->daterange;
        $startDate = null;
        $endDate = null;
        if (!empty($rangeDate)) {
            $date = explode(" - ", $rangeDate);
            $startDate = $date[0].' 00:00:00';
            $endDate = $date[1].' 23:59:59';
        }
        $filter = [
            "key"=>$request->key,
            "module"=>$request->module,
            "type"=>$request->type,
            "user_id"=>$request->user,
        ];
        $filter = array_filter($filter);
        // dd($filter,$rangeDate);
        $db = Audit::where($filter)->orderBy('created_at','desc');
            if(!empty($rangeDate)) {
                $db->whereBetween('created_at',[$startDate, $endDate]);
            };
        $data = $db->get();
        
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        // style
        $bold = [
            'font' => [
                'bold' => true,
                'size' => 20
            ]
        ];

        // title
        $sheet->mergeCells('A1:E1');
        $sheet->setCellValue('A1', 'Audit Trails');
        $sheet->getStyle('A1')->applyFromArray($bold);
        $sheet->mergeCells('A2:E2');
        if (!empty($transactionDate)) {
        $sheet->setCellValue('A2', $start.' - '.$end);
        }
         // header
        $count = 4;
        $sheet->setCellValue('A'.$count, 'No');
        $sheet->setCellValue('B'.$count, 'Module');
        $sheet->setCellValue('C'.$count, 'Key');
        $sheet->setCellValue('D'.$count, 'Type');
        $sheet->setCellValue('E'.$count, 'Before');
        $sheet->setCellValue('F'.$count, 'After');
        $sheet->setCellValue('G'.$count, 'User');
        $sheet->setCellValue('H'.$count, 'Created At');

        if($data->count() > 0) {
            $no = 1;
            foreach($data as $row) {
                $count = $count+1;
                $sheet->setCellValue('A'.$count, $no++);
                $sheet->setCellValue('B'.$count, $row['module']);
                $sheet->setCellValue('C'.$count, $row['key']);
                $sheet->setCellValue('D'.$count, ucfirst($row['type']));
                $sheet->setCellValue('E'.$count, $row['before']);
                $sheet->setCellValue('F'.$count, $row['after']);
                $sheet->setCellValue('G'.$count, $row['username']);
                $sheet->setCellValue('H'.$count, $row['created_at']);
            } 
        }else{
            $count = $count+1;
            $sheet->mergeCells('A'.$count.':Q'.$count);
            $sheet->setCellValue('A'.$count, 'No records found');
        }

        $dateNow = date("ymd");
        $filename = $dateNow.' Audit-trails.xlsx';
        
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xlsx");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'.$filename.'"');
        $writer->save("php://output");
    }
}
