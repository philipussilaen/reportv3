<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\Route;
use App\Models\AccessMenu;
use App\Models\UserRoute;


class RouteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['list'] = Route::all();
        return view('route.route', ['list' => $data['list']]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $menu = Route::getAllMenu();
        return view('route.create', compact('menu'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:255',
            'route' => 'required',
            'route_name' => 'required',
            'parent_id' => 'required',
            'type' => 'required'
        ]);

        if($validatedData) {
            $data = new Route();
            
            $data->name = $request->name;
            $data->route = $request->route;
            $data->route_name = $request->route_name;
            $data->icon = $request->icon;
            $data->parent_id = $request->parent_id;
            $data->type = $request->type;
            $data->description = $request->description;
            $data->save();
        }
		return redirect('route')->with(['success'=> 'New route has been added']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function edit($id)
    // {
    //     $data = Route::where('id', $id)->first();
    //     $menu = Route::getAllMenu();

    //     return view('route.edit', compact('data', 'menu'));
    // }
    public function edit(Request $request)
    {
        $id = $request->input('id',null);
        $data = Route::where('id', $id)->first();
        $menu = Route::getAllMenu();

        return view('route.edit', compact('data', 'menu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = $request->input('id',null);
        $data = Route::where('id', $id)->first();
        $data->name = $request->name;
        $data->route = $request->route;
        $data->route_name = $request->route_name;
        $data->icon = $request->icon;
        $data->parent_id = $request->parent_id;
        $data->type = $request->type;
        $data->description = $request->description;
        $data->save();
        return redirect('route')->with(['success'=> 'Route detail has been changed']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $id = $request->input('id',null);
        Route::where('id', $id)->delete();
        return redirect('route')->with(['success'=> 'Route has been deleted']);
    }

    public function getAllRoute()
    {
        $routes = Route::all();
        $data = [];
        foreach($routes AS $key => $row) {
            $data = array(
                'id' => $row->id ,
                'name' => $row->name,
                'type' => $row->type
            );
        }
        $datas = json_encode($data);
    }
}
