<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;

class CheckAccessController extends Controller
{
    public function checkAccess(Request $request)
    {
        $response = new \stdClass();
        $response->hasAccess = false;
        $baseURL = URL::to('/');

        $url = str_replace($baseURL, '', $request->input('url'));
        
        $user_id = Auth::user()->id;
        $route = \DB::table('user_route')
        ->where('u_id', $user_id)
        ->get()->toArray();
        
        foreach ($route as $key => $value) {
            if(preg_match("/\{(.*)\}/", $value->route, $matches)) {
                $check = str_replace($matches, '', $value->route);
                $routes[] = str_replace('/', '', $check);
            } else {
                $routes[] = str_replace('/', '', $value->route);
            }
        }

        $url = str_replace('/', '', $url);
        
        if (!empty($user_id) && !in_array($url, $routes)) {
            return json_encode($response);
        }
        $response->hasAccess = true;        
        return json_encode($response);
    }
}
