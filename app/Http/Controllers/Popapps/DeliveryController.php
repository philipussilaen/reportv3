<?php

namespace App\Http\Controllers\Popapps;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Popapps\Delivery;
use App\Models\Popapps\DeliveryHistory;
use App\Models\Popapps\DeliveryDestination;
use App\Models\Popapps\DeliveryDestinationHistory;
use App\Models\Popapps\Transaction;
use App\Models\Popapps\User;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class DeliveryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $name = User::orderBy('name','asc')->pluck('name');
        $status = Delivery::distinct()->orderBy('status','asc')->pluck('status');
        $pickup_type = ['Address','Locker'];
        $location = Delivery::distinct()->orderBy('origin_name','asc')->pluck('origin_name');

        $data = [
            'name' => $name,
            'status' => $status,
            'pickup_type' => $pickup_type,
            'location' => $location
        ];
        return view('popapps.delivery.delivery', $data);
    }

    public function getAjaxServerDeliveryList(Request $request)
    {
        // Columns Data
        $columns = array( 
            0 =>'invoice_code', 
            1 =>'status',
            2=> 'name',
            3=> 'origin_name',
            4=> 'pickup_type',
            5=> 'destination_count',
            6=> 'created_at',
            7=> 'total_price',
            8=> 'promo_amount',
            9=> 'paid_amount',
            10=> 'code'
        );

        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $limit = isset($request->length) ? $request->length : 10;
        $currentPage = isset($start) ? ($start/$limit)+1 : 1;
        
        $invoice_code = $request->invoice_code;
        $location = $request->location;
        $status = $request->status;
        $pickup_type = $request->pickup_type;
        $name = $request->name;
        $phone = $request->phone;
        $voucher = $request->voucher;
        $created_at = $request->created_at;

        $startDate = null;
        $endDate = null;
        if (!empty($created_at)) {
            $date = explode(" - ", $created_at);
            $startDate = $date[0].' 00:00:00';
            $endDate = $date[1].' 23:59:59';
        }
        
        $totalData = Delivery::with(['user','destinations','delivery_transaction','delivery_transaction.campaign_usage','delivery_transaction.campaign_usage.campaign','delivery_transaction.campaign_usage.campaign.voucher'])->count();
        $allData = Delivery::with(['user','destinations','delivery_transaction','delivery_transaction.campaign_usage','delivery_transaction.campaign_usage.campaign','delivery_transaction.campaign_usage.campaign.voucher'])
        ->when($invoice_code,function($query) use($invoice_code){
            return $query->where('invoice_code','LIKE',"%$invoice_code%")
            ->orWhereHas('destinations', function($q) use($invoice_code) {
                $q->where('invoice_sub_code','LIKE',"%$invoice_code%");
            });
        })
        ->when($location,function($query) use($location){
            return $query->where('origin_name','LIKE',"%$location%");
        })
        ->when($status,function($query) use($status){
            return $query->where('status',"$status");
        })
        ->when($pickup_type,function($query) use($pickup_type){
            return $query->where('pickup_type',"$pickup_type");
        })
        ->when($created_at, function ($query) use ($startDate, $endDate){
            return $query->whereBetween('created_at', [$startDate, $endDate]);
        })
        ->when($name,function($query) use($name){
            return $query->whereHas('user', function ($q) use($name) {
                $q->where('name','LIKE',"%$name%");
            });
        })
        ->when($phone,function($query) use($phone){
            return $query->whereHas('user', function ($q) use($phone) {
                $q->where('phone','LIKE',"%$phone%");
            });
        })
        ->when($voucher,function($query) use($voucher){
            return $query->whereHas('delivery_transaction.campaign_usage.campaign.voucher', function ($q) use($voucher) {
                $q->where('code','LIKE',"%$voucher%");
            });
        })
        ->orderBy($order, $dir)
        ->paginate($limit,['*'],'draw',$currentPage);

        $data = array();
        if(!empty($allData->items()))
        {
            foreach ($allData->items() as $post)
            {
                $nestedData['invoice_code'] = $post->invoice_code;
                $nestedData['delivery_status'] = $post->status;
                $nestedData['origin_name'] = $post->origin_name;
                $nestedData['pickup_type'] = $post->pickup_type;
                $nestedData['destination_count'] = Delivery::countDestination($post->invoice_code);
                $nestedData['delivery_date'] = $post->created_at->toDateTimeString();
                

                if($post->delivery_transaction != null) {
                    $nestedData['invoice_id'] = $post->delivery_transaction->invoice_id;
                    $nestedData['total_price'] = $post->delivery_transaction->total_price;
                    $nestedData['promo_amount'] = $post->delivery_transaction->promo_amount;
                    $nestedData['paid_amount'] = $post->delivery_transaction->paid_amount;
                } else {
                    $nestedData['invoice_id'] = '';
                    $nestedData['total_price'] = '';
                    $nestedData['promo_amount'] = '';
                    $nestedData['paid_amount'] = '';
                }

                if($post->user != null) {
                    $nestedData['name'] = $post->user->name;
                    $nestedData['phone'] = $post->user->phone;
                    $nestedData['member_id'] = $post->user->member_id;
                } else {
                    $nestedData['name'] = '';
                    $nestedData['phone'] = '';
                    $nestedData['member_id'] = '';
                }

                if($post->delivery_transaction->campaign_usage != null) {
                    $nestedData['code'] = $post->delivery_transaction->campaign_usage->campaign->voucher[0]->code;
                } else {
                    $nestedData['code'] = '';
                }

                $data[] = $nestedData;
            }
        }
        $totalFiltered = $allData->total();
          
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );
            
        echo json_encode($json_data);
    }

    public function getDetail(Request $request, $invoice_code=null, $invoice_id=null)
    {
        // $invoice_code = $request->input('code',null);
        // $invoice_id = $request->input('id',null);
        
        $data = Delivery::getByInvoiceCode($invoice_code)->first();
        // dd($data);
        if($invoice_id != null) {
            $activeTransaction = $invoice_id;
        } else {
            $activeTransaction = '';
        }

        $countDestination = Delivery::countDestination($invoice_code);
        $destinations = DeliveryDestination::getByDeliveryId($data->delivery_id);
        
        $histories = [];
        foreach ($destinations as $des) {
            $histories[] =  array (
                'destination_id' => $des->id,
                'records' => DeliveryDestinationHistory::getByDeliveryDestinationId($des->id)
            );
        }
        // dd($histories);
        $transactions = Transaction::getByDeliveryId($data->delivery_id);
        // dd($transactions);

        return view('popapps.delivery.detail', compact('data','countDestination','destinations','histories','transactions','activeTransaction'));
    }

    public function downloadExcel (Request $request)
    {
        $invoice_code = $request->invoice_code;
        $location = $request->location;
        $status = $request->status;
        $pickup_type = $request->pickup_type;
        $name = $request->name;
        $phone = $request->phone;
        $voucher = $request->voucher;
        $created_at = $request->created_at;
        $startDate = '';
        $endDate = '';

        if (!empty($created_at)) {
            $date = explode(" - ", $created_at);
            $startDate = $date[0]." 00:00:00";
            $endDate = $date[1]." 23:59:59";
            $start = date("d M Y", strtotime($date[0]));
            $end = date("d M Y", strtotime($date[1]));
        }
        
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        // style
        $bold = [
            'font' => [
                'bold' => true,
                'size' => 20
            ]
        ];
        
        // title
        $sheet->mergeCells('A1:E1');
        $sheet->setCellValue('A1', 'Order List');
        $sheet->getStyle('A1')->applyFromArray($bold);
        $sheet->mergeCells('A2:E2');
        if (!empty($created_at)) {
        $sheet->setCellValue('A2', $start.' - '.$end);
        }
        // header
        $count = 4;
        $sheet->setCellValue('A'.$count, 'No');
        $sheet->setCellValue('B'.$count, 'Invoice ID');
        $sheet->setCellValue('C'.$count, 'Invoice ID');
        $sheet->setCellValue('D'.$count, 'Status');
        $sheet->setCellValue('E'.$count, 'Name');
        $sheet->setCellValue('F'.$count, 'Phone');
        $sheet->setCellValue('G'.$count, 'Location');
        $sheet->setCellValue('H'.$count, 'Pickup Type');
        $sheet->setCellValue('I'.$count, 'Delivery Date');
        $sheet->setCellValue('J'.$count, 'Price');
        $sheet->setCellValue('K'.$count, 'Promo Amount');
        $sheet->setCellValue('L'.$count, 'Paid Amount');
        $sheet->setCellValue('M'.$count, 'Promo Code');

        $data = Delivery::with(['user','delivery_transaction','delivery_transaction.campaign_usage','delivery_transaction.campaign_usage.campaign','delivery_transaction.campaign_usage.campaign.voucher'])
        ->when($invoice_code,function($query) use($invoice_code){
            return $query->where('invoice_code','LIKE',"%$invoice_code%")
            ->orWhereHas('destinations', function($q) use($invoice_code) {
                $q->where('invoice_sub_code','LIKE',"%$invoice_code%");
            });
        })
        ->when($location,function($query) use($location){
            return $query->where('origin_name','LIKE',"%$location%");
        })
        ->when($status,function($query) use($status){
            return $query->where('status',"$status");
        })
        ->when($pickup_type,function($query) use($pickup_type){
            return $query->where('pickup_type',"$pickup_type");
        })
        ->when($created_at, function ($query) use ($startDate, $endDate){
            return $query->whereBetween('created_at', [$startDate, $endDate]);
        })
        ->when($name,function($query) use($name){
            return $query->whereHas('user', function ($q) use($name) {
                $q->where('name','LIKE',"%$name%");
            });
        })
        ->when($phone,function($query) use($phone){
            return $query->whereHas('user', function ($q) use($phone) {
                $q->where('phone','LIKE',"%$phone%");
            });
        })
        ->when($voucher,function($query) use($voucher){
            return $query->whereHas('delivery_transaction.campaign_usage.campaign.voucher', function ($q) use($voucher) {
                $q->where('code','LIKE',"%$voucher%");
            });
        })
        ->orderBy('created_at','asc')
        ->get();
        
        if($data->isEmpty()) {
            // data kosong
            $count = $count+1;
            $sheet->mergeCells('A'.$count.':M'.$count);
            $sheet->setCellValue('A'.$count, 'No records found');
        } else { 
            // data ready
            $no = 1;
            foreach($data as $key => $row) {
                $count = $count+1;

                $sheet->setCellValue('A'.$count, $no++);
                $sheet->setCellValue('B'.$count, $row['invoice_code']);
                $sheet->setCellValue('C'.$count, $row['delivery_transaction']['invoice_id']);
                $sheet->setCellValue('D'.$count, $row['status']);
                $sheet->setCellValue('E'.$count, $row['user']['name']);
                $sheet->setCellValue('F'.$count, $row['user']['phone']);
                $sheet->setCellValue('G'.$count, $row['origin_name']);
                $sheet->setCellValue('H'.$count, $row['pickup_type']);
                $sheet->setCellValue('I'.$count, $row['created_at']);
                $sheet->setCellValue('J'.$count, $row['delivery_transaction']['total_price']);
                $sheet->setCellValue('K'.$count, $row['delivery_transaction']['promo_amount']);
                $sheet->setCellValue('L'.$count, $row['delivery_transaction']['paid_amount']);
                $sheet->setCellValue('M'.$count, $row['delivery_transaction']['campaign_usage']['campaign']['voucher'][0]['code']);
            }
        }

        $dateNow = date("ymd");
        $filename = $dateNow.' PopSend List.xlsx';
        
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xlsx");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'.$filename.'"');
        $writer->save("php://output");
        die();
    }
}
