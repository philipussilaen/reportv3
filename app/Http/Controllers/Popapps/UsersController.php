<?php

namespace App\Http\Controllers\Popapps;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Popapps\BalanceRecord;
use App\Models\Popapps\Transaction;
use App\Models\Popapps\User;
use App\Models\Popbox\Country;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Illuminate\Support\Facades\DB;

class UsersController extends Controller
{
    public function index()
    {
        $id = User::where('country', 'ID')->count();
        $my = User::where('country', 'MY')->count();
        $countries = Country::all();
        $status = ['ACTIVE','DISABLED','PENDING'];
        return view('popapps.users.list', compact('id', 'my', 'countries', 'status'));
    }

    public function getAjaxServerUserList(Request $request)
    {
        $columns = array(
            0 => 'member_id',
            1 => 'name',
            2 => 'phone',
            3 => 'email',
            4 => 'country',
            5 => 'balance',
            6 => 'created_at',
            7 => 'last_transaction',
            8 => 'referral_code',
            9 => 'favorite_locker',
            10=> 'status'
        );

        $memberId = $request->input('phone', null);
        $name = $request->input('name', null);
        $email = $request->input('email', null);
        $phone = $request->input('phone', null);
        $referralCode = $request->input('referralCode', null);
        $country = $request->input('country', null);
        $status = $request->input('status', null);

        if(!empty($request->input('columns.0.search.value'))) {
            $memberId = $request->input('columns.0.search.value');
        }

        if(!empty($request->input('columns.1.search.value'))) {
            $name = $request->input('columns.1.search.value');
        }

        if(!empty($request->input('columns.2.search.value'))) {
            $phone = $request->input('columns.2.search.value');
        }

        if(!empty($request->input('columns.3.search.value'))) {
            $email = $request->input('columns.3.search.value');
        }

        if(!empty($request->input('columns.4.search.value'))) {
            $country = $request->input('columns.4.search.value');
        }

        if(!empty($request->input('columns.6.search.value'))) {
            $registerDate = $request->input('columns.6.search.value');
        } else {
            $registerDate = null;//date('Y-m')."-01 - ".date('Y-m-d');
        }

        if(!empty($request->input('columns.8.search.value'))) {
            $referralCode = $request->input('columns.8.search.value');
        }

        if(!empty($request->input('columns.10.search.value'))) {
            $status = $request->input('columns.10.search.value');
        }

        $startDate = null;
        $endDate = null;
        if (!empty($registerDate)) {
            $date = explode(" - ", $registerDate);
            $startDate = $date[0].' 00:00:00';
            $endDate = $date[1].' 23:59:59';
        }

        // $allData = User::all();
        $allData = User::select('users.*','t.created_at as last_transaction')
        ->leftJoin(DB::raw('(select user_id, created_at from popsend.transactions order by created_at desc) as t'),'users.id','=','t.user_id')
        ->groupBy('users.id')
        ->get();
        $totalData = count($allData);

        // kondisi limit ketika show all data
        if($request->input('length') == -1) {
            $limit = $totalData;
        } else {
            $limit = $request->input('length');
        }

        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $search = $request->input('search.value');

        if(empty($search) && empty($request->input('columns.0.search.value')) && empty($request->input('columns.1.search.value')) && empty($request->input('columns.2.search.value')) && empty($request->input('columns.3.search.value')) && empty($request->input('columns.4.search.value')) && empty($request->input('columns.6.search.value')) && empty($request->input('columns.8.search.value')) && empty($request->input('columns.10.search.value'))) {
            $posts = User::select('users.*','t.created_at as last_transaction')
            ->leftJoin(DB::raw('(select user_id, created_at from popsend.transactions order by created_at desc) as t'),'users.id','=','t.user_id')
            ->when($startDate != null && $endDate != null, function ($query) use ($startDate, $endDate){
                return $query->whereBetween('users.created_at', [$startDate, $endDate]);
            })
            ->groupBy('users.id')
            ->limit($limit)
            ->offset($start)
            ->orderBy($order, $dir)
            ->get();

            $allFiltered = User::select('users.*','t.created_at as last_transaction')
            ->leftJoin(DB::raw('(select user_id, created_at from popsend.transactions order by created_at desc) as t'),'users.id','=','t.user_id')
            ->when($startDate != null && $endDate != null, function ($query) use ($startDate, $endDate){
                return $query->whereBetween('users.created_at', [$startDate, $endDate]);
            })
            ->groupBy('users.id')
            ->get();

            $totalFiltered = count($allFiltered);
        } else {
            $posts = User::select('users.*','t.created_at as last_transaction')
            ->leftJoin(DB::raw('(select user_id, created_at from popsend.transactions order by created_at desc) as t'),'users.id','=','t.user_id')
            ->when($memberId != null, function ($query) use ($memberId){
                return $query->where('member_id', 'LIKE', '%'.$memberId.'%');
            })
            ->when($name != null, function ($query) use ($name){
                return $query->where('name', 'LIKE', '%'.$name.'%');
            })
            ->when($email != null, function ($query) use ($email){
                return $query->where('email', 'LIKE', '%'.$email.'%');
            })
            ->when($phone != null, function ($query) use ($phone){
                return $query->where('phone', 'LIKE', '%'.$phone.'%');
            })
            ->when($country != null, function ($query) use ($country){
                if($country == "Indonesia") {
                    return $query->where('country', 'ID');
                } else {
                    return $query->where('country', 'MY');
                }
            })
            ->when($registerDate != null, function ($query) use ($startDate, $endDate){
                return $query->whereBetween('users.created_at', [$startDate, $endDate]);
            })
            ->when($referralCode != null, function ($query) use ($referralCode){
                return $query->where('referral_code', 'LIKE', '%'.$referralCode.'%');
            })
            ->when($status != null, function ($query) use ($status){
                return $query->where('status', '=', $status);
            })
            ->when($search != null, function ($query) use ($search){
                return $query->where('name', 'LIKE', '%'.$search.'%')
                ->orWhere('member_id', 'LIKE', '%'.$search.'%')
                ->orWhere('email', 'LIKE', '%'.$search.'%')
                ->orWhere('phone', 'LIKE', '%'.$search.'%')
                ->orWhere('referral_code', 'LIKE', '%'.$search.'%')
                ->orWhere('favorite_locker', 'LIKE', '%'.$search.'%');
            })
            ->groupBy('users.id')
            ->limit($limit)
            ->offset($start)
            ->orderBy($order, $dir)
            ->get();

            $totalFiltered = User::when($memberId != null, function ($query) use ($memberId){
                return $query->where('member_id', 'LIKE', '%'.$memberId.'%');
            })
            ->when($name != null, function ($query) use ($name){
                return $query->where('name', 'LIKE', '%'.$name.'%');
            })
            ->when($email != null, function ($query) use ($email){
                return $query->where('email', 'LIKE', '%'.$email.'%');
            })
            ->when($phone != null, function ($query) use ($phone){
                return $query->where('phone', 'LIKE', '%'.$phone.'%');
            })
            ->when($country != null, function ($query) use ($country){
                if($country == "Indonesia") {
                    return $query->where('country', 'ID');
                } else {
                    return $query->where('country', 'MY');
                }
            })
            ->when($registerDate != null, function ($query) use ($startDate, $endDate){
                return $query->whereBetween('users.created_at', [$startDate, $endDate]);
            })
            ->when($referralCode != null, function ($query) use ($referralCode){
                return $query->where('referral_code', 'LIKE', '%'.$referralCode.'%');
            })
            ->when($status != null, function ($query) use ($status){
                return $query->where('status', '=', $status);
            })
            ->when($search != null, function ($query) use ($search){
                return $query->where('name', 'LIKE', '%'.$search.'%')
                ->orWhere('member_id', 'LIKE', '%'.$search.'%')
                ->orWhere('email', 'LIKE', '%'.$search.'%')
                ->orWhere('phone', 'LIKE', '%'.$search.'%')
                ->orWhere('referral_code', 'LIKE', '%'.$search.'%')
                ->orWhere('favorite_locker', 'LIKE', '%'.$search.'%');
            })
            ->count();
        }

        $data = array();
        if(!empty($posts))
        {
            foreach ($posts as $post)
            {
                $nestedData['member_id'] = $post->member_id;
                $nestedData['name'] = $post->name;
                $nestedData['phone'] = $post->phone;
                $nestedData['email'] = $post->email;
                $nestedData['country'] = $post->country;
                $nestedData['balance'] = $post->balance;
                $nestedData['register_date'] = $post->created_at;
                $nestedData['referral_code'] = $post->referral_code;
                $nestedData['favorite_locker'] = $post->favorite_locker;
                $nestedData['last_transaction'] = $post->last_transaction;
                $nestedData['status'] = $post->status;

                $data[] = $nestedData;
            }
        }
          
        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );
        return($json_data);
    }

    public function downloadExcel(Request $request)
    {
        $memberId = $request->input('phone', null);
        $name = $request->input('name', null);
        $email = $request->input('email', null);
        $phone = $request->input('phone', null);
        $referralCode = $request->input('referralCode', null);
        $country = $request->input('country', null);
        $registerDate = $request->input('registerDate', null);
        $status = $request->input('status', null);
        $startDate = '';
        $endDate = '';

        if (!empty($registerDate)) {
            $date = explode(" - ", $registerDate);
            $startDate = $date[0]." 00:00:00";
            $endDate = $date[1]." 23:59:59";
            $start = date("d M Y", strtotime($date[0]));
            $end = date("d M Y", strtotime($date[1]));
        }
        
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        // style
        $bold = [
            'font' => [
                'bold' => true,
                'size' => 20
            ]
        ];

        // title
        $sheet->mergeCells('A1:E1');
        $sheet->setCellValue('A1', 'Order List');
        $sheet->getStyle('A1')->applyFromArray($bold);
        $sheet->mergeCells('A2:E2');
        if (!empty($registerDate)) {
        $sheet->setCellValue('A2', $start.' - '.$end);
        }

        // header
        $count = 4;
        $sheet->setCellValue('A'.$count, 'No');
        $sheet->setCellValue('B'.$count, 'Member ID');
        $sheet->setCellValue('C'.$count, 'Country');
        $sheet->setCellValue('D'.$count, 'Name');
        $sheet->setCellValue('E'.$count, 'Phone');
        $sheet->setCellValue('F'.$count, 'Email');
        $sheet->setCellValue('G'.$count, 'Balance');
        $sheet->setCellValue('H'.$count, 'Register Date');
        $sheet->setCellValue('I'.$count, 'Last Transaction Date');
        $sheet->setCellValue('J'.$count, 'Status');

        $data = User::select('users.*','t.created_at as last_transaction')
        ->leftJoin(DB::raw('(select user_id, created_at from popsend.transactions order by created_at desc) as t'),'users.id','=','t.user_id')
        ->when($memberId != null, function ($query) use ($memberId){
            return $query->where('member_id', 'LIKE', '%'.$memberId.'%');
        })
        ->when($name != null, function ($query) use ($name){
            return $query->where('name', 'LIKE', '%'.$name.'%');
        })
        ->when($email != null, function ($query) use ($email){
            return $query->where('email', 'LIKE', '%'.$email.'%');
        })
        ->when($phone != null, function ($query) use ($phone){
            return $query->where('phone', 'LIKE', '%'.$phone.'%');
        })
        ->when($country != null, function ($query) use ($country){
            if($country == "Indonesia") {
                return $query->where('country', 'ID');
            } else {
                return $query->where('country', 'MY');
            }
        })
        ->when($registerDate != null, function ($query) use ($startDate, $endDate){
            return $query->whereBetween('users.created_at', [$startDate, $endDate]);
        })
        ->when($referralCode != null, function ($query) use ($referralCode){
            return $query->where('referral_code', 'LIKE', '%'.$referralCode.'%');
        })
        ->when($status != null, function ($query) use ($status){
            return $query->where('status', '=', $status);
        })
        ->groupBy('users.id')
        ->orderBy('created_at','desc')
        ->get();

        if($data->isEmpty()) {
            // data kosong
            $count = $count+1;
            $sheet->mergeCells('A'.$count.':J'.$count);
            $sheet->setCellValue('A'.$count, 'No records found');
        } else { 
            // data ready
            $no = 1;
            foreach($data as $key => $row) {
                $count = $count+1;
                $sheet->setCellValue('A'.$count, $no++);
                $sheet->setCellValue('B'.$count, $row['member_id']);
                $sheet->setCellValue('C'.$count, $row['country']);
                $sheet->setCellValue('D'.$count, $row['name']);
                $sheet->setCellValue('E'.$count, $row['phone']);
                $sheet->setCellValue('F'.$count, $row['email']);
                $sheet->setCellValue('G'.$count, $row['balance']);
                $sheet->setCellValue('H'.$count, $row['created_at']);
                $sheet->setCellValue('I'.$count, $row['last_transaction']);
                $sheet->setCellValue('J'.$count, $row['status']);
            }
        }

        $dateNow = date("ymd");
        $filename = $dateNow.' User List.xlsx';
        
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xlsx");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'.$filename.'"');
        $writer->save("php://output");
    }

    public function getProfile(Request $request, $member_id)
    {
        $startDate = $request->input('start',null);
        $endDate = $request->input('end',null);
        
        if(is_null($startDate)) {
            $startDate = date("Y-m-01")." 00:00:00";
        }

        if(is_null($endDate)) {
            $endDate = date("Y-m-t"." 23:59:59");
        }

        $data = User::where('member_id','=',$member_id)->first();
        $balances = BalanceRecord::getBetweenDate($member_id, $startDate, $endDate);
        
        return view('popapps.users.profile', compact('data','balances'));
    }

    public function getBalanceBetweenDate(Request $request, $member_id, $start, $end)
    {
        $startDate = $start." 00:00:00";
        $endDate = $end." 23:59:59";
        $balances = BalanceRecord::getBetweenDate($member_id, $startDate, $endDate);
        
        return response()->json($balances);
    }

    public function postCredit(Request $request)
    {
        $validatedData = $request->validate([
            'member_id' => 'required',
            'type' => 'required|in:deduct,topup,refund',
            'amount' => 'required|numeric',
            'remark' => 'required'
        ]);

        $memberId = $request->input('member_id');
        $transactionType = $request->input('type');
        $amount = $request->input('amount',0);
        $remarks = $request->input('remark');
        
        $usersDb = User::where('member_id',$memberId)->first();
        if (!$usersDb){
            $request->session()->flash('error','User not found');
            return back();
        }
        $userId = $usersDb->id;

        DB::connection('popsend')->beginTransaction();
        // create transaction
        $transactionModel = new Transaction();
        $createTransaction = $transactionModel->createTransaction($transactionType,$userId,$remarks,$amount);
        if (!$createTransaction->isSuccess){
            DB::connection('popsend')->rollback();
            $request->session()->flash('error',$createTransaction->errorMsg);
            return back();
        }
        $transactionId = $createTransaction->transactionId;
        // credit balance
        $balanceModel =  new BalanceRecord();
        if($transactionType == "refund" || $transactionType == "topup") {
            $trx = $balanceModel->creditDeposit($userId,$amount,$transactionId);      
        } elseif($transactionType == "deduct") {
            $trx = $balanceModel->debitDeposit($userId,$amount,$transactionId);
        }
        if (!$trx->isSuccess){
            DB::connection('popsend')->rollback();
            $request->session()->flash('error',$trx->errorMsg);
            return back();
        }
        DB::connection('popsend')->commit();
        if($transactionType == "refund" || $transactionType == "topup") {
            $request->session()->flash('success',"Success Credit $amount for $usersDb->name ($usersDb->phone)");
        } elseif($transactionType == "deduct") {
            $request->session()->flash('success',"Success Debit $amount for $usersDb->name ($usersDb->phone)");
        }
        return back();
    }
}
