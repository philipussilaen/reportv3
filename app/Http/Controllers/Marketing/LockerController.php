<?php

namespace App\Http\Controllers\Marketing;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Popbox\BuildingType;
use App\Models\Popbox\Country;
use App\Models\Popbox\LockerLocation;
use App\Models\NewLocker\Express;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class LockerController extends Controller
{
    public function view(Request $request)
    {
        $countries = Country::all();
        $types = BuildingType::orderBy('building_type')->get();
        return view('marketing.locker.summary', compact('countries','types'));
    }

    private function searchByLockerId($id, $array) {
        foreach ($array as $key => $val) {
            if ($val['box_id'] === $id) {
                return $val['count'];
            }
        }
        return null;
    }

    public function source(Request $request)
    {
        $columns = array(
            0 => 'locker_id',
            1 => 'locker_name',
            2 => 'building_type',
            3 => 'count'
        );

        $start = $request->start;
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
        $limit = isset($request->length) ? $request->length : 10;
        $currentPage = isset($start) ? ($start/$limit)+1 : 1;

        $rangeDate = $request->date;
        $locker_id = $request->locker_id;
        $locker_name = $request->locker_name;
        $building_type = $request->building_type;
        $country = $request->country;
        $startDate = null;
        $endDate = null;
        if (!empty($rangeDate)) {
            $date = explode(" - ", $rangeDate);
            $startDate = $date[0].' 00:00:00';
            $endDate = $date[1].' 23:59:59';
        }
        $unixStartDate = strtotime($startDate) * 1000;
        $unixEndDate = strtotime($endDate) * 1000;
        $unixNowDateTime = time()*1000;
        
        // $unixStartDate = '1559322000000'; // Saturday, June 1, 2019 00:00:00
        // $unixEndDate = '1561913999000'; // Sunday, June 30, 2019 23:59:59
        $district_id = [1,2,3,4,5,6,7,8,10,11,12,13,14,15,16,23,24,25,26];
        $district_my = [17,18,19,20,21,22,27,28,29,31,33,35,37,39,41];

        $lockers = LockerLocation::with(['buildingType'])
        ->when($country, function($query) use($country, $district_id, $district_my) {
            if($country == "Indonesia") {
                return $query->whereIn('district_id', $district_id);
            } else if($country == "Malaysia") {
                return $query->whereIn('district_id', $district_my);
            }
        })
        ->when($building_type,function($query) use($building_type){
            return $query->whereHas('buildingType', function ($q) use($building_type) {
                $q->where('id_building', $building_type);
            });
        })
        ->get();

        $box_id = [];
        foreach ($lockers as $key => $row) {
            $box_id[] = $row->locker_id;
        }

        $allUsage = Express::select(DB::raw('COUNT(id) as count, box_id'))
        ->when($rangeDate, function ($query) use ($unixStartDate,$unixEndDate){
            return $query->whereBetween('storeTime',[$unixStartDate,$unixEndDate]);
        })
        ->whereIn('box_id', $box_id)
        ->groupBy('box_id')
        // ->get()->toArray();
        ->orderBy($order, $dir)
        ->paginate($limit,['*'],'draw',$currentPage);
        
        $data = [];
        foreach ($allUsage as $key => $row) {
            $locker_id = $row->box_id;
            $locker = LockerLocation::with(['buildingType'])->where('locker_id',$locker_id)->first();
            $name = $locker->name;
            $building_type = $locker->buildingType->building_type;
            $check = $this->searchByLockerId($locker_id, $allUsage);
            
            if($check == null) {
                $usage = 0;
            } else {
                $usage = $check;
            }

            $data[$key] = [
                "locker_id" => $locker_id,
                "locker_name" => $name,
                "building_type" => $building_type,
                "usage" => $usage
            ];
        }
        $totalFiltered = $allUsage->total();

        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalFiltered),//intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );
        return($json_data);
    }

    public function downloadExcel(Request $request)
    {
        $rangeDate = $request->date;
        $locker_id = $request->locker_id;
        $locker_name = $request->locker_name;
        $building_type = $request->building_type;
        $country = $request->country;
        $startDate = null;
        $endDate = null;
        if (!empty($rangeDate)) {
            $date = explode(" - ", $rangeDate);
            $startDate = $date[0].' 00:00:00';
            $endDate = $date[1].' 23:59:59';
        }
        $unixStartDate = strtotime($startDate) * 1000;
        $unixEndDate = strtotime($endDate) * 1000;
        $unixNowDateTime = time()*1000;
        $district_id = [1,2,3,4,5,6,7,8,10,11,12,13,14,15,16,23,24,25,26];
        $district_my = [17,18,19,20,21,22,27,28,29,31,33,35,37,39,41];
        
        $lockers = LockerLocation::with(['buildingType'])
        ->where('isPublished','1')
        ->when($country, function($query) use($country, $district_id, $district_my) {
            if($country == "Indonesia") {
                return $query->whereIn('district_id', $district_id);
            } else if($country == "Malaysia") {
                return $query->whereIn('district_id', $district_my);
            }
        })
        ->when($building_type,function($query) use($building_type){
            return $query->whereHas('buildingType', function ($q) use($building_type) {
                $q->where('id_building', $building_type);
            });
        })
        ->get();

        $box_id = [];
        foreach ($lockers as $key => $row) {
            $box_id[] = $row->locker_id;
        }
        
        $data = Express::select(DB::raw('COUNT(id) as count, box_id'))
        ->when($rangeDate, function ($query) use ($unixStartDate,$unixEndDate){
            return $query->whereBetween('storeTime',[$unixStartDate,$unixEndDate]);
        })
        ->whereIn('box_id', $box_id)
        ->groupBy('box_id')
        ->orderBy('count', 'desc')
        ->get();
        // dd($box_id, $data);
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        
        // style
        $bold = [
            'font' => [
                'bold' => true,
                'size' => 20
            ]
        ];

        // title
        $sheet->mergeCells('A1:E1');
        $sheet->setCellValue('A1', 'Locker Summary');
        $sheet->getStyle('A1')->applyFromArray($bold);
        $sheet->mergeCells('A2:E2');
        if (!empty($storeTime)) {
        $sheet->setCellValue('A2', $start.' - '.$end);
        }

        // header
        $count = 4;
        $sheet->setCellValue('A'.$count, 'No');
        $sheet->setCellValue('B'.$count, 'Locker ID');
        $sheet->setCellValue('C'.$count, 'Locker Name');
        $sheet->setCellValue('D'.$count, 'Building Type');
        $sheet->setCellValue('E'.$count, 'Usage');
        
        if($data->isEmpty()) {
            // data kosong
            $count = $count+1;
            $sheet->mergeCells('A'.$count.':J'.$count);
            $sheet->setCellValue('A'.$count, 'No records found');
        } else { 
            // data ready
            $no = 1;
            foreach ($box_id as $key => $row) {
                $count = $count+1;
                $locker_id = $row;
                $locker = LockerLocation::with(['buildingType'])->where('locker_id',$locker_id)->first();
                $name = $locker->name;
                $building_type = $locker->buildingType->building_type;
                $check = $this->searchByLockerId($locker_id, $data);
                
                if($check == null) {
                    $usage = 0;
                } else {
                    $usage = $check;
                }

                $sheet->setCellValue('A'.$count, $no++);
                $sheet->setCellValue('B'.$count, $locker_id);
                $sheet->setCellValue('C'.$count, $name);
                $sheet->setCellValue('D'.$count, $building_type);
                $sheet->setCellValue('E'.$count, $usage);
            }
        }

        $dateNow = date("ymd");
        $filename = $dateNow.' Locker Summary.xlsx';
        
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xlsx");
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="'.$filename.'"');
        $writer->save("php://output");
        die();
    }
}
