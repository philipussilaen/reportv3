<?php

namespace App\Http\Controllers\Merchant;

use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Helpers\WebCurl;
use App\Models\Popbox\Company;

class CompanyController extends Controller
{
    // var $header;
    // var $curl;
	
	// function __construct() {
    //     $this->header[] = 'POPBOX-HEADER-AUTH:'.env('POPBOX_HEADER_AUTH');
    //     $this->header[] = 'SERVICE-KEY:'.env('SERVICE_KEY');
    //     $this->curl = new WebCurl($this->header);
    // }
    
    public function view(Request $request)
    {
        $data['list'] = Company::with(['token'])->get();

        return view('merchant.company.list', $data);
    }
    
    public function create(Request $request)
    {
        $data['user_id'] = Auth::user()->id;

        $header[] = 'POPBOX-HEADER-AUTH:'.env('POPBOX_HEADER_AUTH');
        $header[] = 'SERVICE-KEY:'.env('SERVICE_KEY');
        $curl = new WebCurl($header);

        $url1 = env('PROX_URL').'company/list';
        $list = json_decode($curl->get($url1, $header));
        $data['companies'] = $list->data;

        $url2 = env('PROX_URL').'company/types';
        $getType = json_decode($curl->get($url2, $header));
        // $getType = $this->curl->get($url, $this->header);
        $data['type'] = $getType->data;

        // $data['type'] = [
        //     '1' => 'Merchant Company',
        //     '2' => 'Logistic Company',
        //     '3' => 'Aggregator Company'
        // ];
        return view('merchant.company.create', $data);
    }

    public function insert(Request $request)
    {
        // $validatedData = $request->validate([
        //     'company_name' => 'required|unique:popbox_db.companies,company_name|max:2',
        //     'person_in_charge' => 'required',
        // ]);

        // dd($validatedData);

        $params = [
            'token' => env('INTERNAL_API_TOKEN'),
            'name' => $request->name,
            'company_name' => $request->company_name,
            'prefix' => $request->prefix,
            'website_url' => $request->website_url,
            'person_in_charge' => $request->person_in_charge,
            'phone_number' => $request->phone_number,
            'support_email' => $request->support_email,
            'company_type' => $request->company_type,
            'created_user' => $request->user_id,
            'created_date' => date('Y-m-d H:i:s'),
            'parent_company' => $request->parent_id_company,
            'level' => $request->level
        ];
        
        $url = env('INTERNAL_API_URL').'locker/company/create';
        $header = 'Content-type: application/json';
        $curl = new WebCurl($header);
        $insert = json_decode($curl->post($url, json_encode($params)));

        if ($insert->isSuccess) {
            return redirect('merchant/company')->with(['success'=> 'New merchant company has been added']);
        } else {
            dd($insert->message);
        }
    }

    public function edit(Request $request)
    {
        $data['id'] = $request->id;
        $data['user_id'] = Auth::user()->id;
        $data['data'] = Company::with(['token'])
        ->where('id', $data['id'])
        ->first();
        
        $header[] = 'POPBOX-HEADER-AUTH:'.env('POPBOX_HEADER_AUTH');
        $header[] = 'SERVICE-KEY:'.env('SERVICE_KEY');
        $curl = new WebCurl($header);

        $url1 = env('PROX_URL').'company/list';
        $list = json_decode($curl->get($url1, $header));
        $data['companies'] = $list->data;

        $type = env('PROX_URL').'company/types';
        $getType = json_decode($curl->get($type, $header));
        $data['type'] = $getType->data;
        
        return view('merchant.company.edit', $data);
    }

    public function update(Request $request)
    {

        $params = [
            'token' => env('INTERNAL_API_TOKEN'),
            'id' => $request->id,
            'id_company' => $request->logistic_id,
            'name' => $request->name,
            'company_name' => $request->company_name,
            'prefix' => $request->prefix,
            'website_url' => $request->website_url,
            'person_in_charge' => $request->person_in_charge,
            'phone_number' => $request->phone_number,
            'support_email' => $request->support_email,
            'company_type' => $request->company_type,
            'updated_user' => $request->user_id,
            'updated_date' => date('Y-m-d H:i:s'),
            'parent_company' => $request->parent_id_company
        ];
        
        $url = env('INTERNAL_API_URL').'locker/company/update';
        $header = 'Content-type: application/json';
        $curl = new WebCurl($header);
        $insert = json_decode($curl->post($url, json_encode($params)));
        
        if ($insert->isSuccess) {
            return redirect('merchant/company')->with(['success'=> 'Merchant company has been updated']);
        } else {
            dd($insert->message);
        }
    }
}
