<?php
/**
 * Created by PhpStorm.
 * User: arief
 * Date: 11/01/2017
 * Time: 22:05
 */

namespace App\Http\Helpers;

use DateTime;

class Helper
{
    /**
     * generate random string
     * @param int $length
     * @return string
     */
    public static function generateRandomString($length = 10)
    {
        $characters = '23456789ABCDEFGHJKLMNPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * get start date and end date on week
     * @param $week
     * @param $year
     * @return mixed
     */
    public static function getStartAndEndDate($week, $year)
    {
        $dto = new \DateTime();
        $ret['week_start'] = $dto->setISODate($year, $week)->format('Y-m-d');
        $ret['week_end'] = $dto->modify('+6 days')->format('Y-m-d');
        return $ret;
    }

    /**
     * get Date From Day of Year
     * @param $dayOfYear
     * @param $year
     * @return bool|\DateTime
     */
    public static function getDateFromDay($dayOfYear, $year)
    {
        $date = \DateTime::createFromFormat('Y z', strval($year) . ' ' . strval($dayOfYear));
        return $date;
    }

    /**
     * Create Date From Month Number
     * @param $monthOfYear
     * @return bool|\DateTime
     */
    public static function getDateFromMonth($monthOfYear)
    {
        $date = \DateTime::createFromFormat('!m', $monthOfYear);
        return $date;
    }

    /**
     * Create Month Name from Month Number
     * @param $monthOfYear
     * @return bool|\DateTime
     */
    public static function getNameFromMonth($monthOfYear)
    {
        $date = \DateTime::createFromFormat('!m', $monthOfYear);
        $monthName = $date->format('F');
        return $monthName;
    }

    /**
     * trims text to a space then adds ellipses if desired
     * @param string $input text to trim
     * @param int $length in characters to trim to
     * @param bool $ellipses if ellipses (...) are to be added
     * @param bool $stripHtml if html tags are to be stripped
     * @return string
     */
    public static function trimText($input, $length, $ellipses = true, $stripHtml = true)
    {
        //strip tags, if desired
        if ($stripHtml) {
            $input = strip_tags($input);
        }

        //no need to trim, already shorter than trim length
        if (strlen($input) <= $length) {
            return $input;
        }

        //find last space within length
        $lastSpace = strrpos(substr($input, 0, $length), ' ');
        $trimmedText = substr($input, 0, $lastSpace);

        //add ellipses (...)
        if ($ellipses) {
            $trimmedText .= '...';
        }

        return $trimmedText;
    }

    /**
     * create image from base64
     * @param $folder
     * @param $filename
     * @return bool|string
     */
    public static function createImg($folder, $filename)
    {
        $server = $_SERVER['DOCUMENT_ROOT'];
        $tmpPath = $server . env('AGENT_LOC') . $folder . '/' . $filename;
        $path = realpath($tmpPath);
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = @file_get_contents($path);
        if (empty($data)) return false;
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
        return $base64;
    }

    public static function createImgUrlAgent($folder,$filename){
        $publicUrl = env('AGENT_PUBLIC_URL');
        $imageUrl = $publicUrl.$folder."/".$filename;
        return $imageUrl;
    }

    /**
     * Calculate distance in mile
     * @param array $latLong1
     * @param array $latLong2
     * @return float
     */
    public static function calculateDistance($latLong1 = [], $latLong2 = [])
    {
        list($lat1, $lon1) = $latLong1;
        list($lat2, $lon2) = $latLong2;

        $lat1 = (float)$lat1;
        $lon1 = (float)$lon1;
        $lat2 = (float)$lat2;
        $lon2 = (float)$lon2;

        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        return $miles;
    }

    public static function secondsToTime($seconds) {
        $dtF = new \DateTime('@0');
        $dtT = new \DateTime("@$seconds");
        return $dtF->diff($dtT)->format('in %a days, %h hours, %i minutes');
    }

    public static function getColor($number){
        $hash = md5('color'.$number);
        return [
            hexdec(substr($hash,0,2)),
            hexdec(substr($hash,2,2)),
            hexdec(substr($hash,4,2))
        ];
    }

    public static function getHexColor($number){
        return '#' . str_pad(dechex($number), 6, '0', STR_PAD_LEFT);
    }

    public static function formatDateRange($dateRange, $delimiter) {
        $result = new \stdClass();

        $tmpDate = explode($delimiter, $dateRange);
        $startDate = $tmpDate[0] . " 00:00:00";
        $endDate = $tmpDate[1] . " 23:59:59";

        $result->startDate = $startDate;
        $result->endDate = $endDate;

        return $result;
    }

    public static function download(Request $request) {
        $file = request('file');
        if($file != "" && file_exists(storage_path("/app")."/".$file)){
            return response()->download(storage_path("/app")."/".$file)->deleteFileAfterSend(true);
        }else{
            echo "File not found";
        }
    }

    public static function formatDate($date)
    {
        $formatDate = new DateTime($date);

        // return date_format($formatDate, 'd M Y h:i A');
        return date_format($formatDate, 'd M Y H:i:s');
    }

    public static function formatNumber($number)
    {
        return number_format($number,0,",",".");
    }
}