<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MarketingCampaignResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id"=>$this->id,
            "name"=>$this->name,
            "description"=>$this->description,
            "start_date"=>$this->start_time,
            "end_date"=>$this->end_time,
            "status"=>$this->status_name,
            "type"=>ucfirst($this->promo_type),
            "amount"=>'<span class="badge badge-primary">'.$this->type.'</span>/'.$this->amount,
            "category"=>$this->category,
            "voucher"=>'<span class="badge badge-primary">'.$this->voucher_type.'</span>',
            "limit"=>$this->limit_usage,
        ];
    }
}
