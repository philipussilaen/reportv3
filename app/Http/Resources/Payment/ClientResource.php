<?php

namespace App\Http\Resources\Payment;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\PaymentChannel;
use App\Models\PaymentVendor;

class ClientResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        $badge = [
            'CREATED'=>'badge badge-secondary',
            'PAID'=>'badge badge-success',
            'FAILED'=>'badge badge-danger',
            'PENDING'=>'badge badge-primary',
            'EXPIRED'=>'badge badge-info',

        ];

        $paymentChannel = PaymentChannel::find($this->parent->payment_channels_id);
        if($paymentChannel->payment_vendors_id == 3) {
            $country = 'Malaysia';
        } else {
            $country = 'Indonesia';
        }

        return [
            'id'=>$this->id,
            'channel'=>$this->parent->channel->name,
            'payment_id'=>$this->parent->payment_id,
            'sub_payment_id'=>is_null($this->sub_payment_id) ?  $this->parent->payment_id : $this->sub_payment_id,
            'transaction'=>$this->parent->transaction_id,
            'va_number'=>$this->parent->virtual_number,
            'customer_name'=>$this->parent->customer_name,
            'customer_paid_amount'=>number_format($this->paid_amount),
            'customer_received'=>number_format($this->customer_received),
            'status'=>"<span class='".$badge[$this->parent->status]."'>".$this->parent->status."</span>",
            'description'=>$this->description,
            'created_at'=>is_null($this->created_at) ? null : $this->created_at->toDateTimeString(),
            'country'=>$country,
        ];
    }
}
