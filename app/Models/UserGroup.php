<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserGroup extends Model
{
    // Connect to table
    protected $table = 'user_groups';

    // menyimpan data dengan timestamps(created_at, updated_at, delete_at)
    public $timestamps = true;
}
