<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    // Connect to table
    protected $table = 'groups';

    // menyimpan data dengan timestamps(created_at, updated_at, delete_at)
    public $timestamps = true;

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_groups');
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'group_roles');
    }
}
