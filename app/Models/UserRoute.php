<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserRoute extends Model
{
    protected $table = 'user_route';
}
