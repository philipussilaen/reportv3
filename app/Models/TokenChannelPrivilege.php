<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TokenChannelPrivilege extends Model
{
    protected $connection = 'payment';
    protected $table = 'token_channel_privileges';
}
