<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClientTransactionDetail extends Model
{
    protected $connection = 'payment';
    protected $table = 'client_transaction_details';

    public function parent()
    {
        return $this->belongsTo('App\Models\ClientTransaction', 'client_transactions_id');
    }

    public function getCustomerReceivedAttribute() {
        if(is_null($this->channel_admin_fee)) {
            return $this->amount;
        }
        return $this->amount - $this->customer_admin_fee;
    }
}
