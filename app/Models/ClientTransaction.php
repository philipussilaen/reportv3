<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
class ClientTransaction extends Model
{
    protected $connection = 'payment';
    protected $table = 'client_transactions';

    public function channel()
    {
        return $this->belongsTo('App\Models\PaymentChannel', 'payment_channels_id');
    }

    public function detail()
    {
        return $this->hasMany('App\Models\ClientTransactionDetail', 'client_transactions_id');
    }

    public function history()
    {
        return $this->hasMany('App\Models\ClientTransactionHistory', 'client_transactions_id');
    }

    public function getVirtualNumberAttribute() {
        $channel =  $this->channel->code;
        $availableVa = [
            'BNI-VA',
            'DOKU-ALFAMART',
            'DOKU-BCA',
            'DOKU-CREDIT',
            'DOKU-INDOMART',
            'DOKU-MANDIRI',
            'DOKU-PERMATA',
            'DOKU-WALLET'
        ];
        if(in_array($channel, $availableVa)) {
            $virtualNumber = '';
            if($channel === 'BNI-VA') {
                $va = DB::connection('payment')->table('bni_virtual_accounts')->select('virtual_account_number')->where('client_transactions_id',$this->id)->first();
                if($va){
                    $virtualNumber = $va->virtual_account_number;
                }
            }else{
                $va = DB::connection('payment')->table('doku_transactions')->select('virtual_number')->where('client_transactions_id',$this->id)->first();
                if($va) {
                    $virtualNumber = $va->virtual_number;
                }
            }
            return $virtualNumber;
        }
        return null;
    }

    public function getExpiredAttribute() {
        $channel =  $this->channel->code;
        $availableVa = [
            'BNI-VA',
            'DOKU-ALFAMART',
            'DOKU-BCA',
            'DOKU-CREDIT',
            'DOKU-INDOMART',
            'DOKU-MANDIRI',
            'DOKU-PERMATA',
            'DOKU-WALLET'
        ];
        if(in_array($channel, $availableVa)) {
            $virtualNumber = '';
            if($channel === 'BNI-VA') {
                $va = DB::connection('payment')->table('bni_virtual_accounts')->select('datetime_expired')->where('client_transactions_id',$this->id)->first();
                if($va){
                    $virtualNumber = $va->datetime_expired;
                }
            }else{
                $va = DB::connection('payment')->table('doku_transactions')->select('datetime_expired')->where('client_transactions_id',$this->id)->first();
                if($va) {
                    $virtualNumber = $va->datetime_expired;
                }
            }
            return $virtualNumber;
        }
        return null;
    }
    public function getClientNameAttribute() {
        $company = DB::connection('payment')->table('company_access_tokens')->select('name')->where('id',$this->company_access_tokens_id)->first();
        if($company) {
            return $company->name;
        }
        return null;
    }
}
