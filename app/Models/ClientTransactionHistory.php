<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClientTransactionHistory extends Model
{
    protected $connection = 'payment';
    protected $table = 'client_transaction_histories';

    public function parent()
    {
        return $this->belongsTo('App\Models\ClientTransaction', 'client_transactions_id');
    }
}
