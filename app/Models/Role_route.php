<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Role_route extends Model
{
    // Connect to table
    protected $table = 'role_routes';

    // menyimpan data dengan timestamps(created_at, updated_at, delete_at)
    public $timestamps = true;

    public static function get_by_role_id($role_id)
    {
        $data = DB::table('role_routes')
            ->select('route_id')
            ->where('role_id', '=', $role_id)
            ->get();

        return $data;
    }
}
