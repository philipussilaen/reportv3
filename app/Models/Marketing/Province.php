<?php

namespace App\Models\Marketing;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $connection = 'popsend';
    protected $table = 'provinces';
}
