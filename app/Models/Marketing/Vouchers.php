<?php

namespace App\Models\Marketing;

use Illuminate\Database\Eloquent\Model;

class Vouchers extends Model
{
    protected $connection = 'popsend';
    protected $table = 'vouchers';
    protected $guarded = ['id'];
}
