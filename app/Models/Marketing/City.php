<?php

namespace App\Models\Marketing;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $connection = 'popsend';
    protected $table = 'cities';
}
