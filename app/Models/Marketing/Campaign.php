<?php

namespace App\Models\Marketing;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    protected $connection = 'popsend';
    protected $table = 'campaigns';


    public function parameters(){
        return $this->hasMany('App\Models\Marketing\CampaignParameter','campaign_id');
    }

    public function getStatusNameAttribute() {
        return $this->status === '1' ? 'Enabled' : 'Disabled';
    }
}
