<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redis;
use Illuminate\Database\Eloquent\Model;

class AccessMenu extends Model
{
    //
    public static function hasAccessMenu() {
        $hasAccess = new \stdClass();
        $hasAccess->isSuccess = false;
        $hasAccess->data = null;

        $user_id = Auth::user()->id;
        $id_redis = date('Ymd').$user_id;
        $menu = Redis::hget('client:'.$id_redis, "menu");
        $data = json_decode($menu);
        
        $hasAccess->isSuccess = true;
        $hasAccess->data = $data;

        return $hasAccess;
    }

    /* public static function get_menu_tree($parent_id) {
        $item = [];
        $menus = DB::table('routes')
            ->select('name', 'route_name', 'icon', 'type', 'parent_id')
            ->where('type', '=', 'menu')
            ->get();
        
        foreach($menus as $row) {
            $item[] = [
                'route' => $row->route_name,
                'name' => $row->name,
                'icon' => $row->icon,
                'child' => ($row->parent_id != 0) ? false : $this->get_menu_tree($row->parent_id)
                // 'child' => empty($child) ? false : $child
            ];
        }

        return $item;
    } */
}
