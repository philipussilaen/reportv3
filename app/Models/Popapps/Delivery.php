<?php

namespace App\Models\Popapps;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Delivery extends Model
{
    protected $connection = 'popsend';
    protected $table = 'deliveries';
    
    public static function countDestination($invoice_code) {
        $data = self::join('delivery_destinations', 'deliveries.id', '=', 'delivery_destinations.delivery_id')
        ->where('deliveries.invoice_code', $invoice_code)
        ->count();

        return $data;
    }

    public static function getByInvoiceCode($invoice_code)
    {
        $data = self::select('*','deliveries.id as delivery_id','deliveries.status as delivery_status')
        ->leftjoin('users as u','deliveries.user_id','=','u.id')
        ->where('invoice_code', '=', $invoice_code)
        ->get();

        return $data;
    }

    /**
     * Cancel Order
     * @param $transactionId
     * @param string $remarks
     * @return \stdClass
     */
    public function cancelOrder($transactionId,$remarks=""){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->refundAmount = 0;
        $response->totalDestinations = 0;
        $response->refundList = [];

        $transactionDb = Transaction::where('transaction_type','delivery')->where('id',$transactionId)->first();
        if (!$transactionDb){
            $response->errorMsg = 'Invalid Transaction PopSend/Delivery';
            return $response;
        }

        $deliveryId = $transactionDb->transaction_id_reference;

        $deliveryDb = self::find($deliveryId);
        if (!$deliveryDb){
            $response->errorMsg = 'PopSend Delivery Data Not Found';
            return $response;
        }

        $status = $deliveryDb->status;
        if ($status != 'CREATED'){
            $response->errorMsg = 'PopSend Status are not CREATED. Status: '.$status;
            return $response;
        }
        $deliveryDb->status = 'CANCEL';
        $deliveryDb->save();

        // get transaction items
        $transactionItems = $transactionDb->items;
        $refundAmount = 0;

        $refundList = [];

        $response->totalDestinations = count($transactionItems);

        foreach ($transactionItems as $item) {
            $destinationDb = $item->deliveryDestination;
            $subInvoiceCode = $destinationDb->invoice_sub_code;
            $pickupNumber = $destinationDb->pickup_number;
            $paidAmount = $item->paid_amount;

            $tmp = new \stdClass();
            $tmp->isRefund = false;
            $tmp->transactionItemId = $item->id;
            $tmp->destinationId = $destinationDb->id;
            $tmp->refundAmount = $paidAmount;
            $tmp->subInvoice = $subInvoiceCode;
            $tmp->errorMsg = null;

            if (empty($pickupNumber)){
                $response->errorMsg = "Sub Order $subInvoiceCode not have Pickup Number";
                return $response;
            }

            // cancel order
            $apiExpress = new ApiPopExpress();
            $cancelOrder = $apiExpress->cancelPickup($pickupNumber,$remarks);

            if (empty($cancelOrder)){
                $tmp->errorMsg = "Failed Cancel. General Error";
                $refundList[] = $tmp;
                continue;
            }
            if ($cancelOrder->response->code!=200){
                $tmp->errorMsg = $cancelOrder->response->message;
                $refundList[] = $tmp;
                continue;
            }
            $tmp->isRefund = true;
            $refundAmount += $paidAmount;

            // find delivery Destination
            $destinationDataDb = DeliveryDestination::find($destinationDb->id);
            $destinationDataDb->status = 'CANCEL';
            $destinationDataDb->save();

            $refundList[] = $tmp;
        }

        $response->refundAmount = $refundAmount;
        $response->refundList = $refundList;
        $response->isSuccess = true;

        return $response;
    }

    /*Relationship*/
    public function destinations(){
        return $this->hasMany(DeliveryDestination::class);
    }

    public function histories(){
        return $this->hasMany(DeliveryHistory::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function transaction(){
        return $this->hasOne(Transaction::class,'transaction_id_reference','id');
    }

    public function delivery_transaction() {
        return $this->transaction()->where('transaction_type', 'delivery');
    }

    public function campaign_usage(){
        return $this->hasOne(CampaignUsage::class,'transaction_id','id');
    }
}
