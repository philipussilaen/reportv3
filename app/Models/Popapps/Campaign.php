<?php

namespace App\Models\Popapps;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Helpers\Helper;

class Campaign extends Model
{
    protected $connection = 'popsend';
    protected $table = 'campaigns';

    public function campaign_usage(){
        return $this->hasMany(CampaignUsage::class);
    }

    public function voucher(){
        return $this->hasMany(Voucher::class);
    }
}