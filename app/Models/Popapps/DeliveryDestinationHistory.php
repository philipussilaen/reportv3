<?php

namespace App\Models\Popapps;

use Illuminate\Database\Eloquent\Model;

class DeliveryDestinationHistory extends Model
{
    protected $connection = 'popsend';
    protected $table = 'delivery_destination_histories';

    public static function getByDeliveryDestinationId($delivery_destination_id) {
        $data = self::select('delivery_destination_histories.*')
        ->leftjoin('delivery_destinations','delivery_destinations.id','=','delivery_destination_histories.delivery_destination_id')
        ->where('delivery_destination_histories.delivery_destination_id', $delivery_destination_id)
        ->orderBy('delivery_destination_histories.created_at', 'desc')
        ->get();

        return $data;
    }
}
