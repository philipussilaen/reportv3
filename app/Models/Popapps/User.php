<?php

namespace App\Models\Popapps;

use App\Http\Helpers\Helper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;


class User extends Model
{
    protected $connection = 'popsend';
    protected $table = 'users';

    // Relationship
    public function delivery(){
        return $this->hasMany(Delivery::class);
    }

    /**
     * Insert New User
     * @param $name
     * @param $phone
     * @param $email
     * @param $password
     * @param string $country
     * @param float $balance
     * @param null $referralCode
     * @return \stdClass
     */
    public static function insertNewUser($name, $phone, $email,$password, $country = 'ID',$balance=0.0,$referralCode=null){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->memberId = null;

        // generate member ID
        $isExist = true;
        while ($isExist){
            $memberId = Helper::generateRandomString(10);
            $check = self::where('member_id',$memberId)->first();
            if (!$check) $isExist = false;
        }

        // hash password
        $hashedPassword = Hash::make($password);

        // clear double space
        $name = preg_replace('/\s+/',' ',$name);

        // generate referral code
        if (empty($referralCode)){
            $firstName = '';
            $words = explode(' ',$name);
            if (!empty($words)){
                $firstName = $words[0];
            }
            $firstName = strtoupper($firstName);
            $random = Helper::generateRandomString(3);
            $referralCode = $firstName.$random;
        }

        // save to DB
        $userDb = new self();
        $userDb->member_id = $memberId;
        $userDb->name = $name;
        $userDb->phone = $phone;
        $userDb->email = $email;
        $userDb->password = $hashedPassword;
        $userDb->status = 'ACTIVE';
        $userDb->country = $country;
        $userDb->referral_code = $referralCode;
        $userDb->balance = $balance;
        $userDb->save();

        $response->isSuccess = true;
        $response->memberId = $memberId;
        $response->id = $userDb->id;

        return $response;
    }

    public function checkUserExist($phoneNumber)
    {
        $data = self::where('phone', $phoneNumber)->first();
        $isExist = false;
        if ($data) {
            $isExist = true;
            return $isExist;
        }
    }

    public function getUserByPhone($phoneNumber)
    {
        $data = self::where('phone', $phoneNumber)->first();
        return $data;
    }
}
