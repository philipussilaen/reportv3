<?php

namespace App\Models\Popapps;

use Illuminate\Database\Eloquent\Model;

class BalanceRecord extends Model
{
    protected $connection = 'popsend';
    protected $table = 'balance_records';

    public static function getMonthlyByMemberId($member_id)
    {
        $startDate = date("Y-m-01")." 00:00:00";
        $endDate = date("Y-m-t"." 23:59:59");
        $data = self::select('*', 'balance_records.created_at as balance_record_date')
        ->leftjoin('users', 'users.id', '=', 'balance_records.user_id')
        ->whereBetween('balance_records.created_at',[$startDate,$endDate])
        ->where('users.member_id', '=', $member_id)
        ->orderBy('balance_records.id','desc')
        ->get();

        return $data;
    }

    public static function getBetweenDate($member_id, $startDate, $endDate)
    {
        $data = self::select('*', 'balance_records.created_at as balance_record_date')
        ->leftjoin('users', 'users.id', '=', 'balance_records.user_id')
        ->leftjoin('transactions', 'transactions.id', '=', 'balance_records.transaction_id')
        ->whereBetween('balance_records.created_at',[$startDate,$endDate])
        ->where('users.member_id', '=', $member_id)
        ->orderBy('balance_records.id','desc')
        ->get();

        return $data;
    }

    /**
     * Credit Deposit
     * @param $userId
     * @param $amount
     * @param $transactionId
     * @return \stdClass
     */
    public function creditDeposit($userId,$amount,$transactionId){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        $currentBalance = 0;
        // get userDB
        $userDb = User::find($userId);
        if (!$userDb){
            $response->errorMsg = 'User Not Found';
            return $response;
        }
        if (!empty($userDb->balance)){
            $currentBalance = $userDb->balance;
        }
        // credit balance
        $newBalance = $currentBalance + $amount;

        // update to user
        $userDb->balance = $newBalance;
        $userDb->save();

        $credit = $amount;
        $debit = 0;
        $previousBalance = $currentBalance;

        // insert into balance record
        $balanceRecord = $this->insertRecord($userId,$transactionId,$credit,$debit,$previousBalance,$newBalance);

        $response->isSuccess = true;
        return $response;
    }

    /**
     * Deduct / Debit Deposit
     * @param $userId
     * @param $amount
     * @param $transactionId
     * @return \stdClass
     */
    public function debitDeposit($userId,$amount,$transactionId){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        $currentBalance = 0;
        // get userDB
        $userDb = User::find($userId);
        if (!$userDb){
            $response->errorMsg = 'User Not Found';
            return $response;
        }
        if (!empty($userDb->balance)){
            $currentBalance = $userDb->balance;
        }

        if ($currentBalance < $amount){
            $response->errorMsg = 'Insufficient Deposit';
            return $response;
        }
        // debit balance
        $newBalance = $currentBalance - $amount;

        // update to user
        $userDb->balance = $newBalance;
        $userDb->save();

        $credit = 0;
        $debit = $amount;
        $previousBalance = $currentBalance;

        // insert into balance record
        $balanceRecord = $this->insertRecord($userId,$transactionId,$credit,$debit,$previousBalance,$newBalance);

        $response->isSuccess = true;
        return $response;
    }

    /*===============Private Function===============*/

    /**
     * Insert Balance Record
     * @param $userId
     * @param $transactionId
     * @param int $creditAmount
     * @param int $debitAmount
     * @param $previousBalance
     * @param $newBalance
     */
    private function insertRecord($userId,$transactionId,$creditAmount=0,$debitAmount=0,$previousBalance,$newBalance){
        // insert to database
        $balanceRecordDb = new self();
        $balanceRecordDb->user_id = $userId;
        $balanceRecordDb->transaction_id = $transactionId;
        $balanceRecordDb->credit = $creditAmount;
        $balanceRecordDb->debit = $debitAmount;
        $balanceRecordDb->previous_balance = $previousBalance;
        $balanceRecordDb->current_balance = $newBalance;
        $balanceRecordDb->date = date('Y-m-d');
        $balanceRecordDb->time = date('H:i:s');
        $balanceRecordDb->save();
    }

    /*Relationship*/
    public function transaction(){
        return $this->belongsTo(Transaction::class,'transaction_id','id');
    }
}
