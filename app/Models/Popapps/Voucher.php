<?php

namespace App\Models\Popapps;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Helpers\Helper;

class Voucher extends Model
{
    protected $connection = 'popsend';
    protected $table = 'vouchers';

    public function campaign(){
        return $this->belongsTo(Campaign::class);
    }
}