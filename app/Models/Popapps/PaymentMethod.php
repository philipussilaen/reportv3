<?php

namespace App\Models\Popapps;

use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{
    protected $connection = 'popsend';
    protected $table = 'payment_methods';
}
