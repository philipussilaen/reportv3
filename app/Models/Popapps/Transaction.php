<?php

namespace App\Models\Popapps;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Helpers\Helper;

class Transaction extends Model
{
    protected $connection = 'popsend';
    protected $table = 'transactions';

    // Relationship
    public function delivery(){
        return $this->belongsTo(Delivery::class, 'id');
    }

    public function campaign_usage(){
        return $this->hasOne(CampaignUsage::class,'transaction_id','id');
    }

    public static function getByPopsafeId($popsafe_id) {
        $data = self::where('transaction_id_reference', $popsafe_id)
        ->where('transaction_type', 'popsafe')
        ->orderBy('created_at', 'desc')
        ->get();

        return $data;
    }

    public static function getByDeliveryId($delivery_id) {
        $data = self::where('transaction_id_reference', $delivery_id)
        ->where('transaction_type', 'delivery')
        ->orderBy('created_at', 'desc')
        ->get();

        return $data;
    }

    public static function getByInvoiceId($invoice_id) {
        $data = self::where('invoice_id', $invoice_id)->first();

        return $data;
    }

    public static function getDetailByInvoiceId($invoice_id)
    {
        $data = self::select('transactions.invoice_id','transactions.transaction_type','transactions.status','transactions.created_at as transaction_date','transactions.description as transaction_description','transactions.total_price','transactions.paid_amount','transactions.promo_amount','transactions.refund_amount','campaigns.name as campaign_name','campaigns.description as campaign_description','vouchers.code as voucher_code','transaction_histories.id as history_id','transaction_histories.user as history_user','transaction_histories.description as history_description','transaction_histories.paid_amount as history_paid_amount','transaction_histories.promo_amount as history_promo_amount','transaction_histories.refund_amount as history_refund_amount','transaction_histories.status as history_status','transaction_histories.created_at as history_date')
        ->leftjoin('transaction_histories', 'transactions.id', '=', 'transaction_histories.transaction_id')
        ->leftjoin('campaign_usages', 'transactions.id', '=', 'campaign_usages.transaction_id')
        ->leftjoin('campaigns', 'campaign_usages.campaign_id', '=', 'campaigns.id')
        ->leftjoin('vouchers', 'campaigns.id', '=', 'vouchers.campaign_id')
        ->where('transactions.invoice_id', $invoice_id)
        ->get();

        return $data;
    }
    
    public static function checkTransactionIdReference($transaction_id_reference)
    {
        $data = self::where('id', $transaction_id_reference)->first();

        return $data;
    }

    /**
     * Refund Transaction
     * @param $transactionId
     * @param string $remarks
     * @return \stdClass
     */
    public function refundTransaction($transactionId,$remarks=''){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        // get transaction
        $transactionDb = Transaction::with('items')
            ->find($transactionId);
        
        if (!$transactionDb){
            $response->errorMsg = 'Transaction Not Found';
            return $response;
        }

        // get balance record debit
        $balanceRecordDb = BalanceRecord::where('transaction_id',$transactionId)->first();
        if (!$balanceRecordDb){
            $response->errorMsg = 'Balance Record Not Found';
            return $response;
        }
        $debitAmount = $balanceRecordDb->debit;

        $refundAmount = $debitAmount;
        // create refund transaction
        $userId = $transactionDb->user_id;
        $description = "Refund Transaction ".$transactionDb->description." by ".Auth::user()->name;

        // create refund Transaction
        $createRefundTransaction = $this->createRefundTransaction($userId,$description,$transactionDb->id,$refundAmount);
        if (!$createRefundTransaction->isSuccess){
            $response->errorMsg = 'Failed Create Refund Transaction';
            return $response;
        }
        $refundTransactionId = $createRefundTransaction->transactionId;

        // change transaction to refund
        $transactionDb->status = 'REFUND';
        $transactionDb->refund_amount = $refundAmount;
        $transactionDb->save();

        // change transaction item to refund
        $transactionItems = $transactionDb->items;
        foreach ($transactionItems as $item) {
            $itemDb = TransactionItem::find($item->id);
            $itemDb->status = 'REFUND';
            $itemDb->refund_amount = $itemDb->paid_amount;
            $itemDb->save();
        }

        // create transaction history
        $userName = "PopBox: ".Auth::user()->name;
        $transactionHistoryModel = new TransactionHistory();
        $insertHistory = $transactionHistoryModel->insertHistory($transactionId,$userName,$description,$transactionDb->total_price,$transactionDb->paid_amount,$transactionDb->promo_amount,$transactionDb->refund_amount,'REFUND',$remarks);

        // refund user balance
        $balanceModel = new BalanceRecord();
        $creditBalance = $balanceModel->creditDeposit($userId,$refundAmount,$refundTransactionId);
        if (!$creditBalance->isSuccess){
            $response->errorMsg = $creditBalance->errorMsg;
            return $response;
        }

        $response->isSuccess = true;
        return $response;
    }

    /**
     * Refund Delivery
     * @param $transactionId
     * @param $refundAmount
     * @param array $refundList
     * @param $remarks
     * @return \stdClass
     */
    public function refundDelivery($transactionId,$refundAmount, $refundList=[],$remarks){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        // get transaction
        $transactionDb = Transaction::with('items')
            ->find($transactionId);

        if (!$transactionDb){
            $response->errorMsg = 'Transaction Not Found';
            return $response;
        }

        $isAllRefunded = false;
        $transactionItems = $transactionDb->items;
        $refundAmount = 0;

        $tmpItemRefund = 0;
        $descriptionRefund = "";
        foreach ($refundList as $item){
            $transactionItemId = $item->transactionItemId;
            $itemRefundAmount = $item->refundAmount;
            $isRefund = $item->isRefund;
            $subInvoice = $item->subInvoice;

            if ($isRefund){
                $tmpItemRefund++;

                $itemDb = TransactionItem::find($transactionItemId);
                $itemDb->status = 'REFUND';
                $itemDb->refund_amount = $itemRefundAmount;
                $itemDb->save();
                $refundAmount += $itemRefundAmount;
                $descriptionRefund .= "Sub Invoice $subInvoice ";
            }
        }
        if (count($transactionItems) == $tmpItemRefund) {
            $isAllRefunded = true;
            $descriptionRefund = $transactionDb->description;
        }

        if (empty($refundAmount)){
            $response->errorMsg = 'Refund Amount 0 ';
            return $response;
        }

        // create refund transaction
        $userId = $transactionDb->user_id;
        $description = "Refund Transaction ".$descriptionRefund." by ".Auth::user()->name;

        // create refund Transaction
        $createRefundTransaction = $this->createRefundTransaction($userId,$description,$transactionDb->id,$refundAmount);
        if (!$createRefundTransaction->isSuccess){
            $response->errorMsg = 'Failed Create Refund Transaction';
            return $response;
        }
        $refundTransactionId = $createRefundTransaction->transactionId;

        // change transaction to refund
        if ($isAllRefunded) $transactionDb->status = 'REFUND';
        $transactionDb->refund_amount = $refundAmount;
        $transactionDb->save();

        // create transaction history
        $userName = "PopBox: ".Auth::user()->name;
        $transactionHistoryModel = new TransactionHistory();
        $insertHistory = $transactionHistoryModel->insertHistory($transactionId,$userName,$description,$transactionDb->total_price,$transactionDb->paid_amount,$transactionDb->promo_amount,$transactionDb->refund_amount,'REFUND',$remarks);

        // refund user balance
        $balanceModel = new BalanceRecord();
        $creditBalance = $balanceModel->creditDeposit($userId,$refundAmount,$refundTransactionId);
        if (!$creditBalance->isSuccess){
            $response->errorMsg = $creditBalance->errorMsg;
            return $response;
        }

        $response->isSuccess = true;
        return $response;
    }

    public function createTransaction($type, $userId, $remarks="", $amount){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->transactionId = null;
        $response->transactionRef = null;

        // create invoice ID
        $invoiceId = 'TRX-' . $userId . date('ymdhi') . Helper::generateRandomString(3);

        $transactionDb = new self();
        $transactionDb->user_id = $userId;
        $transactionDb->invoice_id = $invoiceId;
        $transactionDb->transaction_type = $type;
        $transactionDb->description = "$remarks Action from : " . Auth::user()->name;
        // create total price
        $transactionDb->total_price = $amount;
        $transactionDb->paid_amount = $amount;
        $transactionDb->status = 'PAID';
        $transactionDb->save();

        // get transaction Id for transaction item db
        $transactionId = $transactionDb->id;

        // insert transaction items
        $itemsDb = new TransactionItem();
        $itemsDb->transaction_id = $transactionId;
        $itemsDb->item_reference = null;
        $itemsDb->description = "$remarks Action from : " . Auth::user()->name;
        $itemsDb->price = $amount;
        $itemsDb->paid_amount = $amount;
        $itemsDb->status = 'PAID';
        $itemsDb->remarks = $remarks;
        $itemsDb->save();

        // insert history
        $history = new TransactionHistory();
        $userName = "System : ".Auth::user()->name;
        $description = "$remarks Action from : ".Auth::user()->name;
        $insertHistory = $history->insertHistory($transactionId,$userName,$description,$amount,$amount,0,0,'PAID',$remarks);

        $response->isSuccess = true;
        $response->transactionId = $transactionId;
        $response->transactionRef = $invoiceId;
        return $response;
    }

    /*=========================Private Function=========================*/

    /**
     * Create Refund Transaction
     * @param $userId
     * @param $description
     * @param $previousTransactionId
     * @param $refundAmount
     * @return \stdClass
     */
    private function createRefundTransaction($userId,$description,$previousTransactionId,$refundAmount){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->transactionId = null;

        // create transaction Reference
        $reference = "REF-".$userId.date('ymdh').Helper::generateRandomString(4);

        // save to Database
        $transactionDb = new self();
        $transactionDb->user_id = $userId;
        $transactionDb->invoice_id = $reference;
        $transactionDb->transaction_id_reference = $previousTransactionId;
        $transactionDb->transaction_type = 'refund';
        $transactionDb->description = $description;
        $transactionDb->total_price = $refundAmount;
        $transactionDb->status = 'PAID';
        $transactionDb->paid_amount = $refundAmount;
        $transactionDb->save();
        
        // get refunded transactionDb
        $refundedTransactionDb = self::with('items')->find($previousTransactionId);

        // create item transaction
        foreach ($refundedTransactionDb->items as $item) {
            // insert transaction item
            $transactionItemDb = new TransactionItem();
            $transactionItemDb->transaction_id = $transactionDb->id;
            $transactionItemDb->item_reference = $item->id;
            $transactionItemDb->description = "Refund Transaction ".$item->description;
            $transactionItemDb->price = 0;
            $transactionItemDb->status = 'PAID';
            $transactionItemDb->remarks = $description;
            $transactionItemDb->save();
        }

        $response->isSuccess = true;
        $response->transactionId = $transactionDb->id;
        return $response;
    }
    
    public function items(){
        return $this->hasMany(TransactionItem::class);
    }
}
