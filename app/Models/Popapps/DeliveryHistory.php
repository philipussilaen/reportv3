<?php

namespace App\Models\Popapps;

use Illuminate\Database\Eloquent\Model;

class DeliveryHistory extends Model
{
    protected $connection = 'popsend';
    protected $table = 'delivery_histories';

    public static function getByDeliveryId($delivery_id) {
        $data = self::leftjoin('deliveries','deliveries.id','=','delivery_histories.delivery_id')
        ->where('delivery_histories.delivery_id', $delivery_id)
        ->orderBy('delivery_histories.created_at', 'desc')
        ->get();

        return $data;
    }
}
