<?php

namespace App\Models\Popapps;

use Illuminate\Database\Eloquent\Model;

class PaymentTransaction extends Model
{
    protected $connection = 'popsend';
    protected $table = 'payment_transactions';

    public static function getByInvoiceId($invoice_id) {
        $data = self::where('reference', $invoice_id)->first();

        return $data;
    }
}
