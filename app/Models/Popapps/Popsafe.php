<?php

namespace App\Models\Popapps;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Helpers\ApiProjectX;

class Popsafe extends Model
{
    protected $connection = 'popsend';
    protected $table = 'popsafes';
    
    public static function searchPopsafeList($start, $limit, $order, $dir, $q) {
        $data = self::select('u.member_id', 'u.name', 'u.phone', 'u.country', 'popsafes.id as popsafes_id', 'popsafes.user_id', 'popsafes.invoice_code', 'popsafes.status', 'popsafes.locker_name', 'popsafes.locker_size', 'popsafes.locker_number', 'popsafes.transaction_date', 't.total_price', 't.promo_amount','t.paid_amount', 'v.code', 'c.name AS campaign_name', 'c.id AS campaign_id')
        ->leftjoin('users as u','popsafes.user_id','=','u.id')
        ->leftJoin('transactions as t', 't.transaction_id_reference', '=', 'popsafes.id')
        ->leftJoin('campaign_usages as cu', 'cu.transaction_id', '=', 't.id')
        ->leftJoin('campaigns as c', 'c.id', '=', 'cu.campaign_id')
        ->leftJoin('vouchers as v', 'v.id', '=', 'cu.voucher_id')
        ->where('t.transaction_type', '=', 'popsafe')
        ->where('popsafes.invoice_code','LIKE','%'.$q.'%')
        ->orWhere('u.name', 'LIKE','%'.$q.'%')
        ->orWhere('u.country', 'LIKE','%'.$q.'%')
        ->orWhere('popsafes.locker_name', 'LIKE','%'.$q.'%')
        ->orWhere('c.name', 'LIKE','%'.$q.'%')
        ->orWhere('c.id', 'LIKE','%'.$q.'%')
        ->orWhere('v.code', 'LIKE','%'.$q.'%')
        ->limit($limit)
        ->offset($start)
        ->orderBy($order,$dir)
        ->get();

        return $data;
    }

    public static function countSearchPopsafeList($q) {
        $data = self::select('u.member_id', 'u.name', 'u.phone', 'u.country', 'popsafes.id as popsafes_id', 'popsafes.user_id', 'popsafes.invoice_code', 'popsafes.status', 'popsafes.locker_name', 'popsafes.locker_size', 'popsafes.locker_number', 'popsafes.transaction_date', 't.total_price', 't.promo_amount','t.paid_amount', 'v.code', 'c.name AS campaign_name', 'c.id AS campaign_id')
        ->leftjoin('users as u','popsafes.user_id','=','u.id')
        ->leftJoin('transactions as t', 't.transaction_id_reference', '=', 'popsafes.id')
        ->leftJoin('campaign_usages as cu', 'cu.transaction_id', '=', 't.id')
        ->leftJoin('campaigns as c', 'c.id', '=', 'cu.campaign_id')
        ->leftJoin('vouchers as v', 'v.id', '=', 'cu.voucher_id')
        ->where('t.transaction_type', '=', 'popsafe')
        ->where('popsafes.invoice_code','LIKE','%'.$q.'%')
        ->orWhere('u.name', 'LIKE','%'.$q.'%')
        ->orWhere('u.country', 'LIKE','%'.$q.'%')
        ->orWhere('popsafes.locker_name', 'LIKE','%'.$q.'%')
        ->orWhere('c.name', 'LIKE','%'.$q.'%')
        ->orWhere('c.id', 'LIKE','%'.$q.'%')
        ->orWhere('v.code', 'LIKE','%'.$q.'%')
        ->count();

        return $data;
    }

    public static function getByInvoiceCode($invoice_code)
    {
        $data = self::select('*','popsafes.id as popsafe_id','popsafes.status as order_status')
        ->leftjoin('users as u','popsafes.user_id','=','u.id')
        ->where('invoice_code', '=', $invoice_code)
        ->get();

        return $data;
    }

    public static function getPinByInvoiceCode($invoice_code)
    {
        $data = self::select('code_pin')
        ->where('invoice_code', '=', $invoice_code)
        ->pluck('code_pin');

        return $data;
    }

    public static function getPopsafeListAPS() {
        $data = self::select('u.member_id', 'u.name', 'u.phone', 'u.country', 'popsafes.id as popsafes_id', 'popsafes.user_id', 'popsafes.invoice_code', 'popsafes.status', 'popsafes.locker_name', 'popsafes.locker_size', 'popsafes.locker_number', 'popsafes.transaction_date', 'v.code', 'c.name AS campaign_name', 'c.id AS campaign_id', DB::raw('sum(t.total_price) as total_price'))
        ->leftjoin('users as u','popsafes.user_id','=','u.id')
        ->leftJoin('transactions as t', 't.transaction_id_reference', '=', 'popsafes.id')
        ->leftJoin('campaign_usages as cu', 'cu.transaction_id', '=', 't.id')
        ->leftJoin('campaigns as c', 'c.id', '=', 'cu.campaign_id')
        ->leftJoin('vouchers as v', 'v.id', '=', 'cu.voucher_id')
        ->where('t.transaction_type', '=', 'popsafe')
        ->where('popsafes.locker_name', 'LIKE', 'DPS %')
        ->groupBy('u.member_id', 'u.name', 'u.phone', 'u.country', 'popsafes.id', 'popsafes.user_id', 'popsafes.invoice_code', 'popsafes.status', 'popsafes.locker_name', 'popsafes.locker_size', 'popsafes.locker_number', 'popsafes.transaction_date', 'v.code', 'c.name', 'c.id')
        ->get();

        return $data;
    }

    public static function getPopsafeListLimitAPS($start, $limit, $order, $dir) {
        // $data = self::select('u.member_id', 'u.name', 'u.phone', 'u.country', 'popsafes.id as popsafes_id', 'popsafes.user_id', 'popsafes.invoice_code', 'popsafes.status', 'popsafes.locker_name', 'popsafes.locker_size', 'popsafes.locker_number', 'popsafes.transaction_date', 't.total_price', 't.promo_amount','t.paid_amount', 'v.code', 'c.name AS campaign_name', 'c.id AS campaign_id')
        $data = self::select('u.member_id', 'u.name', 'u.phone', 'u.country', 'popsafes.id as popsafes_id', 'popsafes.user_id', 'popsafes.invoice_code', 'popsafes.status', 'popsafes.locker_name', 'popsafes.locker_size', 'popsafes.locker_number', 'popsafes.transaction_date', 'v.code', 'c.name AS campaign_name', 'c.id AS campaign_id', DB::raw('sum(t.total_price) as total_price'), 'popsafes.status_taken_by')
        ->leftjoin('users as u','popsafes.user_id','=','u.id')
        ->leftJoin('transactions as t', 't.transaction_id_reference', '=', 'popsafes.id')
        ->leftJoin('campaign_usages as cu', 'cu.transaction_id', '=', 't.id')
        ->leftJoin('campaigns as c', 'c.id', '=', 'cu.campaign_id')
        ->leftJoin('vouchers as v', 'v.id', '=', 'cu.voucher_id')
        ->where('t.transaction_type', '=', 'popsafe')
        ->where('popsafes.locker_name', 'LIKE', 'DPS %')
        ->groupBy('u.member_id', 'u.name', 'u.phone', 'u.country', 'popsafes.id', 'popsafes.user_id', 'popsafes.invoice_code', 'popsafes.status', 'popsafes.locker_name', 'popsafes.locker_size', 'popsafes.locker_number', 'popsafes.transaction_date', 'v.code', 'c.name', 'c.id', 'popsafes.status_taken_by')
        ->limit($limit)
        ->offset($start)
        ->orderBy($order, $dir)
        ->get();

        return $data;
    }

    public static function searchPopsafeListAPS($start, $limit, $order, $dir, $q) {
        $data = self::select('u.member_id', 'u.name', 'u.phone', 'u.country', 'popsafes.id as popsafes_id', 'popsafes.user_id', 'popsafes.invoice_code', 'popsafes.status', 'popsafes.locker_name', 'popsafes.locker_size', 'popsafes.locker_number', 'popsafes.transaction_date', 'v.code', 'c.name AS campaign_name', 'c.id AS campaign_id', DB::raw('sum(t.total_price) as total_price'))
        ->leftjoin('users as u','popsafes.user_id','=','u.id')
        ->leftJoin('transactions as t', 't.transaction_id_reference', '=', 'popsafes.id')
        ->leftJoin('campaign_usages as cu', 'cu.transaction_id', '=', 't.id')
        ->leftJoin('campaigns as c', 'c.id', '=', 'cu.campaign_id')
        ->leftJoin('vouchers as v', 'v.id', '=', 'cu.voucher_id')
        ->where('t.transaction_type', '=', 'popsafe')
        ->where('popsafes.locker_name', 'LIKE', 'DPS %')
        ->where(function($query) use ($q){
            $query->where('popsafes.invoice_code','LIKE','%'.$q.'%');
            $query->orwhere('popsafes.status','LIKE','%'.$q.'%');
            $query->orwhere('u.name', 'LIKE','%'.$q.'%');
            $query->orwhere('u.country', 'LIKE','%'.$q.'%');
            $query->orwhere('popsafes.locker_name', 'LIKE','%'.$q.'%');
            $query->orwhere('c.name', 'LIKE','%'.$q.'%');
            $query->orwhere('c.id', 'LIKE','%'.$q.'%');
            $query->orwhere('v.code', 'LIKE','%'.$q.'%');
        })
        ->groupBy('u.member_id', 'u.name', 'u.phone', 'u.country', 'popsafes.id', 'popsafes.user_id', 'popsafes.invoice_code', 'popsafes.status', 'popsafes.locker_name', 'popsafes.locker_size', 'popsafes.locker_number', 'popsafes.transaction_date', 'v.code', 'c.name', 'c.id')
        ->limit($limit)
        ->offset($start)
        ->orderBy($order,$dir)
        ->get();

        return $data;
    }

    public static function countSearchPopsafeListAPS($q) {
        $data = self::select('u.member_id', 'u.name', 'u.phone', 'u.country', 'popsafes.id as popsafes_id', 'popsafes.user_id', 'popsafes.invoice_code', 'popsafes.status', 'popsafes.locker_name', 'popsafes.locker_size', 'popsafes.locker_number', 'popsafes.transaction_date', 'v.code', 'c.name AS campaign_name', 'c.id AS campaign_id', DB::raw('sum(t.total_price) as total_price'))
        ->leftjoin('users as u','popsafes.user_id','=','u.id')
        ->leftJoin('transactions as t', 't.transaction_id_reference', '=', 'popsafes.id')
        ->leftJoin('campaign_usages as cu', 'cu.transaction_id', '=', 't.id')
        ->leftJoin('campaigns as c', 'c.id', '=', 'cu.campaign_id')
        ->leftJoin('vouchers as v', 'v.id', '=', 'cu.voucher_id')
        ->where('t.transaction_type', '=', 'popsafe')
        ->where('popsafes.locker_name', 'LIKE', 'DPS %')
        ->where(function($query) use ($q){
            $query->where('popsafes.invoice_code','LIKE','%'.$q.'%');
            $query->orwhere('popsafes.status','LIKE','%'.$q.'%');
            $query->orwhere('u.name', 'LIKE','%'.$q.'%');
            $query->orwhere('u.country', 'LIKE','%'.$q.'%');
            $query->orwhere('popsafes.locker_name', 'LIKE','%'.$q.'%');
            $query->orwhere('c.name', 'LIKE','%'.$q.'%');
            $query->orwhere('c.id', 'LIKE','%'.$q.'%');
            $query->orwhere('v.code', 'LIKE','%'.$q.'%');
        })
        ->groupBy('u.member_id', 'u.name', 'u.phone', 'u.country', 'popsafes.id', 'popsafes.user_id', 'popsafes.invoice_code', 'popsafes.status', 'popsafes.locker_name', 'popsafes.locker_size', 'popsafes.locker_number', 'popsafes.transaction_date', 'v.code', 'c.name', 'c.id')
        ->count();

        return $data;
    }

    /**
     * Cancel Order
     * @param $transactionId
     * @param null $remarks
     * @return \stdClass
     */
    public function cancelOrder($transactionId,$remarks=null){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        $transactionDb = Transaction::where('transaction_type','popsafe')->where('id',$transactionId)->first();
        if (!$transactionDb){
            $response->errorMsg = 'Invalid Transaction PopSafe';
            return $response;
        }

        $popSafeId = $transactionDb->transaction_id_reference;
        // get popsafe DB
        $popSafeDb = self::find($popSafeId);
        if (!$popSafeDb){
            $response->errorMsg = 'PopSafe Transaction Not Found';
            return $response;
        }

        $status = $popSafeDb->status;
        if ($status != 'CREATED'){
            $response->errorMsg = 'PopSafe Status are not CREATED. Status: '.$status;
            return $response;
        }
        $expressId = $popSafeDb->express_id;
        if (empty($expressId)){
            $response->errorMsg = "Prox Express ID not Found";
            return $response;
        }

        // change status to cancel
        $popSafeDb->status = 'CANCEL';
        $popSafeDb->save();

        $userName = Auth::user()->name;
        $user = "PopBox $userName";

        // insert history
        $popSafeHistoryModel = new PopSafeHistory();
        $insertHistory = $popSafeHistoryModel->insertPopSafeHistory($popSafeDb->id,'CANCEL',$user,$remarks);

        // cancel to Prox
        $apiProx = new ApiProjectX();
        $deleteImported = $apiProx->deleteImport($expressId);
        if (empty($deleteImported)){
            $response->errorMsg= 'Failed to Delete to Prox';
            return $response;
        }

        if ($deleteImported->response->code!=200){
            $response->errorMsg = $deleteImported->response->message;
            return $response;
        }

        $response->isSuccess = true;
        return $response;
    }

    /**
     * Extend PopSafe
     * @param $invoiceCode
     * @param boolean $isAuto
     * @return \stdClass
     */
    public function extendPopSafe($invoiceCode,$isAuto = false){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->popSafeId = null;

        // get popsafe db
        $popSafeDb = self::where('invoice_code',$invoiceCode)->first();
        if (!$popSafeDb){
            $response->errorMsg = 'PopSafe Invoice Not Found';
            return $response;
        }
        $status = $popSafeDb->status;
        $expiredTime = $popSafeDb->expired_time;
        $autoExtend = $popSafeDb->auto_extend;

        // check expired / overdue time
        $nowTime = time();
        if ($nowTime < strtotime($expiredTime)){
            $response->errorMsg = 'PopSafes not EXPIRED';
            return $response;
        }
        // check if auto extend
        if (!$isAuto){
            if (!empty($autoExtend)){
                $response->errorMsg = 'Auto Extend Already Enabled';
                return $response;
            }
        }

        // add expired time
        $newExpired = new \DateTime($expiredTime);
        $newExpired->modify("+24 hours");
        $newExpired = $newExpired->format("Y-m-d H:i:s");

        // update expired on popsafe
        $popSafeDb = self::find($popSafeDb->id);
        $popSafeDb->status = 'IN STORE';
        $popSafeDb->expired_time = $newExpired;
        $popSafeDb->last_extended_datetime = date('Y-m-d H:i:s');
        $numberOfExtend = $popSafeDb->number_of_extend;
        $popSafeDb->number_of_extend = $numberOfExtend+1;
        $popSafeDb->save();

        $response->isSuccess = true;
        $response->popSafeId = $popSafeDb->id;

        // insert history
        $user = 'User';
        $remarks = 'Extend by User';
        if ($autoExtend){
            $user = 'System';
            $remarks = 'Auto Extend';
        }
        $insertHistory = PopsafeHistory::insertPopsafeHistory($popSafeDb->id,'IN STORE',$user,$remarks);

        return $response;
    }

    /**
     * Get List Of Order PopSafe
     * @param $userId
     * @param null $invoiceId
     * @return \stdClass
     */
    public static function getList($userId, $invoiceId=null)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;
        $response->data = null;

        $historyDb = self::where('user_id','=',$userId)
            ->select('id', 'locker_name','locker_address','locker_address_detail','operational_hours','locker_number','code_pin','latitude','longitude','locker_size','transaction_date','cancellation_time',
                'expired_time','invoice_code','status','notes','total_price','item_photo')
            ->when($invoiceId,function ($query) use ($invoiceId){
                $query->where('invoice_code',$invoiceId);
            })
            ->orderby('created_at','DESC')
            ->paginate(10);

        if ($historyDb->isEmpty()){
            $response->isSuccess = true;
            $response->errorMsg = "You don't have any transactions";
            return $response;
        }
        $nowTime = time();
        foreach ($historyDb as $item) {
            $isCancel = 1;
            $isUse = 1;
            $isCollect = 0;
            $isExpired = 0;
            $status = $item->status;
            $imageName = $item->item_photo;
            $paid_price = 0;
            $promo_price = 0;

            // get promo details
            $promoUsed = DB::table('transactions')
                ->select('paid_amount', 'promo_amount', 'id')
                ->where('transaction_id_reference', $item->id)
                ->where('transaction_type', 'popsafe')
                ->first();

            if (!empty($promoUsed)) {
                $paid_price = (int)$promoUsed->paid_amount;
                $promo_price = (int)$promoUsed->promo_amount;
            }

            if ($item->status == 'CREATED'){
                // check cancellation time
                if ($nowTime > strtotime($item->cancellation_time)) $isCancel = 1;
                // check expired time
                if ($nowTime > strtotime($item->expired_time)){
                    $isUse = 0;
                    $isCancel = 0;
                    $isExpired = 0;
                }
            } elseif ($item->status == 'IN STORE'){
                $isCancel = 0;
                $isCollect = 1;
                // check expired time
                if ($nowTime > strtotime($item->expired_time)) {
                    $isUse = 0;
                    $isCollect = 0;
                    $status = 'OVERDUE';
                    $isExpired = 0;
                }
            } elseif ($item->status == 'COMPLETE'){
                $isCancel = 0;
            } elseif ($item->status == 'OVERDUE' || $item->status == 'EXPIRED' || $item->status == 'CANCEL'){
                $isUse = 0;
                $isCollect = 0;
                $isCancel = 0;
                $isExpired = 0;
            }

            if (!empty($imageName)){
                $imageName = url($imageName);
            }

            $item->is_cancel = $isCancel;
            $item->is_expired = $isUse;
            $item->is_collect = $isCollect;
            $item->is_expired = $isExpired;
            $item->status = $status;
            $item->item_photo = $imageName;
            $item->paid_price = $paid_price;
            $item->promo_price = $promo_price;
        }

        $response->isSuccess = true;
        $response->pagination = (int)ceil($historyDb->total()/$historyDb->perPage());
        $response->data = $historyDb->items();

        return $response;
    }

    public function addHistoryChangePhoneNumber($invoiceCode, $newPhone, $oldPhone, $reason)
    {
        $remarks = "Change Phone Number because $reason from $oldPhone to $newPhone";
        $popsafeHistory = $this->saveToHistory($invoiceCode, $remarks);
        return $popsafeHistory;
    }

    public function addRemarks($invoiceCode, $remarks)
    {
        $popsafeHistory = $this->saveToHistory($invoiceCode, $remarks);  
        return $popsafeHistory; 
    }

    public function saveToHistory($invoiceCode, $remarks)
    {
        $response = new \stdClass();
        $response->isSuccess = false;

        $data =  Popsafe::with('histories')
                    ->where('invoice_code', $invoiceCode)
                    ->first();

        if ($data->histories != null) {
            $last_histories = $data->histories->first();
            $user = Auth::user();

            $remarks = "$remarks by $user->name";

            $historyDb = new PopsafeHistory;
            $historyDb->popsafe_id = $data->id;
            $historyDb->user = $user->name;
            $historyDb->status = $last_histories->status;
            $historyDb->remarks = $remarks;

            if ($historyDb->save()) {
                $historyDb->save();
                $response->isSuccess = true;
                return $response;
            }
        }
    }

    public function switchUserParcel($invoiceCode, $userNew)
    {
        $response = new \stdClass();
        $response->isSuccess = false;

        DB::beginTransaction();
            $dataPopsafe = self::where('invoice_code', $invoiceCode)->first();
            $dataTransaction = Transaction::where('transaction_id_reference', $dataPopsafe->id)->where('transaction_type', 'popsafe')->get();

            $dataPopsafe->user_id = $userNew->id;
            $dataPopsafe->save();

            $trx = [];
            foreach ($dataTransaction as $key => $transaction) {
                $transaction->user_id = $userNew->id;
                $transaction->save();
                
                if ($transaction->save()) {
                    $trx[] = true; 
                }
            }
            
            if ($dataPopsafe->save() && $trx[0]) {
                DB::commit();
                $response->isSuccess = true;
                return $response;
            } else {
                DB::rollBack();
                return $response;
            }


        
            
    }

    /*Relationship*/
    public function histories(){
        return $this->hasMany(PopsafeHistory::class,'popsafe_id','id')->orderBy('created_at', 'desc');
    }
}
