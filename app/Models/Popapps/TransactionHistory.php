<?php

namespace App\Models\Popapps;

use Illuminate\Database\Eloquent\Model;

class TransactionHistory extends Model
{
    protected $connection = 'popsend';
    protected $table = 'transaction_histories';

    public static function getByTransactionId($transaction_id) {
        $data = self::where('transaction_id', $transaction_id)
        ->get();

        return $data;
    }

    /**
     * Insert History
     * @param $transactionId
     * @param string $userName
     * @param string $description
     * @param int $totalPrice
     * @param int $paidAmount
     * @param int $promoAmount
     * @param int $refundAmount
     * @param string $status
     * @param string $remarks
     * @return \stdClass
     */
    public function insertHistory($transactionId, $userName='',$description='',$totalPrice=0,$paidAmount=0,$promoAmount=0,$refundAmount=0,$status='CREATED',$remarks=''){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        // insert into Db
        $dataDb = new self();
        $dataDb->transaction_id = $transactionId;
        $dataDb->user = $userName;
        $dataDb->description = $description;
        $dataDb->total_price = $totalPrice;
        $dataDb->status = $status;
        $dataDb->paid_amount = $paidAmount;
        $dataDb->promo_amount = $promoAmount;
        $dataDb->refund_amount = $refundAmount;
        $dataDb->remarks = $remarks;
        $dataDb->save();

        $response->isSuccess = true;
        return $response;
    }
}
