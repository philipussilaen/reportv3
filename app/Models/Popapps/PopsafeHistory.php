<?php

namespace App\Models\Popapps;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PopsafeHistory extends Model
{
    protected $connection = 'popsend';
    protected $table = 'popsafe_histories';

    public static function getByPopsafeId($popsafe_id) {
        $data = self::where('popsafe_id', $popsafe_id)
        ->orderBy('created_at', 'desc')
        ->get();

        return $data;
    }
    
    /**
     * Insert PopSafe History
     * @param $popsafeId
     * @param $status
     * @param null $user
     * @param null $remarks
     * @return \stdClass
     */
    public function insertPopSafeHistory($popsafeId,$status,$user=null,$remarks=null){
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->errorMsg = null;

        $historyDb = new self();
        $historyDb->popsafe_id = $popsafeId;
        $historyDb->status = $status;
        $historyDb->user = $user;
        $historyDb->remarks = $remarks;
        $historyDb->save();

        $response->isSuccess = true;
        return $response;
    }
}
