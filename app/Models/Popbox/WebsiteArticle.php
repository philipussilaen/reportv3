<?php

namespace App\Models\Popbox;

use Illuminate\Database\Eloquent\Model;

class WebsiteArticle extends Model
{
    protected $connection = 'popbox_db';
    protected $table = 'tb_website_articles';
    protected $primaryKey = 'id_article';
}
