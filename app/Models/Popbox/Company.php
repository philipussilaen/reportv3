<?php

namespace App\Models\Popbox;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $connection = 'popbox_db';
    protected $table = 'companies';

    public function token()
    {
        return $this->hasOne(ApiCompanyAccessToken::class);
    }
}
