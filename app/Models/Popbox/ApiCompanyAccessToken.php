<?php

namespace App\Models\Popbox;

use Illuminate\Database\Eloquent\Model;

class ApiCompanyAccessToken extends Model
{
    protected $connection = 'popbox_db';
    protected $table = 'api_company_access_tokens';
}
