<?php

namespace App\Models\Popbox;

use Illuminate\Database\Eloquent\Model;

class WebsiteCategory extends Model
{
    protected $connection = 'popbox_db';
    protected $table = 'tb_website_categories';
}
