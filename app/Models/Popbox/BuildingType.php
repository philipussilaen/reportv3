<?php

namespace App\Models\Popbox;

use Illuminate\Database\Eloquent\Model;

class BuildingType extends Model
{
    protected $connection = 'popbox_db';
    protected $table = 'buildingtypes';
    protected $primaryKey = 'id_building';

    public function lockerLocation()
    {
        return $this->hasMany(LockerLocation::class, 'id_building');
    }
}
