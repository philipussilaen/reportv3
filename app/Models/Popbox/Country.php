<?php

namespace App\Models\Popbox;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $connection = 'popbox_db';
    protected $table = 'countries';
}
