<?php

namespace App\Models\Popbox;

use Illuminate\Database\Eloquent\Model;

class LockerActivitiesReturn extends Model
{
    protected $connection = 'popbox_db';
    protected $table = 'locker_activities_return';

    /**
     * Get the status record associated with this locker activity.
     */
    public function status()
    {
        return $this->hasOne('App\Models\Popbox\ReturnStatus');
    }
}
