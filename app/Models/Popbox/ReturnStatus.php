<?php

namespace App\Models\Popbox;

use Illuminate\Database\Eloquent\Model;

class ReturnStatus extends Model
{
    protected $connection = 'popbox_db';
    protected $table = 'return_statuses';

    /**
     * Get the locker activity that owns the status.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\Popbox\User');
    }
}
