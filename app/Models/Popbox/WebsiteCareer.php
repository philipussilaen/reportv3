<?php

namespace App\Models\Popbox;

use Illuminate\Database\Eloquent\Model;

class WebsiteCareer extends Model
{
    protected $connection = 'popbox_db';
    protected $table = 'tb_website_career';
    protected $primaryKey = 'id_career';
}
