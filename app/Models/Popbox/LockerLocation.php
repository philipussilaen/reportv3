<?php

namespace App\Models\Popbox;

use Illuminate\Database\Eloquent\Model;

class LockerLocation extends Model
{
    protected $connection = 'popbox_db';
    protected $table = 'locker_locations';
    public $timestamps = false;

    public function buildingType()
    {
        return $this->belongsTo(BuildingType::class, 'building_type_id');
    }

    public function availabilityReport()
    {
        return $this->hasOne(LockerAvailabilityReport::class, 'locker_id', 'locker_id');
    }

    public function district()
    {
        return $this->belongsTo(District::class, 'district_id');
    }

    /* public static function getBuildingType($locker_id)
    {
        $data = self::leftjoin('buildingtypes','buildingtypes.id_building','=','locker_locations.building_type_id')
        ->where('locker_locations.locker_id', $locker_id)
        ->first();
        
        return $data['building_type'];
    } */
}
