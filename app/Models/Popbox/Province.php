<?php

namespace App\Models\Popbox;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $connection = 'popbox_db';
    protected $table = 'provinces';

    public function district()
    {
        return $this->hasMany(District::class);
    }
}