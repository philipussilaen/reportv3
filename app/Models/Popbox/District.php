<?php

namespace App\Models\Popbox;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    protected $connection = 'popbox_db';
    protected $table = 'districts';

    public function lockerLocation()
    {
        return $this->hasMany(LockerLocation::class);
    }

    public function province()
    {
        return $this->belongsTo(Province::class, 'province_id');
    }
}