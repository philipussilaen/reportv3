<?php

namespace App\Models\NewLocker;

use Illuminate\Database\Eloquent\Model;

class Express extends Model
{
    const UPDATED_AT = 'last_update';
    // const CREATED_AT = 'last_update';

    // set connection and table
    protected $connection = 'newlocker_db';
    protected $table = 'tb_newlocker_express';
    protected $primaryKey = 'id';
    public $incrementing = false;
    public $timestamps = false;

    public static function getSmsHistory($expressNumber)
    {
        $response = new \stdClass();
        $response->data = [];
        $response->sms_count = 0;

        $data = self::with('smslog')->where('expressNumber', $expressNumber)->first();
        $result = [];
        if (empty($data->smslog)) {
            return $response;
        } else {
            $response->data = end($data->smslog);
            $count = 0;
            foreach ($data->smslog as $key => $value) {
                if ($value->sms_status == 'SUCCESS') {
                    $count++;
                }
            }
            $response->sms_count = $count;
            return $response;
        }
    }

    /*Relationship*/
    public function company(){
        return $this->belongsTo(Company::class,'logisticsCompany_id','id_company');
    }

    public function userStore(){
        return $this->belongsTo(Users::class,'storeUser_id','id_user');
    }

    public function box(){
        return $this->belongsTo(Box::class,'box_id','id');
    }

    public function smslog(){
        return $this->hasMany(SmsHistory::class,'express_id','id');
    }

    public function mouth(){
        return $this->belongsTo(Mouth::class,'mouth_id','id_mouth');
    }

    public function user(){
        return $this->belongsTo(Users::class,'storeUser_id','id_user');
    }

    public function detail(){
        return $this->hasOne(DetailExpress::class,'parcel_id');
    }
}
