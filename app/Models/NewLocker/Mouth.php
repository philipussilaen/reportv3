<?php

namespace App\Models\NewLocker;

use Illuminate\Database\Eloquent\Model;

class Mouth extends Model
{
    // set connection and table
    protected $connection = 'newlocker_db';
    protected $table = 'tb_newlocker_mouth';

    public function express(){
        return $this->hasMany(Express::class,'mouth_id','id_mouth');
    }

    public function mouthtype(){
        return $this->belongsTo(MouthType::class, 'id_mouthtype');
    }
}
