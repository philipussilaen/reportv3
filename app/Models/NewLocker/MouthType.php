<?php

namespace App\Models\NewLocker;

use Illuminate\Database\Eloquent\Model;

class MouthType extends Model
{
    // set connection and table
    protected $connection = 'newlocker_db';
    protected $table = 'tb_newlocker_mouthtype';

    public function mouth(){
        return $this->hasMany(Mouth::class, 'mouthType_id', 'id_mouthtype');
    }
}
