<?php

namespace App\Models\NewLocker;

use Illuminate\Database\Eloquent\Model;

class MachineStat extends Model
{
    // set connection and table
    protected $connection = 'newlocker_db';
    protected $table = 'tb_newlocker_machinestat';
    
    public function box()
    {
        return $this->belongsTo(Box::class, 'locker_id');
    }
}
