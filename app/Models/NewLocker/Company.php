<?php

namespace App\Models\NewLocker;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $connection = 'newlocker_db';
    protected $table = 'tb_newlocker_company';

    public function express(){
        return $this->hasMany(Express::class,'logisticsCompany_id','id_company');
    }
}