<?php

namespace App\Models\NewLocker;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    // kok ke popbox_db?
    protected $connection = 'popbox_db';
    protected $table = 'locker_locations';
    // protected $connection = 'newlocker_db';
    // protected $table = 'tb_newlocker_locations';
    public function box()
    {
        return $this->belongsTo(Box::class, 'locker_id');
    }
}
