<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    // Connect to roles table
    protected $table = 'roles';

    // menyimpan data dengan timestamps(created_at, updated_at, delete_at)
    public $timestamps = true;
}