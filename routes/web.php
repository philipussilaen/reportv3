<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', 'HomeController@index');

Route::group(['prefix' => 'auth'], function () {
    Route::get('login', '\App\Http\Controllers\Auth\LoginController@getLogin');
});

Route::group(['middleware' => 'auth'], function() {
    // =================================================================================
    // ============================== Everyone Can Access ==============================
    // =================================================================================
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('home', function(){
        $id_company = Auth::user()->id_company;
        if($id_company == '6150257051001564467410PO59nkSDaW' || $id_company == '3320779249001564467468EV9AlxrDEV' || $id_company == '6830104720001564467549OaYIp0HNnR') {
            return App::call('App\Http\Controllers\Popapps\PopsafeController@getLandingAPS');
        } else {
            return App::call('App\Http\Controllers\Popbox\DeliveryActivityController@viewDeliveryActivity');
        }
        
    });
    Route::get('profile', 'HomeController@showProfile');
    Route::post('resetPassword', 'HomeController@updatePassword');
    Route::get('activities', 'LockerController@index');

    // Route::get('/', 'Popapps\TransactionController@getLandingAPS');
    // Route::get('/', 'Popapps\PopsafeController@getLandingAPS');
    
    // =================================================================================
    // ===================================== AJAX ======================================
    // =================================================================================
    Route::post('getAjaxServerPopsafeList/{groupName}', 'Popapps\PopsafeController@getAjaxServerPopsafeList');
    Route::get('getAjaxTransactionDetail/{invoice_id}', 'Popapps\TransactionController@getDetailByInvoiceId');
    Route::post('getAjaxServerTransactionList/{groupName}', 'Popapps\TransactionController@getAjaxServerTransactionList');
    Route::post('getAjaxServerDeliveryList', 'Popapps\DeliveryController@getAjaxServerDeliveryList');
    Route::get('getAjaxUser', 'Popapps\PopsafeController@getAjaxUser');
    Route::get('getAjaxAllUser', 'Popapps\PopsafeController@getAjaxAllUser');
    Route::get('getAjaxAllPhone', 'Popapps\PopsafeController@getAjaxAllPhone');
    Route::post('checkAccess', 'CheckAccessController@checkAccess');
    Route::get('getAjaxUserTransaction/{groupName}', 'Popapps\TransactionController@getAjaxUser');
    Route::post('getAjaxParcelAnalytics', 'Popbox\ParcelController@getAjaxAnalytic');
    Route::get('report/payment/amount','Report\PaymentController@calculateAmount');

    // =================================================================================
    // ============================= Only User with Access =============================
    // =================================================================================
    Route::group(['middleware' => 'App\Http\Middleware\UserAccessControl'], function() {

        Route::group(['prefix' => 'route'], function() {
            Route::get('/', 'RouteController@index');
            Route::get('create', 'RouteController@create')->name('route.create');
            Route::post('store', 'RouteController@store')->name('route.store');
            Route::get('edit', 'RouteController@edit')->name('route.edit');
            Route::post('update', 'RouteController@update')->name('route.upgrade');
            Route::get('destroy', 'RouteController@destroy')->name('route.destroy');
        });
        
        Route::group(['prefix' => 'role'], function () {
            Route::get('/', 'RoleController@index');
            Route::get('create', 'RoleController@create')->name('role.create');
            Route::post('store', 'RoleController@store');
            Route::get('show', 'RoleController@show');
            Route::get('edit', 'RoleController@edit')->name('role.edit');
            Route::post('update', 'RoleController@update');
            Route::get('destroy', 'RoleController@destroy');
        });
        
        Route::group(['prefix' => 'group'], function () {
            Route::get('/', 'GroupController@index');
            Route::get('create', 'GroupController@create');
            Route::post('store', 'GroupController@store');
            Route::get('show', 'GroupController@show');
            Route::get('edit', 'GroupController@edit');
            Route::post('update', 'GroupController@update');
            Route::get('destroy', 'GroupController@destroy');
        });
        
        Route::group(['prefix' => 'user'], function () {
            Route::get('/', 'UserController@index');
            Route::get('create', 'UserController@create');
            Route::post('store', 'UserController@store');
            Route::get('show', 'UserController@show');
            Route::get('edit', 'UserController@edit');
            Route::post('update', 'UserController@update');
            Route::get('destroy', 'UserController@destroy');
            Route::get('reset', 'UserController@reset'); // reset password
        });

        Route::group(['prefix' => 'popsafe'], function () {
            Route::get('/', 'Popapps\PopsafeController@index')->name('popsafe');
            Route::get('detail/{invoice_code}/{invoice_id?}', 'Popapps\PopsafeController@getDetail')->name('popsafe.detail');
            Route::get('detailPin/{invoice_code}', 'Popapps\PopsafeController@getDetailPin')->name('popsafe.detailPin');
            Route::post('addremarks/{invoice_code}', 'Popapps\PopsafeController@addRemarks')->name('popsafe.addremarks');
            Route::post('resendsms/{invoice_code}', 'Popapps\PopsafeController@resendSms')->name('popsafe.resendsms');
            Route::post('summary/{groupName}', 'Popapps\PopsafeController@postAjaxSummaryPopsafeList');
            Route::get('getFileExcel/{groupName}','Popapps\PopsafeController@downloadExcel');
            Route::get('analytics', 'Popapps\PopsafeController@showAnalytics');
            Route::post('analytics', 'Popapps\PopsafeController@dataAnalytics');
        });

        Route::group(['prefix' => 'locker'], function () {
            Route::post('changePhoneNumber/{invoice_id}', 'Popbox\LockerController@changePhoneNumber');
            Route::group(['prefix' => 'location'], function () {
                Route::get('/', 'Popbox\LockerController@viewLockerList'); 
                Route::get('getFileExcel','Popbox\LockerController@downloadExcel');
            });
            Route::post('getAjaxLockerList', 'Popbox\LockerController@getAjaxLockerList');
            Route::post('summary', 'Popbox\LockerController@postAjaxSummaryLockerList');
            Route::get('info/{box_id}', 'Popbox\LockerController@boxInfo');
            Route::get('system/{box_id}', 'Popbox\LockerController@systemInfo');
            Route::get('cabinet/{mouth_id}', 'Popbox\LockerController@parcelDetail');
            Route::post('postOpenDoor/{mouth_id}', 'Popbox\LockerController@remoteOpenDoor');
            Route::post('updateDoorStatus', 'Popbox\LockerController@updateDoorStatus');
            Route::post('openDoorByExpress/{parcel_id}', 'Popbox\LockerController@remoteOpenDoorByExpress');
            Route::post('postChangeOverdue', 'Popbox\LockerController@postChangeOverdue');
            Route::post('postRemoteInitDb/{locker_id}', 'Popbox\LockerController@remoteInitDb');
            Route::post('postReboot/{locker_id}', 'Popbox\LockerController@rebootLocker');
            Route::post('postResync/{locker_id}', 'Popbox\LockerController@resyncLocker');
            Route::get('edit/{locker_id}', 'Popbox\LockerController@edit');
            Route::post('edit/{locker_id}', 'Popbox\LockerController@update');
            Route::group(['prefix' => 'delivery'], function () {
                Route::get('/', 'Popbox\DeliveryActivityController@viewDeliveryActivity');
                Route::post('getAjaxDeliveryActivity', 'Popbox\DeliveryActivityController@getAjaxDeliveryActivity');
                Route::get('summary', 'Popbox\DeliveryActivityController@postAjaxSummaryDeliveryActivityList');
                Route::get('detail/{parcel_id}', 'Popbox\DeliveryActivityController@getDetail');
                Route::get('detailPin', 'Popbox\DeliveryActivityController@getDetailPin');
                Route::get('getFileExcel','Popbox\DeliveryActivityController@downloadExcel');
                Route::post('blocking','Popbox\DeliveryActivityController@blockingParcel');
                Route::post('changePhoneNumber/{express_id}', 'Popbox\DeliveryActivityController@changePhoneNumber');
                Route::post('resendsms/{express_id}', 'Popbox\DeliveryActivityController@resendSms')->name('locker.delivery.resendsms');
                Route::post('/resendWA', 'Notification\WhatsappController@resend');
            });
        });

        Route::group(['prefix' => 'parcel'], function () {
            Route::get('analytics', 'Popbox\ParcelController@getAnalytic');
            Route::post('analytics', 'Popbox\ParcelController@getAjaxAnalytic');
            Route::group(['prefix' => 'return'], function () {
                Route::get('/', 'Popbox\ReturnController@index');
                Route::post('getAjaxReturnActivity', 'Popbox\ReturnController@getAjaxReturnActivity');
                Route::get('getFileExcel','Popbox\ReturnController@downloadExcel');
                Route::get('analytics', 'Popbox\ReturnController@showAnalytics');
                Route::post('analytics', 'Popbox\ReturnController@getAnalytics');
            });
        });

        Route::group(['prefix' => 'transaction'], function () {
            Route::get('/', 'Popapps\TransactionController@index');
            Route::get('detail/{invoice_id}', 'Popapps\TransactionController@getDetail');
            Route::get('getFileExcel/{groupName}','Popapps\TransactionController@downloadExcel');
            Route::post('getAjaxServerTransactionList/{groupName}', 'Popapps\TransactionController@getAjaxServerTransactionList');
            // Route::post('refund', 'PopApps\TransactionController@refundTransaction');
        });

        Route::group(['prefix' => 'delivery'], function () {
            Route::get('/', 'Popapps\DeliveryController@index');
            Route::get('detail/{invoice_code}/{invoice_id?}', 'Popapps\DeliveryController@getDetail')->name('delivery.detail');
            Route::get('getFileExcel','Popapps\DeliveryController@downloadExcel');
        });

        Route::group(['prefix' => 'users'], function () {
            Route::get('/', 'Popapps\UsersController@index')->name('users');
            Route::post('getAjaxServerUserList', 'Popapps\UsersController@getAjaxServerUserList');
            Route::get('getFileExcel','Popapps\UsersController@downloadExcel');
            Route::get('profile/{member_id}', 'Popapps\UsersController@getProfile');
            Route::get('getBalance/{member_id}/{startDate}/{endDate}', 'Popapps\UsersController@getBalanceBetweenDate');
            Route::post('credit', 'Popapps\UsersController@postCredit');
        });

        Route::group(['prefix' => 'notification'], function () {
            Route::get('/sms','Notification\SmsController@index')->name('dashboard.sms');
            Route::post('/sms-source','Notification\SmsController@source');
            Route::get('/sms-count','Notification\SmsController@getCount');
            Route::get('/excel','Notification\SmsController@excel');
        });

        Route::group(['prefix' => 'setting'], function () {
            // ================= hanya butuh 1 route yang didaftarkan =================
            Route::get('/audit','Setting\AuditController@index')->name('dashboard.audit');
            Route::post('/audit','Setting\AuditController@source');
            Route::get('/audit/{id}','Setting\AuditController@detail');
            // ========================================================================
            Route::get('/audit/excel','Setting\AuditController@excel');
            Route::get('/users-active','Setting\AuditController@getUser');
        });

        Route::group(['prefix' => 'report'], function () {
            Route::group(['prefix' => 'payment'], function () {
                Route::get('/client','Report\PaymentController@client')->name('report.payment.client');
                Route::post('/source-client','Report\PaymentController@sourceClient');
                Route::get('/client/{id}','Report\PaymentController@clientDetail')->name('report.payment.client.detail');
                Route::post('/client/update','Report\PaymentController@updateDetail')->name('report.payment.client.update');
                Route::get('/client-excel','Report\PaymentController@clientExcel');
                
                Route::get('/channel','Report\PaymentController@channel')->name('report.payment.channel');
                Route::post('/source-channel','Report\PaymentController@sourceChannel');
                Route::get('/channel/{id}','Report\PaymentController@detailChannel')->name('report.payment.edit');
                Route::post('/channel/{id}','Report\PaymentController@updateChannel')->name('report.payment.update');
                Route::get('/channel-excel','Report\PaymentController@channelExcel');
    
                Route::get('/clients','Report\PaymentController@getClient');
                Route::get('/transaction-list','Report\PaymentController@index')->name('report.payment.transaction');
                Route::post('/source-transaction','Report\PaymentController@sourceTransaction');
                Route::get('/transaction-excel','Report\PaymentController@transExcel');
                Route::get('/{subPaymentId}','Report\PaymentController@detail');
                Route::get('/amount','Report\PaymentController@calculateAmount');
                Route::post('/repush','Report\PaymentController@repush');            
            });
        });

        Route::group(['prefix' => 'marketing'], function () {
            Route::group(['prefix' => 'campaign'], function () {
                route::get('list/','Marketing\MarketingController@campaign');
                route::post('source-campaign/','Marketing\MarketingController@sourceCampaign');
                route::get('excel/','Marketing\MarketingController@excel');
                route::get('detail/{id}','Marketing\MarketingController@edit');
                route::post('update','Marketing\MarketingController@update');
                route::get('voucher/{id}','Marketing\MarketingController@voucher');
                route::post('create-voucher','Marketing\MarketingController@createVoucher');
                route::post('rule-form/','Marketing\MarketingController@buildHtml');
                route::post('helper/','Marketing\MarketingController@helper');
            });
            Route::group(['prefix' => 'locker'], function () {
                Route::get('/','Marketing\LockerController@view');
                Route::post('/','Marketing\LockerController@source');
                Route::get('getFileExcel','Marketing\LockerController@downloadExcel');
            });
        });

        Route::group(['prefix' => 'merchant'], function () {
            Route::group(['prefix' => 'company'], function () {
                Route::get('/','Merchant\CompanyController@view');
                Route::get('create','Merchant\CompanyController@create');
                Route::post('create','Merchant\CompanyController@insert');
                Route::get('edit/{id}','Merchant\CompanyController@edit');
                Route::post('edit/{id}','Merchant\CompanyController@update');
            });
        });

        Route::group(['prefix' => 'website'], function () {
            Route::group(['prefix' => 'article'], function () {
                Route::get('/','Website\ArticleController@view');
                Route::get('create','Website\ArticleController@create');
                Route::post('create','Website\ArticleController@store');
                Route::get('edit/{id}','Website\ArticleController@edit');
                Route::post('edit/{id}','Website\ArticleController@update');
                Route::get('delete/{id}', 'Website\CareerController@destroy');
            });
            Route::group(['prefix' => 'career'], function () {
                Route::get('/','Website\CareerController@view');
                Route::get('create','Website\CareerController@create');
                Route::post('create','Website\CareerController@store');
                Route::get('edit/{id}','Website\CareerController@edit');
                Route::post('edit/{id}','Website\CareerController@update');
                Route::get('delete/{id}', 'Website\CareerController@destroy');
            });
            Route::group(['prefix' => 'image'], function () {
                Route::get('/','Website\ImageController@view');
                Route::get('create','Website\ImageController@create');
                Route::post('create','Website\ImageController@store');
                Route::get('edit/{id}','Website\ImageController@edit');
                Route::post('edit/{id}','Website\ImageController@update');
                Route::get('delete/{id}', 'Website\ImageController@destroy');
            });
            Route::group(['prefix' => 'merchant'], function () {
                Route::get('/','Website\MerchantController@view');
                Route::get('create','Website\MerchantController@create');
                Route::post('create','Website\MerchantController@store');
                Route::get('edit/{id}','Website\MerchantController@edit');
                Route::post('edit/{id}','Website\MerchantController@update');
                Route::get('delete/{id}', 'Website\MerchantController@destroy');
            });
        });
        
        // ========================================================================
        // ============================= Angkasa Pura =============================
        // ========================================================================
        Route::group(['prefix' => 'angkasapura'], function () {
            Route::get('dashboard', 'Popbox\LockerController@index')->name('angkasapura.dashboard');
            Route::any('getAjaxGraph', 'Popbox\LockerController@getAjaxGraph');
            Route::get('payment', 'Popbox\LockerController@getPaymentTransactions');

            Route::group(['prefix' => 'locker'], function () {
                Route::get('location', 'Popbox\LockerController@APSLockerLocation');
            });
            
            Route::group(['prefix' => 'order'], function () {
                Route::get('/','Popapps\PopsafeController@getLandingAPS');
                Route::get('detail/{invoice_code}/{invoice_id?}', 'Popapps\PopsafeController@getDetail')->name('popsafe.detail');
                Route::get('getFileExcel/{groupName}','Popapps\PopsafeController@downloadExcel');
                Route::post('summary/{groupName}', 'Popapps\PopsafeController@postAjaxSummaryPopsafeList');
            });

            Route::group(['prefix' => 'transaction'], function () {
                Route::get('/','Popapps\TransactionController@getLandingAPS');
                Route::get('getFileExcel/{groupName}','Popapps\TransactionController@downloadExcel');
            });
        });
    });

});

Auth::routes();