<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateWebsiteCareer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!(Schema::connection('popbox_db')->hasColumn('tb_website_career', 'company')))
        {
            Schema::connection('popbox_db')->table('tb_website_career', function (Blueprint $table) {
                $table->string('category')->nullable();
                $table->string('company')->nullable();
                $table->string('location')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
