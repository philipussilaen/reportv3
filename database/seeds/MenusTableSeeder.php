<?php

use Illuminate\Database\Seeder;

class MenusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker\Factory::create(); //import library faker
        $limit = 20; //batasan jumlah data
        
        for ($i = 0; $i < $limit; $i++) {
                    DB::table('menus')->insert([ //mengisi data di database
                        'name' => $faker->name,
                        'parent_id' => $faker->numberBetween(1,20),
                        'url' => '#',
                        'icon' => 'fas fa-check'
                    ]);
        }
    }
}
