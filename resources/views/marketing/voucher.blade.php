@extends('layout.main')

@section('title')
	Create Campaign
@endsection

@section('css')
<link href="{{ asset('assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css') }}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/libs/daterangepicker/daterangepicker.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/libs/select2/dist/css/select2.min.css') }}">
@endsection

@section('content')
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="material-card card">
            <div class="card-body ext-white">
                <h3>Detail</h3>
                 <div class="row">
                     <div class="col-lg-6 col-md-6">
                            <ul class="list-group">
                                <li class="list-group-item">Name</li>
                                <li class="list-group-item">Periode</li>
                                <li class="list-group-item">Status</li>
                                <li class="list-group-item">Type/Amount</li>
                                <li class="list-group-item">Category</li>
                                <li class="list-group-item">Limit Usage</li>
                                <li class="list-group-item">Limit Type</li>
                            </ul>
                     </div>
                     <div class="col-lg-6 col-md-6">
                            <ul class="list-group">
                                <li class="list-group-item">{{ $detail->name }}</li>
                                <li class="list-group-item">{{ $detail->start_time }} -  {{ $detail->end_time }}</li>
                                <li class="list-group-item">{{ ($detail->status === '1') ? 'Active': 'Inactive' }}</li>
                                <li class="list-group-item">
                                        <span class="badge badge-primary">{{ ucfirst($detail->type ) }} </span> /{{ $detail->amount }}</li>
                                <li class="list-group-item">{{ $detail->category  }}</li>
                                <li class="list-group-item">{{ $detail->limit_usage }}</li>
                                <li class="list-group-item">{{ $detail->voucher_type}}</li>
                            </ul>
                     </div>
                 </div>
                 <div class="row">
                     <h3>Voucher Generator</h3>
                     <div class="col-lg-12 col-md-12">
                        <div class="row">
                            <div class="col-sm-6 col-lg-6">
                                    <div class="form-group">
                                    <label for="key" class="text-right control-label col-form-label">Jumlah Voucher</label>
                                    <input type="number" name="total" id="total" value="1" class='form-control' />
                                    <input type="hidden" name="campaign_id" id="campaign_id" value="{{ $detail->id }}" />
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-6">
                                    <div class="form-group">
                                    <label for="key" class="text-right control-label col-form-label">Generated Format</label>
                                    <br>
                                    <div class="btn-group mr-2" role="group" aria-label="First group">
                                        <button type="button" class="btn btn-primary" id="ordered_number">Ordered Number</button>
                                        <button type="button" class="btn btn-info" id="random_number">Random Number</button>
                                        <button type="button" class="btn btn-secondary" id="random_alpha">Random Alpha Numeric</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-6">
                                    <div class="form-group">
                                    <label for="key" class="text-right control-label col-form-label">Format Voucher(pisahhkan dengan '-' )</label>
                                    <input type="text" name="format" id="format" class='form-control' />
                                </div>
                            </div>
                            <div class="col-sm-6 col-lg-6">
                                <div class="form-group">
                                    <label for="key" class="text-right control-label col-form-label">Panjang Kode</label>
                                    <input type="number" name="length" id="length" class='form-control' placeholder="default: 4"/>
                                </div>
                            </div>
                            <div class="col-sm-12 col-lg-12">
                                <div class="form-group">
                                    <label for="key" class="text-right control-label col-form-label">Example Result</label>
                                    <input type="text" name="result" id='result' class='form-control' placeholder=""/>
                                </div>
                            </div>
                            <div class="col-sm-12 col-lg-12">
                                <button class="btn btn-success float-right" id="generate-code">Buat Kode</button>
                            </div>
                        </div>
                     </div>
                 </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="material-card card">
            <div class="card-body ext-white">
                <table class="table" id="list-code">
                    <thead>
                        <tr>
                            <th>Code</th>
                            <th>Usage</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($listVoucher as $key => $item)
                            <tr>
                                <td>{{ $item->code }}</td>
                                <td>{{ $item->usage }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script>
// send to ajax
$(document).ready(function() {
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
        });
})
$('#generate-code').click(function() {
    let length =$('#length').val();
    if(length.length < 1) { length = 4}
    const payload = {
        format: $('#format').val(),
        length: length,
        total: $('#total').val(),
        campaign_id: $('#campaign_id').val()
    }
    createVoucher(payload)
})

function createVoucher(payload) {
    $.ajax({
        type: "POST",
        url: "{{ url('marketing/campaign/create-voucher') }}",
        data: payload,
        success: function(response){
           $.each(response, function(index, val){
               const html = `<tr><td>${val}</td><td>0</td></tr>`;
               $("#list-code tbody").append(html);
           })
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            alert("Error, Cannot Create Voucher");
        }
    });
}
// generate format
$( "#ordered_number" ).click(function() {
    const textFormat = '{ordered_number}';
    const format = $('#format').val();
    let length  = $('#length').val()
    let newformat = textFormat;
    const start = 1;
    if(length.length < 1) {
        length = 4
    }
    if(format.length > 1){
        newformat = `${format}-${textFormat}`
    }
    $('#format').val(newformat);
    buildExample()
});
$( "#random_number" ).click(function() {
    const textFormat = '{random_number}';
    const format = $('#format').val();
    let length  = $('#length').val()
    let newformat = textFormat;
    if(length.length < 1) {
        length = 4
    }
    if(format.length > 1){
        newformat = `${format}-${textFormat}`
    }
    $('#format').val(newformat);
    buildExample()
});

$('#random_alpha').click(function() {
    const textFormat = '{random_alpha}';
    const format = $('#format').val();
    const result = $('#result').val();
    let length  = $('#length').val()
    let newformat = textFormat;
    if(length.length < 1) {
        length = 4
    }
    if(format.length > 1){
        newformat = `${format}-${textFormat}`
    }
    $('#format').val(newformat);
    buildExample()
})

function buildExample(){
    const format = $('#format').val();
    let length = $('#length').val();
    if(length.length < 1) { length = 4 }
    const exploded = format.split('-')
    let result='';
    $.each(exploded, function( index, value ) {
        if(value === '{ordered_number}'){
            result+=pad(1,length)
        }else if(value === '{random_number}'){
            result+=generateRandomNumber(length)
        }else if(value === '{random_alpha}'){
            result+=generateRandomAlphaNumber(length)
        }else{
            result+=value;
        }
    });
    $('#result').val(result)
}
function generateRandomNumber(length = 4){
        var chars = "0123456789";
        var string_length = length;
        var myrnd = [], pos;

        // loop as long as string_length is > 0
        while (string_length--) {
            // get a random number between 0 and chars.length - see e.g. http://www.shawnolson.net/a/789/make-javascript-mathrandom-useful.html
            pos = Math.floor(Math.random() * chars.length);
            // add the character from the base string to the array
            myrnd.push(chars.substr(pos, 1));
        }

        // join the array using '' as the separator, which gives us back a string
        var result = myrnd.join(''); // e.g "6DMIG9SP1KDEFB4JK5KWMNSI3UMQSSNT"
        return result;
    }
function pad(number, length) {
   
   var str = '' + number;
   while (str.length < length) {
       str = '0' + str;
   }
   return str;

}

function generateRandomAlphaNumber(length = 4){
        var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZ";
        var string_length = length;
        var myrnd = [], pos;

        // loop as long as string_length is > 0
        while (string_length--) {
            // get a random number between 0 and chars.length - see e.g. http://www.shawnolson.net/a/789/make-javascript-mathrandom-useful.html
            pos = Math.floor(Math.random() * chars.length);
            // add the character from the base string to the array
            myrnd.push(chars.substr(pos, 1));
        }

        // join the array using '' as the separator, which gives us back a string
        var result = myrnd.join(''); // e.g "6DMIG9SP1KDEFB4JK5KWMNSI3UMQSSNT"
        return result;
    }
    
</script>
@endsection