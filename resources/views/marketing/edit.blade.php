@extends('layout.main')

@section('title')
	Create Campaign
@endsection

@section('css')
<link href="{{ asset('assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css') }}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/libs/daterangepicker/daterangepicker.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/libs/select2/dist/css/select2.min.css') }}">
@endsection

@section('content')
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="material-card card">
            <div class="card-body ext-white">
                    <h3>Campaign</h3>
                <form action="{{ url('marketing/campaign/update/') }}" method="POST" enctype="multipart/form-data">
                    <div class="row">
                            @csrf
                            <input type="hidden" value="{{$detail->id}}" name="id" />
                            <div class="col-sm-4 col-lg-4">
                                <div class="form-group">
                                    <label for="key" class="text-right control-label col-form-label">Name</label>
                                        <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="{{ $detail->name }}" />
                                </div>
                            </div>
                            <div class="col-sm-4 col-lg-4">
                                <div class="form-group">
                                    <label for="key" class="text-right control-label col-form-label">Start Date - End Date</label>
                                    <input type="text" class="form-control date" id="date" name="date" placeholder="Campaign Date" value="{{ $startDate}} - {{  $endDate }}">
                                </div>
                            </div>
                            <div class="col-sm-4 col-lg-4">
                                <div class="form-group">
                                    <label for="key" class="text-right control-label col-form-label">Status</label>
                                    <select class="form-control select2" id="status_list" name="status" style="width: 100%; height:36px;">
                                        <option value="active" {{ $detail->status === '1' ? 'selected': ''}}>Active</option>
                                        <option value="inactive" {{ $detail->status === '0' ? 'selected': ''}}>In Active</option>
                                    </select>
        
                                </div>
                            </div>
                            <div class="col-sm-4 col-lg-4">
                                <div class="form-group">
                                    <label for="key" class="text-right control-label col-form-label">Campaign Type</label>
                                    <select class="form-control select2" id="campaign_type_list" name="campaign_type" style="width: 100%; height:36px;">
                                        <option value="potongan" {{ $detail->promo_type === 'potongan' ? 'selected': ''}}>Potongan</option>
                                        <option value="deposit" {{ $detail->promo_type === 'deposit' ? 'selected': ''}}>Deposit</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4 col-lg-4">
                                <div class="form-group">
                                    <label for="key" class="text-right control-label col-form-label">Priority</label>
                                    <select class="form-control select2" id="" name="priority" style="width: 100%; height:36px;">
                                            @foreach (range(1,10) as $item)
                                            <option value="{{ $item}}" {{ $detail->priority === $item  ? 'selected': ''}}>{{ $item }}</option>
                                            @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-4 col-lg-4">
                                <div class="form-group">
                                    <label for="key" class="text-right control-label col-form-label">Minimal Amount</label>
                                    <input type="number" class="form-control" name="minimal_amount" value="{{ $detail->min_amount }}" />
                                </div>
                            </div>

                            <div class="col-sm-4 col-lg-4">
                                <div class="form-group">
                                    <label for="key" class="text-right control-label col-form-label">Maximum Amount</label>
                                    <input type="number" class="form-control" name="maximum_amount" value="{{ $detail->max_amount }}" />
                                </div>
                            </div>

                            <div class="col-sm-4 col-lg-4">
                                <div class="form-group">
                                    <label for="key" class="text-right control-label col-form-label">Amount Type</label>
                                    <select class="form-control select2" id="" name="amount_type" style="width: 100%; height:36px;">
                                        <option value='fixed' {{ $detail->type === 'fixed' ? 'selected' : ''}}>FIXED</option>
                                        <option value='percent' {{ $detail->type === 'percent' ? 'selected' : ''}}>PERCENT</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-4 col-lg-4">
                                <div class="form-group">
                                    <label for="key" class="text-right control-label col-form-label">Promo Amount</label>
                                    <input type="number" name="amount" class="form-control" value="{{ $detail->amount }}" />
                                </div>
                            </div>

                            <div class="col-sm-4 col-lg-4">
                                <div class="form-group">
                                    <label for="key" class="text-right control-label col-form-label">Category</label>
                                    <select class="form-control select2" id="" name="category" style="width: 100%; height:36px;">
                                            <option value="fixed" {{ $detail->category === 'fixed' ? 'selected' : ''}}>Fixed</option>
                                            <option value="cutback" {{ $detail->category === 'cutback' ? 'selected' : ''}} >Cutback</option>
                                    </select>
                                    <ul>
                                        <li><strong>Fixed</strong>: Nilai Final yang harus dibayar <u>Menjadi</u> Nilai Promo</li>
                                        <li><strong>Cutback</strong>: Nilai Final yang harus dibayar <u>dikurangi</u>  Nilai Promo</li>
                                    </ul>
                                </div>
                            </div>

                            <div class="col-sm-4 col-lg-4">
                                <div class="form-group">
                                    <label for="key" class="text-right control-label col-form-label">Voucher Required</label>
                                    <select class="form-control select2" id="" name="voucher_required" style="width: 100%; height:36px;">
                                            <option value="yes" {{ $detail->voucher_required === '1' ? 'selected' : ''}}>Ya</option>
                                            <option value="no" {{ $detail->voucher_required === '0' ? 'selected' : ''}}>Tidak</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-4 col-lg-4">
                                <div class="form-group">
                                    <label for="key" class="text-right control-label col-form-label">Campaign Limit Type</label>
                                    <select class="form-control select2" id="" name="voucher_type" style="width: 100%; height:36px;">
                                            <option value="all" {{ $detail->voucher_type === 'all' ? 'selected' : ''}}>All</option>
                                            <option value="member" {{ $detail->voucher_type === 'member' ? 'selected' : ''}}>Member</option>
                                            <option value="voucher" {{ $detail->voucher_type === 'voucher' ? 'selected' : ''}}>Voucher</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-4 col-lg-4">
                                <div class="form-group">
                                    <label for="key" class="text-right control-label col-form-label">Limit Usage</label>
                                    <input type="number" name="limit_usage" class="form-control" id="" value="{{ $detail->limit_usage }}">
                                </div>
                            </div>

                            <div class="col-sm-4 col-lg-4">
                                <div class="form-group">
                                    <label for="key" class="text-right control-label col-form-label">Description</label>
                                    <textarea id="" name="description" class="form-control">{{ $detail->description }}
                                    </textarea>
                                </div>
                            </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <h3>Rule</h3>
                            <a href="javascript:void(1);" class="btn btn-primary add-rule">Add Rules</a> 
                                <input type="hidden" value="{{ $totalParameter }}" id='counter-rule'/>
                            <div class="row">
                                <div class="col-lg-12">
                                    @foreach ($detail->parameters as $key=>$itemParam)
                                         <div class='row' id='counter-{{ $key+1 }}'>
                                            <div class='col-lg-12'>
                                                <hr>
                                                <div class='form-group'>
                                                    <label>Parameter</label>
                                                    <select name='rules[{{ $key+1 }}][parameter]' rule='' id='parameter-{{ $key+1 }}'  class='form-control select2 rule-parameter'>
                                                        @foreach ($availableParamList as $item)
                                                             <optgroup label='{{ $item['paramCategory'] }}'>
                                                                    @foreach ($item['paramName'] as $value)
                                                                    <option value='{{ $value['id'] }}' name='{{ $value['name'] }}' {{ $itemParam->available_parameter_id === $value['id'] ? 'selected': '' }}>{{ $value['description'] }}  ( {{ $value['name'] }} )</option>
                                                                    @endforeach
                                                             </optgroup>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class='form-group'>
                                                    <label>Operator</label>
                                                <select name='rules[{{ $key+1 }}][operator]' rule='' id='operators-{{ $key+1 }}' class='form-control select2 rule-parameter'>
                                                        @foreach ($operatorList as $keys=>$val)
                                                            <option value="{{ $keys }}" {{ $itemParam->operator === $keys ? 'selected': '' }}>{{ $val }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class='form-group'>
                                                    <label>Value ( Jika lebih dari satu, pisahkan dengan koma (,) )</label>
                                                     <input type='text' class='form-control' name='rules[{{ $key+1 }}][value]' id='value-{{ $key+1 }}' value="{{ (is_array($itemParam->value)) ? explode(',',$itemParam->value) : $itemParam->value }}">
                                                    <br>
                                                    <a href='javascript:void(1)' class='btn btn-success float-right helper-rule' id='helper-{{ $key+1 }}'>Helper</a>
                                                </div>
                                                <div class='form-group'>
                                                    <a href='javascript:void(1)' class='btn btn-danger float-lef delete-rule' id='delete-{{ $key+1 }}'>Delete Rule</a>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                        
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12" id="box-rule">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <button class="btn btn-success float-right">Simpan</button>
                             <a href="{{url('marketing/campaign/list')}}" class="btn btn-danger float-right">Batal</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="detailModal">
    <div class="modal-dialog">
        <div class="modal-content">
    
        <!-- Modal Header -->
        <div class="modal-header">
            <h4 class="modal-title">Bantuan Untuk <span class="helper-text"></span></h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
    
        <!-- Modal body -->
            <div class="modal-body">
                <div class="row">
                    <input type="hidden" class="form-control" name="index" id="index" />
                    <input type="hidden" class="form-control" name="limit" id="limit" />
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="form-group">
                            <label for="helper" class="text-right control-label col-form-label">Value</label>
                            <select name="helper-value" id="selected-helper" class="form-control select2">
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a href="javascript:void(1);" class="btn btn-info float-right" id="save-value">Pilih</a>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="{{ asset('assets/libs/moment/moment.js') }}"></script>
<script src="{{ asset('assets/extra-libs/DataTables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/libs/daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('assets/libs/select2/dist/js/select2.min.js') }}"></script>
{{-- <script src="{{ asset('js/common.js') }}"></script> --}}

<script>
$(function() {
  $('input[name="date"]').daterangepicker({
    timePicker: true,
    // startDate: moment().startOf('hour'),
    // endDate: moment().startOf('hour').add(32, 'hour'),
    locale: {
      format: 'DD-MM-YYYY hh:mm A'
    }
  });
});

    $(document).ready(function() {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
        });
        $('.select2').select2();
        $('.add-rule').on('click',function() {
            let counter = $('#counter-rule').val()
                counter = parseInt(counter) + 1;
            const payload = {
                counter: counter
            }
            $('#counter-rule').val(counter)
            
            $.ajax({
                type: "POST",
                url: "{{ url('marketing/campaign/rule-form') }}",
                data: payload,
                success: function(msg){
                    $('#box-rule').append(msg);
                    $('.select2').select2();
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    alert("Error, Cannot Create Rule");
                }
            });
            
        })

    })
    $(document).on( 'click', '.delete-rule', function(){
        const id = this.id
        const explode = id.split('-')
        const counter = explode[1];
        $('#counter-'+counter).remove();
    });

    $('#save-value').on('click', function() {
        const index = $('#index').val();
        let value = $('#selected-helper').val();
        if(Array.isArray(value)) {
             value = value.join(',')
        }

        $('#value-'+index).val(value);
        $('#detailModal').modal('hide');

    })
    $(document).on('change', '#selected-helper', function(){
        const limit = $('#limit').val();
        if(limit == 2) {
            if ($(this).val().length > 2) {
                alert('Anda Hanya boleh memilih 2 saja');
                $('#save-value').hide();
            }else{
                $('#save-value').show();
            }
        }
    } );

    $(document).on( 'click', '.helper-rule', function(){
        const id = this.id
        const explode = id.split('-')
        const counter = explode[1];
        const text = $('#parameter-'+counter+' option:selected').text();
        const name = $('#parameter-'+counter+' option:selected').attr('name');
        const operator = $('#operators-'+counter).val();
        let multiple = false;
        let limit = 0;
        if(operator == 'between') {
            multiple = true;
            limit = 2;
        }
        if(operator == 'exist' || operator == 'except') {
            multiple = true;
        }
        $('#index').val(counter);
        $('.helper-text').html(text);
        const payload = {
            type: name
        }
        $.ajax({
                type: "POST",
                url: "{{ url('marketing/campaign/helper') }}",
                data: payload,
                success: function(response){
                    $('#selected-helper').html('');
                    $('#limit').val(limit);
                    if(multiple === true) {
                        $('#selected-helper').attr('multiple','multiple');
                    }
                    $.each(response, function(k, v) {
                        $('#selected-helper').append('<option value="'+k+'">'+v+'</option>')
                    });

                    $('#detailModal').modal('toggle');

                    $('.select2').select2();
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) {
                    alert("Error, Lookup helper lookup not available");
                }
            });
        
    } );

   
</script>
@endsection