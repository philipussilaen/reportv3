@extends('layout.main')

@section('title')
	Campaign List
@endsection

@section('css')
<link href="{{ asset('assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css') }}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/libs/daterangepicker/daterangepicker.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/libs/select2/dist/css/select2.min.css') }}">
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="material-card card">
                <form id="form-filter" class="form-horizontal">
                    <div class="card-body">
                        <h4 class="card-title">Filter 
                            <a id="linkFilter" data-toggle="collapse" href="#collapseFilter" role="button" aria-expanded="false" aria-controls="collapseFilter" style="float:right;"> <button id="btnCollapse" class="btn btn-secondary"> FILTER <i class="fas fa-chevron-right"></i> </button> </a>
                        </h4>
                        <div class="row button-group mb-4">
                            <div class="col-12" id="info-search">
                                
                            </div>
                        </div>
                        <div class="row button-group mb-4">
                            <div class="col-12">
                            </div>
                        </div>
                        <div class="collapse" id="collapseFilter">
                        <hr>
                        <div class="row">
                            <div class="col-sm-12 col-lg-4">
                                <div class="form-group">
                                    <label for="key" class="text-right control-label col-form-label">Name</label>
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Name" />
                                </div>
                            </div>
                            <div class="col-sm-12 col-lg-4">
                                <div class="form-group">
                                    <label for="key" class="text-right control-label col-form-label">Status</label>
                                    <select class="form-control select2" id="status_list" name="status" style="width: 100%; height:36px;">
                                        <option value="" selected="selected">All</option>
                                        <option value="enable">Active</option>
                                        <option value="disable" >Disabled</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12 col-lg-4">
                                <div class="form-group">
                                    <label for="key" class="text-right control-label col-form-label">Campaign Type</label>
                                    <select class="form-control select2" id="campaign_type_list" name="campaign_type" style="width: 100%; height:36px;">
                                        <option value="" selected="selected">All</option>
                                        <option value="fixed" >Fixed</option>
                                        <option value="percent" >Percent</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12 col-lg-4">
                                <div class="form-group">
                                    <label for="key" class="text-right control-label col-form-label">Voucher</label>
                                    <input type="text" class="form-control" id="voucher" name="voucher" placeholder="Voucher" />
                                </div>
                            </div>
                            <div class="col-sm-12 col-lg-4">
                                <div class="form-group">
                                    <label for="date" class="text-right control-label col-form-label">Start Date - End Date</label>
                                    <input type="text" class="form-control daterange" id="daterange" name="date-range" placeholder="Transaction Date">
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group mb-0 text-right">
                            <a href="javascript:void(1);" id="btnFilter" class="btn btn-info waves-effect waves-light">Filter</a>
                            <a href="javascript:void(1);" id="btnReset" class="btn btn-dark waves-effect waves-light">Reset</a>
                        </div>
                        </div>
                    </div>
                </form>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="material-card card">
            <div class="card-body row">
                <div class="col-sm-12 col-md-6">
                    <h4 class="card-title">Campaign</h4>
                    <h6 class="card-subtitle"></h6>
                </div>
                <div class="col-sm-12 col-md-6">
                    <a id="download" class="btn btn-primary mb-3 float-right" href="javascript:void(1);" > Download </a>
                    {{-- <a href="{{ url('marketing/campaign/create') }}" class="btn btn-success float-right">Create Campaign</a> --}}
                </div>
                <div class="table-responsive">
                    <table id="source-table" class="table table-striped border text-inputs-searching" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Status</th>
                                    <th>Campaign Type</th>
                                    <th>Type/Amount</th>
                                    <th>Category</th>
                                    <th>Voucher/Limit Type</th>
                                    <th>Limit Usage</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Status</th>
                                    <th>Campaign Type</th>
                                    <th>Type/Amount</th>
                                    <th>Category</th>
                                    <th>Voucher/Limit Type</th>
                                    <th>Limit Usage</th>
                                    <th>Action</th>
                                </tr>
                            </tfoot>
                        </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="{{ asset('assets/libs/moment/moment.js') }}"></script>
<script src="{{ asset('assets/extra-libs/DataTables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/libs/daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('assets/libs/select2/dist/js/select2.min.js') }}"></script>
<script src="{{ asset('js/common.js') }}"></script>

<script>
    $(document).ready(function() {
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            });
        $('.select2').select2();
        getTable({
            "date": $('#daterange').val()
        });
    })
function getTable(param = {}) {
    
    return $('#source-table').DataTable( {
          processing: true,
          serverSide: true,
          bFilter: false,
          ajax: {
              url: "{{ url('marketing/campaign/source-campaign') }}",
              dataType: "json",
              type: "POST",
              data: param
          },
          columns: [
            { 
            "data": "name",
            "name": "name",
            "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            { 
            "data": "description",
            "name": "description",
            "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            { 
            "data": "start_date",
            "name": "start_date",
            "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            { 
            "data": "end_date",
            "name": "end_date",
            "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            { 
            "data": "status",
            "name": "status",
            "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            { 
            "data": "type",
            "name": "type",
            "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            { 
            "data": "amount",
            "name": "amount",
            "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            { 
            "data": "category",
            "name": "category",
            "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            { 
            "data": "voucher",
            "name": "voucher",
            "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            { 
            "data": "limit",
            "name": "limit",
            "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            { 
            "data": "",
            "name": "",
            "render": function ( data, type, full, meta ) {
                 return "<a class='btn btn-info' href='{{ url('marketing/campaign/detail/') }}/"+full.id+"'>Edit</a> | <a class='btn btn-success' href='{{ url('marketing/campaign/voucher/') }}/"+full.id+"'>Voucher</a>";
                }
            },
          ]
          
      } );
}
$('#btnFilter').on('click', function() {
    $('#source-table').DataTable().clear().destroy();
    const payload = {
        "date": $('#daterange').val(),
        "name": $('#name').val(),
        "status": $('#status_list').val(),
        "type": $('#campaign_type_list').val(),
        "voucher":$('#voucher').val(),
    }
    getTable(payload)
})
$('#btnReset').on('click', function() {
    $('#source-table').DataTable().clear().destroy();
    resetDate();
    $('#name').val('')
    $('#voucher').val('')
    $('#campaign_type_list').val('').trigger('change') 
    $('#status_list').val('').trigger('change') 
    const payload = {
        "date": $('#daterange').val()
    }
    getTable(payload)
})

function resetDate() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; // January is 0

    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    var today = yyyy + '-' + mm + '-' + dd;
    var startDate = yyyy + '-' + mm + '-' + '01';
    $('#daterange').daterangepicker({
        startDate: startDate,
        maxDate: today,
        alwaysShowCalendars: true,
        locale: {
            format: 'YYYY-MM-DD'
        }
    })

}

$('#download').on('click', function () {
        var search = $('#form-filter').serialize();
        window.location.href = "{{ url('marketing/campaign/excel/') }}?"+search;
    });
</script>
@endsection