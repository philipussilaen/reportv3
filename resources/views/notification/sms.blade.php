@extends('layout.main')

@section('title')
	Notifikasi SMS
@endsection

@section('css')
<link href="{{ asset('assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css') }}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/libs/daterangepicker/daterangepicker.css') }}">
@endsection

@section('breadcrumb')
    {{-- expr --}}
@endsection

@section('content')
<div class="row">
<div class="col-lg-12" style="padding-bottom:5px;">
        <a href="javascript:void(1);"
         class="btn btn-info float-right"
         data-toggle="modal" data-target="#helpModal"
         >Help <i class="fa fa-question-circle" aria-hidden="true"></i>
        </a>
</div>
@foreach ($status as $statusName)
    <div class="col-sm-4 col-md-3">
        <a id="created" class="filter" href="javascript:void(0)">
            <div class="card bg-light">
                <div class="card-body">
                    <div class="d-flex no-block align-items-center">
                        <div>
                        <h2 id="total-{{ $statusName }}" class="count font-medium mb-0">0</h2>
                            <hr>
                        <h5 class="filterVal" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="{{ ucfirst($statusName) }}">{{ ucfirst($statusName) }}</h5>
                        </div>
                        <div class="ml-auto">
                            <span class="display-6"><i class="fa fa-envelope"></i></span>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    @endforeach
</div>

<div class="row">
    <div class="col-12">
        <div class="material-card card">
            <form id="form-filter" class="form-horizontal">
                <div class="card-body">
                    <h4 class="card-title">Filter 
                        <a id="linkFilter" data-toggle="collapse" href="#collapseFilter" role="button" aria-expanded="false" aria-controls="collapseFilter" style="float:right;"> <button id="btnCollapse" class="btn btn-secondary"> FILTER <i class="fas fa-chevron-right"></i> </button> </a>
                    </h4>
                    <div class="row button-group mb-4">
                        <div class="col-12">
                        </div>
                    </div>
                    <div class="collapse" id="collapseFilter">
                    <hr>
                    <div class="row">
                        <div class="col-sm-12 col-lg-4">
                            <div class="form-group">
                                <label for="invoiceId" class="text-right control-label col-form-label">Phone Number</label>
                                <input type="text" class="form-control" id="phone-number" name="phone" placeholder="Phone Number" onkeypress="return isNumber(event);">
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-4">
                            <div class="form-group">
                                <label for="status" class="text-right control-label col-form-label">Status</label>
                                <select id="status-sms" class="form-control select2" name="status" style="width: 100%; height:36px;">
                                    <option value="">All Status</option>
                                    @foreach ($status as $statusName)
                                    <option value="{{ $statusName }}">{{ ucfirst($statusName) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-4">
                                <div class="form-group">
                                    <label for="date" class="text-right control-label col-form-label">Sms Timestamp</label>
                                    <input type="text" class="form-control daterange" id="date-range" name="date-range" placeholder="Transaction Date">
                                </div>
                            </div>
                        <div class="col-sm-12 col-lg-4">
                        </div>
                    </div>
                    <hr>
                    <div class="form-group mb-0 text-right">
                        <a href="javascript:void(1);" id="btnFilter" class="btn btn-info waves-effect waves-light">Filter</a>
                        <a href="javascript:void(1);" id="btnReset" class="btn btn-dark waves-effect waves-light">Reset</a>
                    </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-12">
        <div class="material-card card">
            <div class="card-body row">
                <div class="col-sm-12 col-md-6">
                    <h4 class="card-title">SMS</h4>
                    <h6 class="card-subtitle">List Nexmo Webhook</h6>
                </div>
                <div class="col-sm-12 col-md-6">
                    <button id="popExcel" class="btn btn-primary mb-3" style="float:right;"> Download </button>
                    
                </div>
                <div class="table-responsive">
                    <table id="sms-table" class="table table-striped border text-inputs-searching" style="width:100%">
                        <thead>
                            <tr>
                                <th>Phone Number</th>
                                <th>Message ID</th>
                                <th>Status</th>
                                <th>Error Code</th>
                                <th>SCTS</th>
                                <th>Network Code</th>
                                <th>Message Timestamp</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Phone Number</th>
                                <th>Message ID</th>
                                <th>Status</th>
                                <th>Error Code</th>
                                <th>SCTS</th>
                                <th>Network Code</th>
                                <th>Message Timestamp</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="helpModal">
        <div class="modal-dialog">
          <div class="modal-content">
      
            <!-- Modal Header -->
            <div class="modal-header">
              <h4 class="modal-title">Legend</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
      
            <!-- Modal body -->
            <div class="modal-body">
                    <table class="table">
                            <thead>
                              <tr>
                                <th scope="col">#</th>
                                <th scope="col">Kode</th>
                                <th scope="col">Deskripsi</th>
                              </tr>
                            </thead>
                            <tbody>
                                @foreach ($legend as $key=>$info)
                                    <tr>
                                        <td>{{ $key+1}}</td>
                                        <td>{{ $info['code']}}</td>
                                        <td>{!! $info['description'] !!}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                          </table>

                    <p>
                        <a class="btn btn-primary" data-toggle="collapse" href="#errorInfo" role="button" aria-expanded="false" aria-controls="errorInfo">
                            Informasi Kode Error
                        </a>
                    </p>
                    <div class="collapse" id="errorInfo">
                        <div class="card card-body">
                            <code>
                                Informasi error ini berasal dari langsung dari Nexmo bukan kode kustom
                            </code>
                            <table class="table">
                                <thead>
                                    <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Kode</th>
                                    <th scope="col">Deskripsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($error as $key=>$code)
                                        <tr>
                                            <td>{{ $key+1}}</td>
                                            <td>{{ $code['code']}}</td>
                                            <td>{!! $code['description'] !!}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
            </div>
      
            <!-- Modal footer -->
            <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
      
          </div>
        </div>
      </div>
@endsection


@section('js')
<script src="{{ asset('assets/libs/moment/moment.js') }}"></script>
<script src="{{ asset('assets/extra-libs/DataTables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/libs/daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('js/common.js') }}"></script>

<script>
$(document).ready(function() {
    getSms({
        "_token" : "{{ csrf_token() }}",
        "date": $('#date-range').val()
    })

    groupCount();

    var transactionDate = $('#transactionDate').val();
    if(transactionDate != '') { $('#btnDate').show(); $('#btnDate').text('Date : ' + transactionDate); }
})

function groupCount() {
    $.ajax({
        url: "{{ url('notification/sms-count') }}",
        type: 'GET',
        data: {
            "_token" : "{{ csrf_token() }}",
        },
        success: function(data){ 
            $.each(data, function( index, value ) {
                // console.log( index + ": " + value );
                $('#total-'+value.status).html(value.total)
            });
        },
        error: function(data) {
            console.log('something went wrong')
        }
    })
}
$("#btnFilter").on('click',function(){
    $('#sms-table').DataTable().clear().destroy();
    getSms({
        "_token" : "{{ csrf_token() }}",
        "phone": $('#phone-number').val(),
        "status": $('#status-sms').val(),
        "date": $('#date-range').val()
    })
})

$("#btnReset").on('click',function(){
    $('#sms-table').DataTable().clear().destroy();
    $('#phone-number').val(''),
    $('#status-sms').val('')
    $('#date-range').val('')
    getSms({
        "_token" : "{{ csrf_token() }}",
    })
})
$('#popExcel').click(function () {
    var search = $('#form-filter').serialize();
    var phone = $('#phone-number').val()
    var status = $('#status-sms').val();
    if(phone.length > 0 || status.length > 0) {
        window.location.href = "{{ url('notification/excel/') }}?"+search;
    }else{
        alert('Masukan filter terlebih dahulu!')
        // console.log('kosong ')
    }
});

function getSms(param = {}) {
    
  return $('#sms-table').DataTable( {
        processing: true,
        serverSide: true,
        order: [['6', 'desc']],
        bFilter: false,
        ajax: {
            url: "{{ url('notification/sms-source') }}",
            dataType: "json",
            type: "POST",
            data: param
        },
        columns: [
            { 
            "data": "phone",
            "name": "phone",
            "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            { 
            "data": "message_id",
            "name": "message_id",
            "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            { 
            "data": "status",
            "name": "status",
            "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            { 
            "data": "code",
            "name": "code",
            "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            { 
            "data": "scts",
            "name": "scts",
            "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            { 
            "data": "network",
            "name": "network",
            "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            { 
            "data": "message_time",
            "name": "message_time",
            "render": function ( data, type, full, meta ) {
                    return data;
                }
            }
        ],
        
    } );
}

function isNumber(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;
}

gtag('config', 'UA-72128831-7',{
    'page_title' : 'Notifikasi SMS',
    'page_path': '/notification/sms'
  });
</script>
@endsection