@extends('layout.main')
{{-- Do Not Edit this file. This is Base File for View --}}

@section('title')
	Users
@endsection

@section('css')
<link href="{{ asset('assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css') }}" rel="stylesheet">
@endsection

@section('breadcrumb')
    {{-- expr --}}
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="material-card card">
            <div class="card-body">
                <h4 class="card-title">List of Users</h4>
                <h6 class="card-subtitle">
                    Default password : 
                    <code>popdash123</code>
                </h6>
                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
                @if (session('error'))
	                <div class="alert alert-danger">
	                    {{ session('error') }}
	                </div>
	            @endif
                <p><a href="{{ url('user/create') }}"><button id="btn-add-user" class="btn btn-info"><i class="fas fa-plus"></i> Add</button></a></p>
                <div class="table-responsive">
                    <table id="zero_config" class="table table-striped border">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                            //echo $menus;
                            //print_r($parent);
                            if(!is_null($list)) {
                            @endphp
                            @foreach($list as $row)
                            <tr>
                                <td>{{ $row->name }}</td>
                                <td>{{ $row->email }}</td>
                                <td>
                                    <a href="{{ url('user/edit')."?id=$row->id" }}"><button class="btn-edit-user btn btn-info" data-toggle="tooltip" data-placement="top" title="Edit User Group"><i class="fas fa-edit"></i></button></a>
                                    <a href="{{ url('user/destroy')."?id=$row->id" }}"><button class="btn btn-danger" onclick="return confirm('Yakin ingin menghapus data?')" data-toggle="tooltip" data-placement="top" title="Delete User"><i class="fas fa-trash"></i></button></a>
                                    <a href="{{ url('user/reset')."?id=$row->id" }}"><button class="btn btn-success" onclick="return confirm('Yakin mengatur ulang kata sandi?')" data-toggle="tooltip" data-placement="top" title="Reset Password"><i class="fas fa-lock-open"></i></button></a>
                                </td>
                            </tr>
                            @endforeach
                            @php
                            } else {
                                echo '-';
                            }
                            @endphp
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Action</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script src="{{ asset('assets/extra-libs/DataTables/datatables.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-confirmation/1.0.7/bootstrap-confirmation.min.js"></script>
    <script type="text/javascript">
		jQuery(document).ready(function($) {
			$('#zero_config').DataTable();

            /*$('#zero_config').DataTable( {
                ajax: {
                    url: '/api/myData',
                },
                columns: [ ... ]
            } );*/
        });
	</script>
@endsection