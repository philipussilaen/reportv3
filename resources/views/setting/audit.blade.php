@extends('layout.main')

@section('title')
	Audit Trail
@endsection

@section('css')
<link href="{{ asset('assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css') }}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/libs/daterangepicker/daterangepicker.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/libs/select2/dist/css/select2.min.css') }}">
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="material-card card">
                <form id="form-filter" class="form-horizontal">
                        <div class="card-body">
                            <h4 class="card-title">Filter 
                                <a id="linkFilter" data-toggle="collapse" href="#collapseFilter" role="button" aria-expanded="false" aria-controls="collapseFilter" style="float:right;"> <button id="btnCollapse" class="btn btn-secondary"> FILTER <i class="fas fa-chevron-right"></i> </button> </a>
                            </h4>
                            <div class="row button-group mb-4">
                                <div class="col-12" id="info-search">
                                    
                                </div>
                            </div>
                            <div class="row button-group mb-4">
                                <div class="col-12">
                                </div>
                            </div>
                            <div class="collapse" id="collapseFilter">
                            <hr>
                            <div class="row">
                                <div class="col-sm-12 col-lg-4">
                                    <div class="form-group">
                                        <label for="key" class="text-right control-label col-form-label">Key</label>
                                        <input type="text" class="form-control" id="key" name="key" placeholder="key reference" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-lg-4">
                                    <div class="form-group">
                                        <label for="module" class="text-right control-label col-form-label">Module</label>
                                        <input type="text" class="form-control" id="module" name="module" placeholder="Module" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-lg-4">
                                    <div class="form-group">
                                        <label for="type" class="text-right control-label col-form-label">Type</label>
                                        <select class="form-control select2" id="type" name="type" style="width: 100%; height:36px;">
                                                <option value="">All</option>
                                            @foreach ($type as $val)
                                                <option value="{{ $val }}">{{ ucfirst($val) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-lg-4">
                                    <div class="form-group">
                                        <label for="user" class="text-right control-label col-form-label">User</label>
                                        <select class="form-control select2" id="user-list" name="user" style="width: 100%; height:36px;">
                                                <option value="" selected="selected">All</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-lg-4">
                                    <div class="form-group">
                                        <label for="date" class="text-right control-label col-form-label">Rentang Waktu</label>
                                        <input type="text" class="form-control daterange" id="daterange" name="date-range" placeholder="Transaction Date">
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group mb-0 text-right">
                                <a href="javascript:void(1);" id="btnFilter" class="btn btn-info waves-effect waves-light">Filter</a>
                                <a href="javascript:void(1);" id="btnReset" class="btn btn-dark waves-effect waves-light">Reset</a>
                            </div>
                            </div>
                        </div>
                    </form>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="material-card card">
            <div class="card-body row">
                <div class="col-sm-12 col-md-6">
                    <h4 class="card-title">Audit</h4>
                    <h6 class="card-subtitle">Log Activity</h6>
                </div>
                <div class="col-sm-12 col-md-6">
                    <button id="popExcel" class="btn btn-primary mb-3" style="float:right;"> Download </button>
                </div>
                <div class="table-responsive">
                    <table id="audit-table" class="table table-striped border text-inputs-searching" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Module</th>
                                    <th>Key</th>
                                    <th>Type</th>
                                    <th>Before</th>
                                    <th>After</th>
                                    <th>User</th>
                                    <th>Created At</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Module</th>
                                    <th>Key</th>
                                    <th>Type</th>
                                    <th>Before</th>
                                    <th>After</th>
                                    <th>User</th>
                                    <th>Created At</th>
                                </tr>
                            </tfoot>
                        </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal" id="detailModal">
    <div class="modal-dialog">
        <div class="modal-content">
    
        <!-- Modal Header -->
        <div class="modal-header">
            <h4 class="modal-title">Trail</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
    
        <!-- Modal body -->
            <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6 col-md-6 col-lg-6">
                            <div class="form-group">
                                <label for="invoiceId" class="text-right control-label col-form-label">Module</label>
                                <input type="text" class="form-control" readonly id="detail-module" value="">
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-6">
                            <div class="form-group">
                                <label for="invoiceId" class="text-right control-label col-form-label">Type</label>
                                <input type="text" class="form-control" readonly id="detail-type" value="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-md-6 col-lg-6">
                            <div class="form-group">
                                <label for="invoiceId" class="text-right control-label col-form-label">User</label>
                                <input type="text" class="form-control" id="detail-user" readonly value="">
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-6">
                            <div class="form-group">
                                <label for="invoiceId" class="text-right control-label col-form-label">Created At</label>
                                <input type="text" class="form-control" id="detail-created" readonly value="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class="form-group">
                                <label for="invoiceId" class="text-right control-label col-form-label">Before</label>
                                <textarea class="form-control" id="detail-before" readonly></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class="form-group">
                                <label for="invoiceId" class="text-right control-label col-form-label">After</label>
                                <textarea class="form-control" id="detail-after" readonly></textarea>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="{{ asset('assets/libs/moment/moment.js') }}"></script>
<script src="{{ asset('assets/extra-libs/DataTables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/libs/daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('assets/libs/select2/dist/js/select2.min.js') }}"></script>
<script src="{{ asset('js/common.js') }}"></script>

<script>
$(document).ready(function() {
    $('.select2').select2();

    $('#user-list').select2({
        ajax: {
            url: '{{ url("setting/users-active") }}',
            dataType: 'json',
            data: function (params) {
                var query = {
                    search: params.term,
                }
                // Query parameters will be ?search=[term]&page=[page]
                return query;
            },
            processResults: function(data) {
                return {
                        results: $.map(data, function(obj) {
                            return {
                                id: obj.id,
                                text: obj.text
                            };
                        })
                    };
            }
        }
    });

    getTable({
        "_token" : "{{ csrf_token() }}",
        "date": $('#daterange').val()
    });
})

$('#btnFilter').on('click', function() {
    $('#audit-table').DataTable().clear().destroy();
    const payload = {
        "_token" : "{{ csrf_token() }}",
        "date": $('#daterange').val(),
        "key": $('#key').val(),
        "module": $('#module').val(),
        "type": $('#type').val(),
        "user_id": $('#user-list').val()
    }
    getTable(payload)
})


$('#btnReset').on('click', function() {
    $('#audit-table').DataTable().clear().destroy();
    resetDate();
    $('#key').val('')
    $('#module').val('')
    $('#type').val('').trigger('change') 
    $('#user-list').val('').trigger('change') 
    const payload = {
        "_token" : "{{ csrf_token() }}",
        "date": $('#daterange').val()
    }
    getTable(payload)
})

function resetDate() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; // January is 0

    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    var today = yyyy + '-' + mm + '-' + dd;
    var startDate = yyyy + '-' + mm + '-' + '01';
    $('#daterange').daterangepicker({
        startDate: startDate,
        maxDate: today,
        alwaysShowCalendars: true,
        locale: {
            format: 'YYYY-MM-DD'
        }
    })
}

$('#popExcel').click(function () {
    var search = $('#form-filter').serialize();
    window.location.href = "{{ url('setting/audit/excel/') }}?"+search;
});

$('#audit-table').on('click', 'a',function() {
    const id = $(this).attr('id');
    $.ajax({
        url: "{{ url('setting/audit') }}/"+id,
        type: 'GET',
        success: function(data){ 
            console.log(data.data.id)
            $("#detail-module").val(data.data.module)
            $("#detail-type").val(data.data.type)
            $("#detail-user").val(data.data.user)
            $("#detail-created").val(data.data.created_at)
            $("#detail-before").val(data.data.before)
            $("#detail-after").val(data.data.after)
            $('#detailModal').modal('toggle')
        },
        error: function(data) {
            alert('Terjadi kesalahan, hubungi team IT!'); //or whatever
        }
    });
    
})
function getTable(param = {}) {
    
    return $('#audit-table').DataTable( {
          processing: true,
          serverSide: true,
          bFilter: false,
          ajax: {
              url: "{{ url('setting/audit') }}",
              dataType: "json",
              type: "POST",
              data: param
          },
          columns: [
            { 
            "data": "module",
            "name": "module",
            "render": function ( data, type, full, meta ) {
                    return '<a href="javascript:void(1);" class="modal-detail" id="'+full.id+'" >'+data+'</a>';
                }
            },
            { 
            "data": "key",
            "name": "key",
            "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            { 
            "data": "type",
            "name": "type",
            "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            { 
            "data": "before",
            "name": "before",
            "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            { 
            "data": "after",
            "name": "after",
            "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            { 
            "data": "user",
            "name": "user",
            "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
            { 
            "data": "created_at",
            "name": "created_at",
            "render": function ( data, type, full, meta ) {
                    return data;
                }
            },
        ]
          
      } );
  }
  gtag('config', 'UA-72128831-7',{
    'page_title' : 'Audit Trail',
    'page_path': '/setting/audit'
  });
</script>
@endsection