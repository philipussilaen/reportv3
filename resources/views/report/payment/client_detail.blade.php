@extends('layout.main')

@section('title')
	Transaction CLient Detail
@endsection

@section('css')
<link href="{{ asset('assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="row">
        <div class="col-md-4 col-sm-12">
            <div class="material-card card">
                <div class="card-body bg-primary text-white">
                <h2 class="card-title" style="font-size: 24px;">{{ $data->name }}</h2>
                    <h6>Token</h6>
                <p>{{ $data->token }}</p>
                </div>
            </div>
        </div>
        <div class="col-md-8 col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Nama</th>
                                        <th>Code</th>
                                        <th>Channel Status</th>
                                        <th>Client Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($data->channels as $row)
                                        <tr>
                                            <td>{{ $row->name}}</td>
                                            <td>{{ $row->code}}</td>
                                            <td>{{ $row->status}}</td>
                                            <td>
                                                <a href="javascript:void(1)" onclick="changeStatus({{ $row->pivot->id}})">
                                                    @if ($row->pivot->status == 'OPEN')
                                                        <span class="badge badge-success">OPEN</span>
                                                    @else 
                                                        <span class="badge badge-danger">{{ $row->status }}</span>
                                                    @endif
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
        </div>
</div>
<div class="modal" id="detailModal">
    <div class="modal-dialog">
        <div class="modal-content">
    
        <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Ubah Status</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <input type="hidden" id="id-pivot" value="" >
                <h3 id="infoLoad"></h3>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-success" id="updates">Ubah</button>
            </div>
        
        </div>
    </div>
</div>
@endsection
@section('js')
<script>
    function changeStatus(id) {
        $('#id-pivot').val(id)
        $('#detailModal').modal('toggle')
    }
    $("#updates").on('click',function() {
        $("#infoLoad").html('Tunggu sebentar');
        const id = $('#id-pivot').val();
        const param = {
            "_token" : "{{ csrf_token() }}",
            "id": id
        }
        $.ajax({
        type: "POST",
        url: "{{ url('report/payment/client/update/') }}",
        data: param,
        success: function(msg){
                alert( "Data Saved: " + msg );
                $("#infoLoad").html('');
                window.location.reload(true)
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            alert("Call Admin!!!");
            $("#infoLoad").html('');
        }
        });
    })
</script>  
@endsection