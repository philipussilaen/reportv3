@extends('layout.main')

@section('title')
	Transaction Detail
@endsection

@section('css')
<link href="{{ asset('assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css') }}" rel="stylesheet">
<style>
table th:nth-child(1) {
    padding-left: 1.57rem;
}
</style>
@endsection

@section('content')
<div class="row">
    <div class="col-md-4 col-sm-12">
        <div class="material-card card">
            <div class="card-body bg-info text-white">
            <h2 class="card-title" style="font-size: 24px;">{{ $detail->parent->payment_id}}</h2>
                <h6>Payment Channel</h6>
            <p>{{ $detail->parent->channel->name }}</p>
            </div>
            <div class="table-responsive">
                <table class="table">
                    <tbody>
                    <tr>
                        <th scope="row">STATUS</th>
                        <td>
                        <span class="{{ $classBadge[$detail->parent->status] }}">{{ $detail->parent->status}}</span>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">Payment Id</th>
                        <td>
                           {{ $detail->parent->payment_id}}
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">Sub Payment ID</th>
                        <td>
                            {{ $detail->sub_payment_id }}
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">Amount</th>
                        <td>
                        Rp{{ $helper->formatNumber($detail->paid_amount) }}
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">Time</th>
                        <td>
                        {{ $helper->formatDate($detail->created_at) }}
                        </td>
                    </tr>
                    </tbody>
                </table> 
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col text-center">
                        @if ($repush)
                             <button class="btn btn-danger" data-toggle="modal" data-target="#detailModal">Repush</button>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-8 col-sm-12">
        <div class="card">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs customtab" role="tablist"><li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#detail" role="tab"><span class="hidden-sm-up"></span><i class="icon-wallet"></i> <span class="hidden-xs-down">Payment Detail</span></a> </li>
                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#smslog" role="tab"><span class="hidden-sm-up"></span><i class="fas fa-user"></i> <span class="hidden-xs-down">Customer Detail</span> </a> </li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content p-4">
                <!-- DETAIL -->
                <div class="tab-pane active" id="detail" role="tabpanel">  
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                                <table class="table">
                                        <tbody>
                                            <tr>
                                                <th scope="row">Client Transaction ID</th>
                                                <td>
                                                    @if ($detail->parent->company_access_tokens_id == '9')
                                                        <a href="{{ url('transaction/detail') }}/{{ $detail->parent->transaction_id }}" target="_blank">{{ $detail->parent->transaction_id }}</a>
                                                    @else
                                                        {{ $detail->parent->transaction_id }}
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Description</th>
                                                <td>{{ $detail->description }}</td>
                                            </tr>
                                            <tr>
                                                <th scope="row">VA Number</th>
                                                <td><span id="vaNumber">{{ $detail->parent->virtual_number }}</span> &nbsp;&nbsp;&nbsp;
                                                    @if ($detail->parent->virtual_number != null)
                                                    <a href="javascript:void(0)" id="copyVA" onclick="copyToClipboard('#vaNumber')"  data-toggle="tooltip" title="VA Number Copied"><i class="far fa-copy"></i></a>
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <th scope="row">Expired Time</th>
                                                <td>{{ $helper->formatDate($detail->parent->expired) }}</td>
                                            </tr>
                                        </tbody>
                                </table>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-body">
                            <div class="table-responsive">
                                <h3>Detail Amount</h3>
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <th scope="row">Paid Amount <i class="fas fa-info-circle" data-toggle="tooltip" data-placement="top" title="Jumlah yang ditransfer pelanggan"></i></th>
                                            <td>Rp{{ $helper->formatNumber($detail->paid_amount) }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Customer Admin Fee <i class="fas fa-info-circle" data-toggle="tooltip" data-placement="top" title="Biaya admin yang dibebankan ke pelanggan"></i></th>
                                            <td>Rp{{ $helper->formatNumber($detail->customer_admin_fee) }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Total Amount <i class="fas fa-info-circle" data-toggle="tooltip" data-placement="top" title="Jumlah yang diterima di akun pelanggan"></i></th>
                                            <td>Rp{{ $helper->formatNumber($detail->amount) }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Total Amount VA <i class="fas fa-info-circle" data-toggle="tooltip" data-placement="top" title="Akumulasi yang pernah dilakukan transfer terhadap nomor VA, berlaku akumulasi jika billing type OPEN"></i></th>
                                            <td>Rp{{ $helper->formatNumber($detail->parent->total_amount) }}</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Admin Fee Payment Gateway</th>
                                            <td>Rp{{ $helper->formatNumber($detail->channel_admin_fee) }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                    </div>
                </div>
                    <div class="card">
                        <div class="card-body">
                                <div class="table-responsive">
                                    <h3>Histories</h3>
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Status</th>
                                                <th>Amount</th>
                                                <th>Remark</th>
                                                <th>TIme</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                             @foreach ($detail->parent->history as $history)
                                                <tr>
                                                    <td><span class="{{ $classBadge[$history->status] }}">{{ $history->status}}</span></td>
                                                    <td>Rp{{ $helper->formatNumber($history->amount) }}</td>
                                                    <td>{{ $history->remarks}}</td>
                                                    <td>{{ $helper->formatDate($history->created_at) }}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                        </div>
                    </div>
                </div>
                <!-- END DETAIL -->
               
                <div class="tab-pane" id="smslog" role="tabpanel">
                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <th>Client</th>
                                    <td>{{ $detail->parent->client_name }}</td>
                                </tr>
                                <tr>
                                    <th>Customer Email</th>
                                    <td>{{ $detail->parent->customer_email }}</td>
                                </tr>
                                <tr>
                                    <th>Customer Name</th>
                                    <td>{{ $detail->parent->customer_name }}</td>
                                </tr>
                                <tr>
                                    <th>Customer Phone</th>
                                    <td>{{ $detail->parent->customer_phone }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>

<div class="modal" id="detailModal">
    <div class="modal-dialog">
        <div class="modal-content">
    
        <!-- Modal Header -->
        <div class="modal-header">
            <h4 class="modal-title">Repush Transaksi ini?</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
    
        <!-- Modal body -->
            <div class="modal-body">
                    <h3>{{ $detail->parent->payment_id }}</h3>
                    <h2 style="display:none;" class="loading">Mohon Tunggu...</h2>
            </div>
            <div class="modal-footer">
                    <button class="btn btn-primary float-right" id='repush'>Ya</button>
                    <button class="btn btn-warning float-right" data-dismiss="modal">Tidak</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    $(document).ready(function(){
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                }
            });
    })
    $('#repush').on('click',function(){
        $('.loading').show()
        $.ajax({
            type: "POST",
            url: "{{ url('report/payment/repush') }}",
            data: {
                id: '{{ $detail->parent->payment_id}}'
            },
            success: function(msg){
                $('.loading').hide()
                $('#detailModal').modal('toggle');
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                $('.loading').hide()
                alert(XMLHttpRequest.responseText)
            }
        });
    })

    function copyToClipboard(element) {
        var $temp = $("<input>");
        $("body").append($temp);
        $temp.val($(element).text()).select();
        document.execCommand("copy");
        $temp.remove();
    }

    $('a[data-toggle="tooltip"]').tooltip({
        animated: 'fade',
        placement: 'bottom',
        trigger: 'click'
    });
</script>
@endsection