@extends('layout.main')

@section('title')
	Channel Detail
@endsection

@section('css')
<link href="{{ asset('assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="material-card card">
            <div class="card-body bg-primary text-white">
            <h2 class="card-title" style="font-size: 24px;"></h2>
                <h6>Ubah Payment Channel</h6>
            <p></p>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                  <form action="{{ url('report/payment/channel/'.$detail->id) }}" method="POST" enctype="multipart/form-data">
                        <div class="form-group">
                          @csrf
                          <label for="name">Payment Vendor</label>
                        <input type="text" name="vendor" class="form-control" id="" value="{{ $detail->vendor->name }}">
                        </div>
                        <div class="form-group">
                          <label for="name">Code</label>
                        <input type="text" class="form-control" id="" name="code" value="{{$detail->code}}">
                        </div>
                        <div class="form-group">
                          <label for="name">Type</label>
                        <input type="text" class="form-control" name="type" value="{{ $detail->type }}">
                        </div>
                        <div class="form-group">
                          <label for="name">Name</label>
                        <input type="text" class="form-control" name="name" id="" value="{{ $detail->name }}">
                        </div>
                        <div class="form-group">
                          <label for="name">Status</label>
                          <select name="status" class="form-control">
                          <option value="OPEN" {{ ($detail->status === 'OPEN') ? 'selected': '' }}>OPEN</option>
                          <option value="CLOSE" {{ ($detail->status === 'CLOSE') ? 'selected': '' }}>CLOSE</option>
                          </select>
                        </div>
                        <div class="form-group">
                          <label for="name">Text</label>
                          <input type="text" class="form-control" name="text" id="" value="{{ $detail->text }}">
                        </div>
                        <div class="form-group">
                          <label for="name">Description</label>
                        <input type="text" class="form-control" name="description" id="" value="{{ $detail->description}}">
                        </div>
                        <div class="form-group">
                            <label for="name">File</label>
                            @if (isset($detail->image_url))
                              <img src="{{ env('PAYMENT_URL') }}/{{ $detail->image_url }}" class="img-responsive">
                            @endif
                            <input type="file" class="form-control" name="file" id="">
                         </div>
                        <button type="submit" class="btn btn-primary float-right">Submit</button>
                    </form> 
            </div>
        </div>
    </div>
</div>
@endsection