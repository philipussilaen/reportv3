@extends('layout.main')
{{-- Do Not Edit this file. This is Base File for View --}}

@section('title')
	Locker Delivery Activities
@endsection

@section('css')
<link href="{{ asset('assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css') }}" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/libs/daterangepicker/daterangepicker.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/libs/select2/dist/css/select2.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/pace-theme.css') }}">
<script src="{{ asset('js/pace.min.js') }}"></script>
<style>
#popTable_processing.card {
    background-color: #dddddf;
    color: #000000;
    opacity: 0.8;
}
</style>
@endsection

@section('breadcrumb')
{{-- expr --}}
@endsection

@section('content')
<!-- <div class="row">
    <div class="col-sm-4 col-md-4">
    <a id="overdue" class="filter" href="javascript:void(0)">
        <div class="card bg-danger">
            <div class="card-body text-white">
                <div class="d-flex no-block align-items-center">
                    <div>
                        <h2 id="counterOverdue" class="count font-medium mb-0">0</h2>
                        <hr>
                        <h5 data-toggle="tooltip" data-placement="bottom" title="" data-original-title="parcel in the locker and need to extend">Overdue</h5>
                    </div>
                    <div class="ml-auto">
                        <span class="text-white display-6"><i class="icon-clock"></i></span>
                    </div>
                </div>
            </div>
        </div>
    </a>
    </div>
    <div class="col-sm-4 col-md-4">
    <a id="complete" class="filter" href="javascript:void(0)">
        <div class="card bg-info">
            <div class="card-body text-white">
                <div class="d-flex no-block align-items-center">
                    <div>
                        <h2 id="counterInStore" class="count font-medium mb-0">0</h2>
                        <hr>
                        <h5 data-toggle="tooltip" data-placement="bottom" title="" data-original-title="parcel in the locker">In Store</h5>
                    </div>
                    <div class="ml-auto">
                        <span class="text-white display-6"><i class="icon-check"></i></span>
                    </div>
                </div>
            </div>
        </div>
    </a>
    </div>
    <div class="col-sm-4 col-md-4">
    <a id="in-store" class="filter" href="javascript:void(0)">
        <div class="card bg-success">
            <div class="card-body text-white">
                <div class="d-flex no-block align-items-center">
                    <div>
                        <h2 id="counterComplete" class="count font-medium mb-0">0</h2>
                        <hr>
                        <h5 data-toggle="tooltip" data-placement="bottom" title="" data-original-title="parcel has been taken by user">Customer Taken</h5>
                    </div>
                    <div class="ml-auto">
                        <span class="text-white display-6"><i class="icon-basket-loaded"></i></span>
                    </div>
                </div>
            </div>
        </div>
    </a>
    </div>
</div> -->
<div class="row">
    <div class="col-12">
        <div class="material-card card">
            <form id="form-filter" class="form-horizontal">
                <input type="hidden" id="isAnomaly" name="isAnomaly" value="0">
                <input type="hidden" name="dateBefore" value="1420070400000">
                <input type="hidden" name="dateAfter" value="<?php echo time()*1000; ?>">
                <div class="card-body">
                    <h4 class="card-title">Filter <a id="linkFilter" data-toggle="collapse" href="#collapseFilter" role="button" aria-expanded="false" aria-controls="collapseFilter" style="float:right;"> <button id="btnCollapse" class="btn btn-secondary"> SHOW FILTER <i class="fas fa-chevron-right"></i> </button> </a></h4>
                    <div class="row button-group mb-4">
                        <div class="col-12">
                            <button id="btnDate" type="button" class="btn btn-outline-secondary filter" style="display:none"></button>
                            <button id="btnExpressNumber" type="button" class="btn btn-outline-secondary filter" style="display:none"></button>
                            <button id="btnParcelId" type="button" class="btn btn-outline-secondary filter" style="display:none"></button>
                            <button id="btnExpressType" type="button" class="btn btn-outline-secondary filter" style="display:none"></button>
                            <button id="btnCountry" type="button" class="btn btn-outline-secondary filter" style="display:none"></button>
                            <button id="btnStatus" type="button" class="btn btn-outline-secondary filter" style="display:none"></button>
                            <button id="btnLocation" type="button" class="btn btn-outline-secondary filter" style="display:none"></button>
                            <button id="btnPhone" type="button" class="btn btn-outline-secondary filter" style="display:none"></button>
                            <button id="btnCode" type="button" class="btn btn-outline-secondary filter" style="display:none"></button>
                            <button id="btnCompany" type="button" class="btn btn-outline-secondary filter" style="display:none"></button>
                            <button id="btnCourier" type="button" class="btn btn-outline-secondary filter" style="display:none"></button>
                            <button id="btnImportTime" type="button" class="btn btn-outline-secondary filter" style="display:none"></button>
                            <button id="btnTakeTime" type="button" class="btn btn-outline-secondary filter" style="display:none"></button>
                            <button id="btnOverdueTime" type="button" class="btn btn-outline-secondary filter" style="display:none"></button>
                        </div>
                    </div>
                    <div class="collapse" id="collapseFilter">
                    <hr>
                    <div class="row">
                        <div class="col-sm-12 col-lg-4">
                            <div class="form-group">
                                <label for="expressNumber" class="text-right control-label col-form-label">Parcel Number</label>
                                <input type="text" class="form-control" id="expressNumber" name="expressNumber" placeholder="Parcel Number">
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-4">
                            <div class="form-group">
                                <label for="parcelId" class="text-right control-label col-form-label">Parcel ID</label>
                                <input type="text" class="form-control" id="parcelId" name="parcelId" placeholder="Parcel ID">
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-4">
                            <div class="form-group">
                                <label for="expressType" class="text-right control-label col-form-label">Type</label>
                                <select id="expressType" class="form-control select2" name="expressType" style="width: 100%; height:36px;">
                                    <option value="" readonly>All Type</option>
                                    @foreach ($types as $key => $type)
                                    <option value="{{ $key }}">{{ $type }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-4">
                            <div class="form-group">
                                <label for="country" class="text-right control-label col-form-label">Country</label>
                                <select id="country" class="form-control select2" name="country" style="width: 100%; height:36px;">
                                    <option value="" readonly>All Country</option>
                                    @foreach ($countries as $key => $country)
                                    <option value="{{ $key }}">{{  $country }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-4">
                            <div class="form-group">
                                <label for="status" class="text-right control-label col-form-label">Status</label>
                                <select id="status" class="form-control select2" name="status" style="width: 100%; height:36px;">
                                    <option value="" readonly>All Status</option>
                                    @foreach ($status as $key => $stat)
                                    <option value="{{ $key }}">{{ $stat }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-4">
                            <div class="form-group">
                                <label for="location" class="text-right control-label col-form-label">Location</label>
                                <select id="location" class="form-control select2" name="location" style="width: 100%; height:36px;">
                                    <option value="" readonly>All Location</option>
                                    @foreach ($location as $key => $loc)
                                        <option value="{{ $loc }}">{{ $key }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-4">
                            <div class="form-group">
                                <label for="phone" class="text-right control-label col-form-label">Phone</label>
                                <input type="text" class="form-control" id="phone" name="phone" placeholder="08xxxxxxxxxx">
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-4">
                            <div class="form-group">
                                <label for="company" class="text-right control-label col-form-label">Company Name</label>
                                <select id="company" class="form-control select2" name="company" style="width: 100%; height:36px;">
                                    <option value="" readonly>All Companies</option>
                                    @foreach ($companies as $key => $company)
                                    <option value="{{ $company }}">{{ $company }}</option>
                                    @endforeach
                                    @if($id_company == '161e5ed1140f11e5bdbd0242ac110001' || $id_company == NULL)
                                    <option value="GRAB">GRAB</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-4">
                            <div class="form-group">
                                <label for="courier" class="text-right control-label col-form-label">Courier Name</label>
                                <input type="text" class="form-control" id="courier" name="courier" placeholder="Courier Name">
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-4">
                            <div class="form-group">
                                <label for="pin" class="text-right control-label col-form-label">PIN</label>
                                <input type="text" class="form-control" id="pin" name="pin" placeholder="PIN">
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-4">
                            <div class="form-group">
                                <label for="storeTime" class="text-right control-label col-form-label">Store Time</label>
                                <input type="text" class="form-control daterange" id="storeTime" name="storeTime" placeholder="Store Time">
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-4">
                            <div class="form-group">
                                <label for="importTime" class="text-right control-label col-form-label">Import Time</label>
                                <input type="text" class="form-control daterange" id="importTime" name="importTime" placeholder="Import Time">
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-4">
                            <div class="form-group">
                                <label for="takeTime" class="text-right control-label col-form-label">Take Time</label>
                                <input type="text" class="form-control daterange" id="takeTime" name="takeTime" placeholder="Take Time">
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-4">
                            <div class="form-group">
                                <label for="overdueTime" class="text-right control-label col-form-label">Overdue Time</label>
                                <input type="text" class="form-control daterange" id="overdueTime" name="overdueTime" placeholder="Overdue Time">
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class=row>
                        <div class="col-sm-6 mb-0 text-left">
                            <button type="button" id="showAnomaly" class="btn btn-primary waves-effect waves-light" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Backdate & Anomaly Data">Show Anomaly</button>
                        </div>
                        <div class="col-sm-6 mb-0 text-right">
                            <button type="button" id="btnFilter" class="btn btn-info waves-effect waves-light">Filter</button>
                            <button type="button" id="btnReset" class="btn btn-dark waves-effect waves-light">Reset</button>
                        </div>
                    </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="material-card card">
            <div class="card-body row">
                <div class="col-sm-12 col-md-6">
                    <h4 class="card-title">Locker Delivery Activities</h4>
                    <h6 class="card-subtitle">List all delivery activities on locker</h6>
                </div>
                <div class="col-sm-12 col-md-6">
                    <button id="popExcel" class="btn btn-primary mb-3" style="float:right;">Download</button>
                </div>
                
                <div class="table-responsive">
                    <table id="popTable" class="table table-striped border text-inputs-searching" style="width:100%">
                        <thead>
                            <tr>
                                <th>Parcel Number</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Locker</th>
                                <th>Phone</th>
                                <th>Courier</th>
                                <th>Status</th>
                                <th>Import Time</th>
                                <th>Store Time</th>
                                <th>Take Time</th>
                                <th>Overdue Time</th>
                                <th>Pin</th>
                                <th>Parcel ID</th>
                                <th>Country</th>
                                <th>Courier Name</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Parcel Number</th>
                                <th>Type</th>
                                <th>Location</th>
                                <th>Locker</th>
                                <th>Phone</th>
                                <th>Courier</th>
                                <th>Status</th>
                                <th>Import Time</th>
                                <th>Store Time</th>
                                <th>Take Time</th>
                                <th>Overdue Time</th>
                                <th>Pin</th>
                                <th>Parcel ID</th>
                                <th>Country</th>
                                <th>Courier Name</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('js')
<script src="{{ asset('assets/libs/moment/moment.js') }}"></script>
<script src="{{ asset('assets/extra-libs/DataTables/datatables.min.js') }}"></script>
<script src="{{ asset('assets/libs/daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('assets/libs/select2/dist/js/select2.min.js') }}"></script>
<script src="{{ asset('js/common.js') }}"></script>
<script>
$(document).ready(function() {
$('.select2').select2();
$( "#importTime" ).val('');
$( "#takeTime" ).val('');
$( "#overdueTime" ).val('');
// pace.start();
var storeTime = $('#storeTime').val();
if(storeTime != '') { $('#btnDate').show(); $('#btnDate').text('Date : ' + storeTime); }

    getData({
        "_token" : "{{ csrf_token() }}",
        "date": $('#storeTime').val()
    })
});

function getData(param = {}) {

return $('#popTable').DataTable({
    processing: true,
    serverSide: true,
    order: [['8', 'desc']],
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
    language: {
        processing: '<h4 class="font-bold" style="text-align:center;">Loading data to show</h4><br><div class="spinner-border text-danger" role="status"><span class="sr-only">Loading...</span></div>'
    },
    ajax: {
        url: "{{ url('locker/delivery/getAjaxDeliveryActivity') }}",
        dataType: "json",
        type: "POST",
        data: param
    },
    columns: [
        { "data": "expressNumber",
          "name": "expressNumber",
          "render": function ( data, type, full, meta ) {
            if(full.expressType == 'COURIER_STORE') {
                return '<a href="{{ url("locker/delivery/detail") }}'+'/'+full.parcelId+'">'+data+'</a>';
            } else {
                return '<a href="{{ url("locker/delivery/detail") }}'+'/'+full.parcelId+'">'+full.customerStoreNumber+'</a>';
            }
          }
        },
        { "data": "expressType",
          "name": "expressType",
          "render": function ( data, type, full, meta ) {
            if(data == 'COURIER_STORE') {
                label = '<span class="badge badge-info ml-1 text-white font-medium">LAST MILE</span>';
            } else if(data == 'CUSTOMER_REJECT') {
                label = '<span class="badge badge-dark ml-1 text-white font-medium">RETURN</span>';
            } else if(data == 'CUSTOMER_STORE') {
                label = '<span class="badge badge-danger ml-1 text-white font-medium">POPSEND</span>';
            }
            return label
          }
        },
        { "data": "locker_name",
          "name": "locker_name",
          "render": function ( data, type, full, meta ) {
            return data;
          }
        },
        { "data": "locker_size",
          "name": "locker_size",
          "render": function ( data, type, full, meta ) {
            var label;
            if(data != null) {
                label = data+' / '+full.locker_number;
            } else {
                label = '';
            }
            return label;
          }
        },
        { "data": "phone",
          "name": "phone",
          "render": function ( data, type, full, meta ) {
            // if(full.expressType == 'COURIER_STORE') {
            if(data != null) {
                return data;
            } else {
                return full.storePhone;
            }
          }
        },
        { "data": "company_name",
          "name": "company_name",
          "render": function ( data, type, full, meta ) {
            return data;
          }
        },
        { "data": "status",
          "name": "status",
          "render": function ( data, type, full, meta ) {
            if(data == 'CUSTOMER_TAKEN') {
                return '<span class="badge badge-success ml-1 text-white font-medium">'+data+'</span>';
            } else if(data == 'COURIER_TAKEN') {
                return '<span class="badge badge-warning ml-1 font-medium">'+data+'</span>';
            } else if(data == 'OPERATOR_TAKEN') {
                return '<span class="badge badge-warning ml-1 font-medium">'+data+'</span>';
            } else if(data == 'IN_STORE') {
                if(full.nowDateTime > full.overdueTime && full.expressType == 'COURIER_STORE') {
                    return '<span class="badge badge-danger ml-1 text-white font-medium">OVERDUE</span>';
                } else {
                    return '<span class="badge badge-info ml-1 text-white font-medium">'+data+'</span>';
                }
            } else {
                return '<span class="badge badge-dark ml-1 text-white font-medium">'+data+'</span>';
            }
          }
        },
        { "data": "importTime",
          "name": "importTime",
          "render": function ( data, type, full, meta ) {
            if(data == null) {
                return '';
            } else {
                return formatEpochDate(data);
            }
          }
        },
        { "data": "storeTime",
          "name": "storeTime",
          "render": function ( data, type, full, meta ) {
            if(data == null) {
                return '';
            } else {
                return formatEpochDate(data);
            }
          }
        },
        { "data": "takeTime",
          "name": "takeTime",
          "render": function ( data, type, full, meta ) {
            if(data == null) {
                return '';
            } else {
                return formatEpochDate(data);
            }
          }
        },
        { "data": "overdueTime",
          "name": "overdueTime",
          "render": function ( data, type, full, meta ) {
            if(data == null) {
                return '';
            } else {
                return formatEpochDate(data);
            }
          }
        },
        { "data": "validateCode",
          "name": "validateCode",
          "visible": false,
          "render": function ( data, type, full, meta ) {
            return data;
          }
        },
        { "data": "parcelId",
          "name": "parcelId",
          "visible": false,
          "render": function ( data, type, full, meta ) {
            return data;
          }
        },
        { "data": "country",
          "name": "country",
          "visible": false,
          "render": function ( data, type, full, meta ) {
            return data;
          }
        },
        { "data": "courier",
          "name": "courier",
          "visible": false,
          "render": function ( data, type, full, meta ) {
            return data;
          }
        }
    ]
})
}

$('#btnFilter').click(function(e) {
    e.preventDefault();
    $("#isAnomaly").val('0');
    var expressNumber = $('#expressNumber').val();
    var expressType = $('#expressType').select2('data');
    var expressTypeId = expressType[0].id;
    var expressTypeText = expressType[0].text;
    var status = $('#status').select2('data')
    var statusId = status[0].id
    var statusText = status[0].text
    var location = $('#location').select2('data')
    var locationId = location[0].id
    var locationText = location[0].text
    var phone = $('#phone').val();
    
    var storeTime = $('#storeTime').val(); // 02/01/2019 - 02/25/2019
    var importTime = $('#importTime').val();
    var takeTime = $('#takeTime').val();
    var overdueTime = $('#overdueTime').val();
    var pin = $('#pin').val();
    var parcelId = $('#parcelId').val();
    var country = $('#country').select2('data')
    var countryId = country[0].id
    var countryText = country[0].text
    var courier = $('#courier').val();
    var company = $('#company').val();

    // hide all filter
    $('#btnExpressNumber').hide();
    $('#btnParcelId').hide();
    $('#btnExpressType').hide();
    $('#btnCountry').hide();
    $('#btnStatus').hide();
    $('#btnLocation').hide();
    $('#btnPhone').hide();
    $('#btnLocation').hide();
    $('#btnCode').hide();
    $('#btnCompany').hide();
    $('#btnCourier').hide();
    $('#btnImportTime').hide();
    $('#btnTakeTime').hide();
    $('#btnOverdueTime').hide();

    // show the active filter
    if(expressNumber != '') { $('#btnExpressNumber').show(); $('#btnExpressNumber').text('Parcel Number : ' + expressNumber); }
    if(parcelId != '') { $('#btnParcelId').show(); $('#btnParcelId').text('Parcel ID : ' + parcelId); }
    if(expressTypeId != '') { $('#btnExpressType').show(); $('#btnExpressType').text('Type : ' + expressTypeText); }
    if(countryId != '') { $('#btnCountry').show(); $('#btnCountry').text('Country : ' + countryText); }
    if(statusId != '') { $('#btnStatus').show(); $('#btnStatus').text('Status : ' + statusText); }
    if(locationId != '') { $('#btnLocation').show(); $('#btnLocation').text('Location : ' + locationText); }
    if(phone != '') { $('#btnPhone').show(); $('#btnPhone').text('Phone : ' + phone); }
    if(storeTime != '') { $('#btnDate').show(); $('#btnDate').text('Date : ' + storeTime); }
    if(importTime != '') { $('#btnImportTime').show(); $('#btnImportTime').text('Import Date : ' + importTime); }
    if(takeTime != '') { $('#btnTakeTime').show(); $('#btnTakeTime').text('Take Date : ' + takeTime); }
    if(overdueTime != '') { $('#btnOverdueTime').show(); $('#btnOverdueTime').text('Overdue Date : ' + overdueTime); }
    if(pin != '') { $('#btnCode').show(); $('#btnCode').text('PIN : ' + pin); }
    if(courier != '') { $('#btnCourier').show(); $('#btnCourier').text('Courier : ' + courier); }
    if(company != '') { $('#btnCompany').show(); $('#btnCompany').text('Company : ' + company); }

    $('#popTable').DataTable().clear().destroy();
    getData({
        "_token" : "{{ csrf_token() }}",
        "expressNumber": expressNumber,
        "expressType": expressTypeId,
        "location": locationId,
        "phone": phone,
        "status": statusId,
        "date": storeTime,
        "pin": pin,
        "parcelId": parcelId,
        "country": countryId,
        "courier": courier,
        "company": company,
        "storeTime": storeTime,
        "importTime": importTime,
        "takeTime": takeTime,
        "overdueTime": overdueTime,
    })
})

$('#btnReset').click(function() {
    var expressNumber = $('#expressNumber').val('');
    var expressType = $('#expressType').val('').trigger('change');
    var status = $('#status').val('').trigger('change');
    var location = $('#location').val('').trigger('change');
    var phone = $('#phone').val('');
    var storeTime = $('#storeTime').val();
    var pin = $('#pin').val('');
    var parcelId = $('#parcelId').val('');
    var country = $('#country').val('').trigger('change');
    var courier = $('#courier').val('');
    var company = $('#company').val('').trigger('change');
    var importTime = $('#importTime').val('');
    var takeTime = $('#takeTime').val('');
    var overdueTime = $('#overdueTime').val('');

    // hide all filter
    $('#btnExpressNumber').hide();
    $('#btnParcelId').hide();
    $('#btnExpressType').hide();
    $('#btnCountry').hide();
    $('#btnStatus').hide();
    $('#btnLocation').hide();
    $('#btnPhone').hide();
    $('#btnLocation').hide();
    $('#btnCode').hide();
    $('#btnCompany').hide();
    $('#btnCourier').hide();
    $('#btnImportTime').hide();
    $('#btnTakeTime').hide();
    $('#btnOverdueTime').hide();

    $('#popTable').DataTable().clear().destroy();
    getData({
        "_token" : "{{ csrf_token() }}",
        "date": storeTime
    })
});

$('#popExcel').click(function () {
    var search = $('#form-filter').serialize();
    window.location.href = "{{ url('locker/delivery/getFileExcel/') }}"+"?"+search;
});

function appendLeadingZeroes(n){
  if(n <= 9){
    return "0" + n;
  }
  return n
}

$('#showAnomaly').click(function() {
    $("#isAnomaly").val('1');
    var dateBefore = '1420070400000'; //'2015-01-01 00:00:00';
    var dateAfter = Date.now();
    var expressNumber = $('#expressNumber').val();
    var expressType = $('#expressType').select2('data');
    var expressTypeId = expressType[0].id;
    var expressTypeText = expressType[0].text;
    var status = $('#status').select2('data')
    var statusId = status[0].id
    var statusText = status[0].text
    var location = $('#location').select2('data')
    var locationId = location[0].id
    var locationText = location[0].text
    var phone = $('#phone').val();
    var pin = $('#pin').val();
    var parcelId = $('#parcelId').val();
    var country = $('#country').select2('data')
    var countryId = country[0].id
    var countryText = country[0].text
    var courier = $('#courier').val();
    var company = $('#company').val();
    
    $('.filter').hide();

    if(expressNumber != '') { $('#btnExpressNumber').show(); $('#btnExpressNumber').text('Parcel Number : ' + expressNumber); }
    if(parcelId != '') { $('#btnParcelId').show(); $('#btnParcelId').text('Parcel ID : ' + parcelId); }
    if(expressTypeId != '') { $('#btnExpressType').show(); $('#btnExpressType').text('Type : ' + expressTypeText); }
    if(countryId != '') { $('#btnCountry').show(); $('#btnCountry').text('Country : ' + countryText); }
    if(statusId != '') { $('#btnStatus').show(); $('#btnStatus').text('Status : ' + statusText); }
    if(locationId != '') { $('#btnLocation').show(); $('#btnLocation').text('Location : ' + locationText); }
    if(phone != '') { $('#btnPhone').show(); $('#btnPhone').text('Phone : ' + phone); }
    if(pin != '') { $('#btnCode').show(); $('#btnCode').text('PIN : ' + pin); }
    if(courier != '') { $('#btnCourier').show(); $('#btnCourier').text('Courier : ' + courier); }
    if(company != '') { $('#btnCompany').show(); $('#btnCompany').text('Company : ' + company); }

    $('#popTable').DataTable().clear().destroy();
    getData({
        "_token" : "{{ csrf_token() }}",
        "expressNumber": expressNumber,
        "expressType": expressTypeId,
        "location": locationId,
        "phone": phone,
        "status": statusId,
        "pin": pin,
        "parcelId": parcelId,
        "country": countryId,
        "courier": courier,
        "company": company,
        "dateBefore": dateBefore,
        "dateAfter": dateAfter
    })
})
</script>
@endsection