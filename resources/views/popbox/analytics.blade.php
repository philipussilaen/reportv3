@extends('layout.main')
{{-- Do Not Edit this file. This is Base File for View --}}

@section('title')
	Parcel Analytics
@endsection

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset('assets/libs/daterangepicker/daterangepicker.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/libs/select2/dist/css/select2.min.css') }}">
<link href="{{ asset('css/epic-spinners.css') }}" rel="stylesheet">
<script src="{{ asset('assets/libs/highcharts/code/highcharts.js') }}"></script>
<script src="{{ asset('assets/libs/highcharts/code/modules/exporting.js') }}"></script>
<script src="{{ asset('assets/libs/highcharts/code/modules/export-data.js') }}"></script>
<style>
.highcharts-credits {
	display: none;
}
</style>
@endsection

@section('breadcrumb')

@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="material-card card">
            <form id="form-filter" class="form-horizontal">
                <div class="card-body">
                    <h4 class="card-title">Filter <a id="linkFilter" data-toggle="collapse" href="#collapseFilter" role="button" aria-expanded="false" aria-controls="collapseFilter" style="float:right;"> <button id="btnCollapse" class="btn btn-secondary"> SHOW FILTER <i class="fas fa-chevron-right"></i> </button> </a></h4>
                    <div class="row button-group mb-4">
                        <div class="col-12">
                            <button id="btnLocker" type="button" class="btn btn-outline-secondary filter" style="display:none"></button>
                            <button id="btnCountry" type="button" class="btn btn-outline-secondary filter" style="display:none"></button>
                            <button id="btnYear" type="button" class="btn btn-outline-secondary filter" style="display:none"></button>
                            <button id="btnDate" type="button" class="btn btn-outline-secondary filter" style="display:none"></button>
                        </div>
                    </div>
                    <div class="collapse" id="collapseFilter">
                    <hr>
                    <div class="row">
                        <div class="col-sm-12 col-lg-4">
                            <div class="form-group">
                                <label for="lockerName" class="text-right control-label col-form-label">Locker Name</label>
                                <select id="lockerName" class="form-control select2" name="lockerName" style="width: 100%; height:36px;">
                                    <option value="" readonly>All Locker</option>
                                    @foreach ($lockerNames as $key => $row)
                                    <option value="{{ $key }}">{{ $row }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <!-- <div class="col-sm-12 col-lg-4">
                            <div class="form-group">
								<label for="type" class="text-right control-label col-form-label">Building Type</label>
								<select id="type" class="form-control select2" name="type" style="width: 100%; height:36px;">
                                    <option value="" readonly>All Type</option>
                                    @foreach ($buildingTypes as $key => $type)
                                    <option value="{{ $key }}">{{ $type }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div> -->
                        <div class="col-sm-12 col-lg-4">
                            <div class="form-group">
								<label for="country" class="text-right control-label col-form-label">Country</label>
								<select id="country" class="form-control select2" name="country" style="width: 100%; height:36px;">
                                    <option value="" readonly>All Country</option>
                                    @foreach ($countries as $key => $row)
                                    <option value="{{ $row }}">{{ $row }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-4">
                            <div class="form-group">
								<label for="year" class="text-right control-label col-form-label">Year</label>
								<select id="year" class="form-control select2" name="year" style="width: 100%; height:36px;">
                                    <option value="" readonly>All Year</option>
                                    @foreach ($year as $row)
                                    <option value="{{ $row }}">{{ $row }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-4">
                            <div class="form-group">
                                <label for="date" class="text-right control-label col-form-label">Range Date</label>
                                <input type="text" class="form-control daterange" id="date" name="date" placeholder="Range Date">
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group mb-0 text-right">
                        <button type="button" id="btnFilter" class="btn btn-info waves-effect waves-light">Filter</button>
                        <button type="button" id="btnReset" class="btn btn-dark waves-effect waves-light">Reset</button>
                    </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>



<div class="row">
    <div class="col-md-12 col-lg-12">
        <div class="card">
<!-- Nav tabs -->
<ul class="nav nav-tabs customtab justify-content-end" role="tablist">
    <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#usage-monthly" role="tab"><span class="hidden-xs-down">Monthly</span></a> </li>
    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#usage-weekly" role="tab"><span class="hidden-xs-down">Weekly</span></a> </li>
    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#usage-daily" role="tab"><span class="hidden-xs-down">Daily</span></a> </li>
</ul>
<!-- Tab panes -->
<div class="tab-content">
    <div class="tab-pane active p-4" id="usage-monthly" role="tabpanel">
        <div class="loading-image" style="text-align: center; display: none;">
            <h4 class="font-bold">Loading Graph</h4>
                    <div class="atom-spinner" style="margin: 0 auto;">
                        <div class="spinner-inner">
                            <div class="spinner-line"></div>
                            <div class="spinner-line"></div>
                            <div class="spinner-line"></div>
                            <div class="spinner-circle">
                            &#9679;
                            </div>
                        </div>
                    </div>
        </div>
		<div id="all-usage-chart"></div>
    </div>
    <div class="tab-pane p-4" id="usage-weekly" role="tabpanel">
        <div class="loading-image" style="text-align: center; display: none;">
            <h4 class="font-bold">Loading Graph</h4>
                    <div class="atom-spinner" style="margin: 0 auto;">
                        <div class="spinner-inner">
                            <div class="spinner-line"></div>
                            <div class="spinner-line"></div>
                            <div class="spinner-line"></div>
                            <div class="spinner-circle">
                            &#9679;
                            </div>
                        </div>
                    </div>
        </div>
		<div id="all-usage-chart-weekly"></div>
    </div>
    <div class="tab-pane p-4" id="usage-daily" role="tabpanel">
        <div class="loading-image" style="text-align: center; display: none;">
            <h4 class="font-bold">Loading Graph</h4>
                    <div class="atom-spinner" style="margin: 0 auto;">
                        <div class="spinner-inner">
                            <div class="spinner-line"></div>
                            <div class="spinner-line"></div>
                            <div class="spinner-line"></div>
                            <div class="spinner-circle">
                            &#9679;
                            </div>
                        </div>
                    </div>
        </div>
		<div id="all-usage-chart-daily"></div>
    </div>
</div>
</div></div></div>



<div class="row">
	<div class="col-md-12 col-lg-6">
    	<div class="card">
        	<div class="card-body">
                <div class="loading-image" style="text-align: center; display: none;">
                    <h4 class="font-bold">Loading Graph</h4>
                    <div class="atom-spinner" style="margin: 0 auto;">
                        <div class="spinner-inner">
                            <div class="spinner-line"></div>
                            <div class="spinner-line"></div>
                            <div class="spinner-line"></div>
                            <div class="spinner-circle">
                            &#9679;
                            </div>
                        </div>
                    </div>
                </div>
            	<div id="top-location-chart">
                </div>
        	</div>
    	</div>
	</div>
	<div class="col-md-12 col-lg-6">
    	<div class="card">
        	<div class="card-body">
                <div class="loading-image" style="text-align: center; display: none;">
                    <h4 class="font-bold">Loading Graph</h4>
                    <div class="atom-spinner" style="margin: 0 auto;">
                        <div class="spinner-inner">
                            <div class="spinner-line"></div>
                            <div class="spinner-line"></div>
                            <div class="spinner-line"></div>
                            <div class="spinner-circle">
                            &#9679;
                            </div>
                        </div>
                    </div>
                </div>
            	<div id="bottom-location-chart">
                </div>
        	</div>
    	</div>
	</div>
	<!-- <div class="col-md-12 col-lg-6">
    	<div class="card">
        	<div class="card-body">
            	<div id="categories-chart">
                    <div class="loading-image" style="text-align: center; display: none;">
                        <h4 class="font-bold">Loading Graph</h4>
                        <div class="atom-spinner" style="margin: 0 auto;">
                        <div class="spinner-inner">
                            <div class="spinner-line"></div>
                            <div class="spinner-line"></div>
                            <div class="spinner-line"></div>
                            <div class="spinner-circle">
                            &#9679;
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
        	</div>
    	</div>
	</div> -->
    <div class="col-md-12 col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="loading-image" style="text-align: center; display: none;">
                    <h4 class="font-bold">Loading Graph</h4>
                    <div class="atom-spinner" style="margin: 0 auto;">
                        <div class="spinner-inner">
                            <div class="spinner-line"></div>
                            <div class="spinner-line"></div>
                            <div class="spinner-line"></div>
                            <div class="spinner-circle">
                            &#9679;
                            </div>
                        </div>
                    </div>
                </div>
				<div id="unique-user-chart">
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="{{ asset('assets/libs/raphael/raphael.min.js') }}"></script>
<script src="{{ asset('assets/libs/morris.js/morris.min.js') }}"></script>
<script src="{{ asset('assets/libs/moment/moment.js') }}"></script>
<script src="{{ asset('assets/libs/daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('assets/libs/select2/dist/js/select2.min.js') }}"></script>
<script src="{{ asset('js/common.js') }}"></script>
<script>
$(document).ready(function() {
    // categories()
    $('.select2').select2();
    var year = '2019';
    $('#year').val(year).trigger('change')
    $('#date').val(year + '-01-01 - ' + year + '-12-31')
    if(year != '') { $('#btnYear').show(); $('#btnYear').text('Year : ' + year); }
    getData({
        "_token" : "{{ csrf_token() }}",
        "year" : year
    });
});

$('#year').change(function() {
    var year = $('#year').val()
    if(year) {
        $('#date').val(year + '-01-01 - ' + year + '-12-31')
    }
})

function getData(params) {
$.ajax({
    url: "{{ url('getAjaxParcelAnalytics') }}",
    type: "POST",
    data: params,
    beforeSend: function() {
        $(".loading-image").show();
    },
    success: function(result){
        var res = JSON.parse(result)
        allUsage(res.category, res.allUsageData, res.categoryPercentage, res.categoryGrowth);
        allUsageWeekly(res.categoryWeekly, res.categoryDataWeekly, res.categoryPercentageWeekly, res.categoryGrowthWeekly);
        allUsageDaily(res.categoryDaily, res.categoryDataDaily, res.categoryPercentageDaily, res.categoryGrowthDaily);
        topLocation(res.topLocation, res.parcelByLocation);
        bottomLocation(res.bottomLocation, res.lowestParcelByLocation);
        uniqueUser(res.category, res.userData, res.userPercent);
        $(".loading-image").hide();
    }
});
}

function allUsageDaily(cat, data, percentage, growth) {
Highcharts.chart('all-usage-chart-daily', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'Locker Activity by Parcel Usage'
  },
  xAxis: {
    categories: cat,
    crosshair: true
  },
  yAxis: [
  { // Secondary yAxis
    title: {
      text: 'Percentage',
      style: {
        color: Highcharts.getOptions().colors[0]
      }
    },
    labels: {
      format: '{value} %',
      style: {
        color: Highcharts.getOptions().colors[0]
      }
    },
    opposite: true
  },
  { // Primary yAxis
    labels: {
      format: '{value}',
      style: {
        color: Highcharts.getOptions().colors[1]
      }
    },
    title: {
      text: 'Number of Parcels',
      style: {
        color: Highcharts.getOptions().colors[1]
      }
    }
  }],
  tooltip: {
    formatter: function () {
      return this.point.series.name + ':<b>' + this.y + '</b><br>Growth: <b>' + this.point.growth + '</b>';
    }
  },
  plotOptions: {
    series: {
      dataLabels: {
        enabled: true,
        useHTML: true,
        formatter: function () {
          return '<center>' + this.y + ' <br>(' + this.point.growth + '%)</center>';
        }
      }
    }
  },
  series: 
  [{
    name: 'Parcel Count',
    type: 'column',
    yAxis: 1,
    data: data
  }, {
    name: 'Growth Percentage',
    type: 'spline',
    data: percentage
  }]
});
}

function allUsageWeekly(cat, data, percentage, growth) {
Highcharts.chart('all-usage-chart-weekly', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'Locker Activity by Parcel Usage'
  },
  xAxis: {
    categories: cat,
    crosshair: true
  },
  yAxis: [
  { // Secondary yAxis
    title: {
      text: 'Percentage',
      style: {
        color: Highcharts.getOptions().colors[0]
      }
    },
    labels: {
      format: '{value} %',
      style: {
        color: Highcharts.getOptions().colors[0]
      }
    },
    opposite: true
  },
  { // Primary yAxis
    labels: {
      format: '{value}',
      style: {
        color: Highcharts.getOptions().colors[1]
      }
    },
    title: {
      text: 'Number of Parcels',
      style: {
        color: Highcharts.getOptions().colors[1]
      }
    }
  }],
  tooltip: {
    formatter: function () {
      return this.point.series.name + ':<b>' + this.y + '</b><br>Growth: <b>' + this.point.growth + '</b>';
    }
  },
  plotOptions: {
    series: {
      dataLabels: {
        enabled: true,
        useHTML: true,
        formatter: function () {
          return '<center>' + this.y + ' <br>(' + this.point.growth + '%)</center>';
        }
      }
    }
  },
  series: 
  [{
    name: 'Parcel Count',
    type: 'column',
    yAxis: 1,
    data: data
  }, {
    name: 'Growth Percentage',
    type: 'spline',
    data: percentage
  }]
});
}

function allUsage(cat, data, percentage, growth) {
Highcharts.chart('all-usage-chart', {
  title: {
    text: 'Locker Activity by Parcel Usage'
  },
  xAxis: {
    categories: cat,
    // crosshair: true
  },
  yAxis: [
  { // Secondary yAxis
    title: {
      text: 'Percentage',
      style: {
        color: Highcharts.getOptions().colors[0]
      }
    },
    labels: {
      format: '{value} %',
      style: {
        color: Highcharts.getOptions().colors[0]
      }
    },
    opposite: true
  },
  { // Primary yAxis
    labels: {
      format: '{value}',
      style: {
        color: Highcharts.getOptions().colors[1]
      }
    },
    title: {
      text: 'Number of Parcels',
      style: {
        color: Highcharts.getOptions().colors[1]
      }
    }
  }
  ],
  tooltip: {
    formatter: function () {
      return this.point.series.name + ':<b>' + this.y + '</b><br>Growth: <b>' + this.point.growth + '</b>';
    },
    positioner: function(labelWidth, labelHeight, point) {
        var tooltipX = point.plotX + 20;
        var tooltipY = point.plotY - 30;
        return {
            x: tooltipX,
            y: tooltipY
        };
    }
  },
  plotOptions: {
    series: {
      dataLabels: {
        enabled: true,
        useHTML: true,
        formatter: function () {
          return '<center>' + this.y + ' <br>(' + this.point.growth + '%)</center>';
        }
      }
    }
  },
  series: 
  [{
    name: 'Parcel Count',
    type: 'column',
    yAxis: 1,
    data: data
  }, 
  {
    name: 'Growth Percentage',
    type: 'spline',
    data: percentage
  }]
});
}

function topLocation(cat, value) {
Highcharts.chart('top-location-chart', {
    chart: {
        type: 'bar'
    },
    title: {
        text: 'Top 10 Utility Location'
    },
    subtitle: {
        text: '10 Locations with The Highest Number of Parcels'
    },
    xAxis: {
        categories: cat,
        title: {
            text: null
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Number of Parcels',
            align: 'high'
        },
        labels: {
            overflow: 'justify'
        }
    },
    plotOptions: {
        bar: {
            dataLabels: {
                enabled: true
            }
        }
    },
    credits: {
        enabled: false
    },
    series: [{
        name: 'Count',
        data: value
    }]
});
}

function bottomLocation(cat, value) {
Highcharts.chart('bottom-location-chart', {
    chart: {
        type: 'bar'
    },
    title: {
        text: 'Lowest 10 Utility Location'
    },
    subtitle: {
        text: '10 Locations with The Lowest Number of Parcels'
    },
    xAxis: {
        categories: cat,
        title: {
            text: null
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Number of Parcels',
            align: 'high'
        },
        labels: {
            overflow: 'justify'
        }
    },
    plotOptions: {
        bar: {
            dataLabels: {
                enabled: true
            }
        }
    },
    credits: {
        enabled: false
    },
    series: [{
        name: 'Count',
        data: value,
        color: '#eb9797'
    }]
});
}

function uniqueUser(cat, catData, catPercentage) {
Highcharts.chart('unique-user-chart', {
  chart: {
    type: 'column'
  },
  title: {
    text: 'Unique User'
  },
  xAxis: {
    categories: cat,
    crosshair: true
  },
  yAxis: [
  { // Secondary yAxis
    title: {
      text: 'Percentage',
      style: {
        color: Highcharts.getOptions().colors[0]
      }
    },
    labels: {
      format: '{value} %',
      style: {
        color: Highcharts.getOptions().colors[0]
      }
    },
    opposite: true
  },
  { // Primary yAxis
    labels: {
      format: '{value}',
      style: {
        color: Highcharts.getOptions().colors[1]
      }
    },
    title: {
      text: 'Number of Users',
      style: {
        color: Highcharts.getOptions().colors[1]
      }
    }
  }],
  tooltip: {
    shared: true
  },
  plotOptions: {
    column: {
      pointPadding: 0.2,
      borderWidth: 0,
      dataLabels: {
        enabled: true
      },
    },
    series: {
      pointWidth: 50
    }
  },
  series: 
  [{
    name: 'User Count',
    type: 'column',
    yAxis: 1,
    data: catData,
    tooltip: {
      valueSuffix: ''
    }

  }, {
    name: 'User Growth',
    type: 'spline',
    data: catPercentage,
    tooltip: {
      valueSuffix: ' %'
    }
  }]
});
}

/* function categories(data) {
// Make monochrome colors
var pieColors = (function () {
  var colors = [],
    base = Highcharts.getOptions().colors[0],
    i;

  for (i = 0; i < 10; i += 1) {
    // Start out with a darkened base color (negative brighten), and end
    // up with a much brighter color
    colors.push(Highcharts.Color(base).brighten((i - 3) / 7).get());
  }
  return colors;
}());

// Build the chart
Highcharts.chart('categories-chart', {
  chart: {
    plotBackgroundColor: null,
    plotBorderWidth: null,
    plotShadow: false,
    type: 'pie'
  },
  title: {
    text: 'Building Category'
  },
  tooltip: {
    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
  },
  plotOptions: {
    pie: {
      allowPointSelect: true,
      cursor: 'pointer',
      colors: pieColors,
      dataLabels: {
        enabled: true,
        format: '<b>{point.name}</b><br>{point.percentage:.1f} %',
        distance: -50,
        filter: {
          property: 'percentage',
          operator: '>',
          value: 4
        }
      }
    }
  },
  series: [{
    name: 'Percentage',
    data: [
      { name: 'Office', y: 19.27 },
      { name: 'Super/Mini Market', y: 16.11 },
      { name: 'Railway Station', y: 14.19 },
      { name: 'Apartment/Residential', y: 13.97 },
      { name: 'University', y: 12.09 },
      { name: 'Shopping Mall', y: 11.48 },
      { name: 'Gas Station', y: 7.43 },
      { name: 'Cafe', y: 3.05 },
      { name: 'Other Public Area', y: 2.41 }
    ]
  }]
});
} */

$('#btnFilter').click(function() {
    var locker = $('#lockerName').select2('data')
    var lockerId = locker[0].id
    var lockerText = locker[0].text
    var country = $('#country').val();
    var year = $('#year').val();
    var date = $('#date').val();
    console.log(date);
    if(lockerId != '') { $('#btnLocker').show(); $('#btnLocker').text('Locker Name : ' + lockerText); }
    if(country != '') { $('#btnCountry').show(); $('#btnCountry').text('Country : ' + country); }
    if(year != '') { $('#btnYear').show(); $('#btnYear').text('Year : ' + year); }

    $('#all-usage-chart').empty();
    $('#top-location-chart').empty();
    $('#bottom-location-chart').empty();
    $('#unique-user-chart').empty();

    getData({
        "_token" : "{{ csrf_token() }}",
        "country" : country,
        "locker_id" : lockerId,
        "year" : year,
        "date" : date
    });
});

$('#btnReset').click(function() {
    $('#country').val('').trigger('change');;
    $('#lockerName').val('').trigger('change');;
    $('#year').val('').trigger('change');;

    $('.filter').hide();
    $('#all-usage-chart').empty();
    $('#top-location-chart').empty();
    $('#bottom-location-chart').empty();
    $('#unique-user-chart').empty();

    getData({
        "_token" : "{{ csrf_token() }}"
    });
});
</script>
@endsection